#!/bin/bash
#
# iprover_sine.sh -t 600 problem.smt2
# sets STAREXEC_WALLCLOCK_LIMIT=timelimit

#set -x

set -o pipefail

#START_DIR=$(dirname $0)
START_DIR=$(pwd)
IPROVER_DIR=$(dirname "${BASH_SOURCE[0]}")
HOS_ML="$IPROVER_DIR/HOS-ML"

SCRIPT_NAME=$(basename "$0")



echo ""
echo "======== iProver multi-core TPTP/SMT ========="
echo ""

# default values; can be changed by options
LANG=tptp # tptp|smt
CORES=8 
SCHEDULE= # defined below
STAREXEC_WALLCLOCK_LIMIT=300

case $LANG in 
    tptp) SCHEDULE="fof_schedule"
        ;;
    smt) SCHEDULE="smt_schedule"
        ;;
esac


SCHEDULE_MODE="external"

usage_msg="$SCRIPT_NAME [-h] [-t n] [-c n] [-] [-s {fof_schedule|fnt_schedule|ueq_schedule|smt_schedule}] [-m {external|internal}] problem.p

-h      help

-t      time limit 
        default: $STAREXEC_WALLCLOCK_LIMIT

-c      number of cores; 
        default: $CORES

-l      tptp|smt

-s         
        fof_schedule: general fof problems (tptp)
        fnt_schedule: model finding (tptp)
        ueq_schedule: unit equality (tptp)
        smt_schedule: smt  
        default: $SCHEDULE

-m   
        external: restart prover with each new heuristic
        internal: prover internally switches heursitics
        default: $SCHEDULE_MODE

Examples:
$SCRIPT_NAME -t 300 -c 8 -l tptp -s fof_schedule problem.p

$SCRIPT_NAME -t 300 -c 8 -l smt -s smt_schedule problem.smt2

"

if [[ $# -eq 0 ]]
then
    echo "$usage_msg"
    exit 1
fi

# if option -t 300 is supplied
while getopts ":ht:c:l:s:m:" option; do      
    case "${option}" 
        in
        h)  echo "$usage_msg" 
            exit 1
            ;;
        t) STAREXEC_WALLCLOCK_LIMIT=${OPTARG}
            ;;
        c) CORES=${OPTARG}
            ;;
        l) LANG=${OPTARG}           
            ;;
        s) SCHEDULE=${OPTARG} # convert smt_schedule to ...
            ;;
        m) SCHEDULE_MODE=${OPTARG}
            ;;
        ?) echo "Error: unsupported options"
            echo ""
            echo "$usage_msg"
            exit 1
            ;;
    esac    
done    






# PROBLEM="${@: -1}"
# "${@: -1}" does not work in StarExec bash
# check that works in Starexec

PROBLEM=${@:${#@}}

#full path 
if [ ! "${PROBLEM:0:1}" = "/" ]
then # relative  path
    PROBLEM="$(pwd)/$PROBLEM"    
fi


if [ ! -f "$PROBLEM" ]; then
    echo "error: problem does not exists: $PROBLEM" 
    echo "usage: --help"
    exit 1
fi


if [ ! -f "$PROBLEM" ]; then
    echo "error: problem does not exists: usage: --help"
    exit 1
fi

if [ ! -d "$HOS_ML" ]; then
    echo "error: should be run from iprover top directory containing HOS-ML"
    exit 1
fi

#------ 

cd "$HOS_ML"

# Increase the soft ulimit

SYS=$(uname -s)

if [ "$SYS" = "Darwin" ]; 
then     
    ULIMIT=60000
else
    ULIMIT=200000
fi

#ulimit -s $ULIMIT



if [ -z "$STAREXEC_WALLCLOCK_LIMIT" ]; then
    #  echo 	"error: time limit should be provided by setting e.g. env: export STAREXEC_WALLCLOCK_LIMIT=600"
    echo 	"error: time limit should be provided; usage: -h"
  exit 1
fi


# used in SMT only
TCF_TMP=$(mktemp /tmp/iprover.tmp.tcf.XXXXXX)
OUT_TMP=$(mktemp /tmp/iprover.tmp.out.XXXXXX)

function rm_tmp_files {
    rm "$TCF_TMP"
    rm "$OUT_TMP"    
}

#-----------kill children

function killtree {
    local ppid=$1
    if [ "$SYS" = "Darwin" ]; 
    then
        local CHILDREN=`ps -o pid,ppid | awk -v ppid=$ppid '$2 ~ ppid' | awk '{print $1;}'`
#       local CHILDREN=`pstree -p $ppid | tr "\n" " " |sed "s/[^0-9]/ /g" |sed "s/\s\s*/ /g"`
        #local CHILDREN=`ps -o pid,ppid | grep '^[0-9]' | grep ' '$ppid | cut -f 1 -d ' '`
    else
        local CHILDREN=`ps -o pid --no-heading --ppid $ppid`
    fi
     

    if [ ! -z "$CHILDREN" ];
    then
        for child_pid in ${CHILDREN}; 
        do
            killtree ${child_pid}
        done
    fi
   # kill -TERM ${ppid}
    kill -9 ${child_pid} 2>/dev/null
}

function killChildProcesses {

    killtree "$$"
}


function terminate {
    killChildProcesses
    rm_tmp_files
#    echo "Time Out Real"
    wait
    cd $START_DIR
    exit $1
}

function interrupted {
#       echo "interrupted" >> $LOGFILE
        echo "Interrupted"
        terminate 1
}

trap interrupted SIGINT SIGQUIT SIGTERM

#------------ end kill children

#--------- TPTP

function run_tptp {
    ulimit -s $ULIMIT
    python3 prover_driver.py --no_cores $CORES --schedule_mode $SCHEDULE_MODE --schedule $SCHEDULE  $PROBLEM $STAREXEC_WALLCLOCK_LIMIT
}


#----------- SMT

function run_smt {
    ulimit -s $ULIMIT
    # already in HOS-ML dir
    ./res/vclausify_rel --input_syntax smtlib2 --mode tclausify --show_fool true $PROBLEM |sed -e "s/[']\+/'/g" | sed -e "s/\([^[:blank:](),~]\)\('\)\([^[:blank:](),:]\)\([^']*\)\('\)/\1\3\4\5/g" > $TCF_TMP  
#2>/dev/null

    vclausify_exit_status=$?
    
    if [ $vclausify_exit_status -ne 0 ]; then
        cat $TCF_TMP
        echo "error: clausifier exit with status: $vclausify_exit_status"
        echo unknown        
        terminate 1    
    else
        grep "(set-logic UF)" $PROBLEM &> /dev/null
        is_UF=$?
        if [ $is_UF -eq 0 ]; then
            # echo "UF problem: can return sat/unsat"            
            if [ "$SCHEDULE" = "smt_schedule" ]; then
                #TODO: schedule for more cores! currently only 4 are used
                SCHEDULE="smt_2scp_default_with_sat_300_x4"
            fi


            python3 prover_driver.py --no_cores $CORES --schedule_mode $SCHEDULE_MODE --schedule $SCHEDULE $TCF_TMP $STAREXEC_WALLCLOCK_LIMIT > $OUT_TMP  2>/dev/null
	    cat $OUT_TMP
            if cat $OUT_TMP | grep -q "SZS status Theorem\|SZS status Unsatisfiable"
            then
                echo "unsat"
            else        
                cat $OUT_TMP
	        if cat $OUT_TMP | grep -q "SZS status Satisfiable\|SZS status CounterSatisfiable"
	        then
	            echo "sat"
	        else
	            echo "unknown"
	        fi	
            fi          
        else
#            echo "not UF problem; can return only unsat"
            
            if [ "$SCHEDULE" = "smt_schedule" ]; then
                #TODO: schedule for more cores! currently only 4 are used
                SCHEDULE="smt_3scp_default_300_x4"
            fi
            
            python3 prover_driver.py --no_cores $CORES --schedule_mode $SCHEDULE_MODE --schedule $SCHEDULE $TCF_TMP $STAREXEC_WALLCLOCK_LIMIT >  $OUT_TMP  2>/dev/null

# replace sat with 
            cat $OUT_TMP | sed -e "s/% SZS status Satisfiable\|% SZS status CounterSatisfiable/% SZS status Unknown; Satisfiable under incomplete theory axiomatisation/g"
            if cat $OUT_TMP | grep -q "SZS status Theorem\|SZS status Unsatisfiable"
            then
                echo "unsat"
            else        
                echo "unknown"
            fi
        fi
    fi       
}


#---------------

case $LANG in 
    tptp) run_tptp
        ;;
    smt) run_smt
        ;;
esac 

terminate 0

