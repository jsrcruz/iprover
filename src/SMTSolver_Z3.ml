open Lib
open Logic_interface



(*----- debug modifiable part-----*)

let dbg_flag = true

type dbg_gr = 
  | D_trace


let dbg_gr_to_str = function 
  | D_trace -> "trace"

let dbg_groups = [
  D_trace;
]
    
let module_name = "SMTSolver_Z3"

(*----- debug fixed part --------*)

let () = dbg_flag_msg dbg_flag module_name

let dbg group str_lazy = 
  Lib.dbg_out_pref dbg_flag dbg_groups group dbg_gr_to_str module_name str_lazy

let dbg_env group f = 
  Lib.dbg_env_set dbg_flag dbg_groups group f
    
(*----- debug -----*)





module Term_map = struct
  type key = { 
    regular: Z3.Expr.expr option; 
    prop: Z3.Expr.expr option; 
    quant: Z3.Expr.expr option; 
  }

  (* type map = key TMap.t *)

  let empty = 
    { regular=None; prop=None; quant=None }
end

type symb_or_var = Symb of symbol | Var of var

type state = {
  ctx: Z3.context;

  mutable prop_term_id: int;
  mutable term_map: Term_map.key TMap.t;  (* TODO: investigate performance impact when this potentially has 100,000s of terms *)

  mutable sort_map: Z3.Sort.sort SMap.t;

  mutable var_id: int;
  mutable var_map: Z3.Symbol.symbol VMap.t;

  mutable funcdecl_map: Z3.FuncDecl.func_decl SMap.t;
  mutable functheory_map: (Z3.Expr.expr list -> Z3.Expr.expr) SMap.t;  (* TODO: split according to arity? *)

  mutable rev_symbol_map: symb_or_var IntMap.t;

  (* mutable numbering_max: int;
  mutable numbering_map: Z3.Symbol.symbol IntMap.t;  (* TODO change to lookup array? *) *)

  fast_tactic: Z3.Tactic.tactic;
}

type term = Z3.Expr.expr

type clause = Z3.Expr.expr

module Ip = Logic_interface



(* Theories *)
let add_bool_sorts ctx smap = 
  smap 
  |> SMap.add Symbol.symb_bool_type (Z3.Boolean.mk_sort ctx)
  |> SMap.add Symbol.symb_bool_fun_type (Z3.Boolean.mk_sort ctx)

let add_arithmetic_sorts ctx smap = 
  smap 
  |> SMap.add Symbol.symb_real_type (Z3.Arithmetic.Real.mk_sort    ctx)
  |> SMap.add Symbol.symb_rat_type  (Z3.Arithmetic.Real.mk_sort    ctx)
  |> SMap.add Symbol.symb_int_type  (Z3.Arithmetic.Integer.mk_sort ctx)

let add_bool_funcs ctx smap = 
  let [@warning "-8"] z3_true  [] = Z3.Boolean.mk_val ctx true  in
  let [@warning "-8"] z3_false [] = Z3.Boolean.mk_val ctx false in
  smap
  |> SMap.add Symbol.symb_true_fun  z3_true
  |> SMap.add Symbol.symb_false_fun z3_false

let add_arithmetic_funcs ctx fmap = 
  (* TODO: convert iprover $add(x,$add(y,z)) to z3 (+ x y z) *)
  let z3_add = Z3.Arithmetic.mk_add ctx in
  let z3_sub = Z3.Arithmetic.mk_sub ctx in
  let z3_mul = Z3.Arithmetic.mk_mul ctx in
  let [@warning "-8"] z3_div [x;y] = Z3.Arithmetic.mk_div ctx x y in
  let [@warning "-8"] z3_uminus [x] = Z3.Arithmetic.mk_unary_minus ctx x in
  let [@warning "-8"] z3_lt [x;y] = Z3.Arithmetic.mk_lt ctx x y in
  let [@warning "-8"] z3_le [x;y] = Z3.Arithmetic.mk_le ctx x y in
  let [@warning "-8"] z3_gt [x;y] = Z3.Arithmetic.mk_gt ctx x y in
  let [@warning "-8"] z3_ge [x;y] = Z3.Arithmetic.mk_ge ctx x y in

  fmap 
  |> SMap.add Symbol.symb_sum_real        z3_add
  |> SMap.add Symbol.symb_difference_real z3_sub
  |> SMap.add Symbol.symb_product_real    z3_mul
  |> SMap.add Symbol.symb_quotient_real   z3_div
  |> SMap.add Symbol.symb_uminus_real     z3_uminus
  |> SMap.add Symbol.symb_less_real       z3_lt
  |> SMap.add Symbol.symb_lesseq_real     z3_le
  |> SMap.add Symbol.symb_greater_real    z3_gt
  |> SMap.add Symbol.symb_greatereq_real  z3_ge
  
  |> SMap.add Symbol.symb_sum_int         z3_add
  |> SMap.add Symbol.symb_difference_int  z3_sub
  |> SMap.add Symbol.symb_product_int     z3_mul
  (* |> SMap.add Symbol.symb_quotient_int    z3_div *)
  |> SMap.add Symbol.symb_uminus_int      z3_uminus
  |> SMap.add Symbol.symb_less_int        z3_lt
  |> SMap.add Symbol.symb_lesseq_int      z3_le
  |> SMap.add Symbol.symb_greater_int     z3_gt
  |> SMap.add Symbol.symb_greatereq_int   z3_ge



(*  Symbols in Z3 are indexed by ints. We use the following ranges: 
      lower    | upper       | 
      0        | 1 ×2^28 - 1 | symbols fast_key 
      1  ×2^28 | 2 ×2^28 - 1 | variables (no relation to var fast key)
      3  ×2^28 | 4 ×2^28 - 1 | tags
      4  ×2^28 | 5 ×2^28 - 1 | unsat core tags
      10 ×2^28 | ——          | atoms to propositional atoms
*)
let range_symb_l = 0
let range_symb_u = 1 lsl 28
let range_var_l = 1 lsl 28
let range_var_u = 2 lsl 28
(* let range_intprop_l = 2 lsl 28
let range_intprop_u = 3 lsl 28 *)
let range_tag_l = 3 lsl 28
let range_tag_u = 4 lsl 28
let range_uctag_l = 4 lsl 28
let range_uctag_u = 5 lsl 28
let range_prop_l = 10 lsl 28
let range_prop_u = Int.max_int



type options = {
  interpreted_arithmetic: bool;
}

let make_state opts = 
  let ctx = Z3.mk_context [] in

  (* Database of interpreted terms/sorts must already be baked into the initial state *)
  let sort_map = 
    SMap.empty 
    |> add_bool_sorts ctx
    |> if opts.interpreted_arithmetic then add_arithmetic_sorts ctx else Fun.id
  in
  
  let funcdecl_map =
    SMap.empty
    (* |> SMap.add symb_typed_equality (Z3.) *)
  in

  let functheory_map = 
    SMap.empty 
    |> add_bool_funcs ctx
    |> if opts.interpreted_arithmetic then add_arithmetic_funcs ctx else Fun.id
  in

  {
    ctx;

    prop_term_id = range_prop_l;
    term_map = TMap.empty;

    sort_map = sort_map;

    var_id = range_var_l;
    var_map = VMap.empty;

    funcdecl_map;
    functheory_map;

    rev_symbol_map = IntMap.empty;

    (* numbering_max = 0;
    numbering_map = IntMap.empty; *)

    (*
    fast_tactic = 
      Z3.Tactic.mk_tactic ctx 
        "solve-eqs";
        (* "ctx-simplify"; *)
        (* "ctx-solver-simplify"; *)
        (* "smt"; *)
        (* "sat-preprocess"; *)
    *)
    fast_tactic = 
      let (|>) a b = Z3.Tactic.and_then ctx a b [] in
      let t x = Z3.Tactic.mk_tactic ctx x in
      t "simplify" |> t "ctx-simplify" |> t "ctx-solver-simplify" |> t "sat-preprocess"
  }





(* Plain version *)
let var_to_z3symbol state var =
  (* Map variables to z3 symbols in range [1*2^28, 2*2^28[ *)
  let n = state.var_id in
  assert (n < range_var_u);
  state.var_id <- succ state.var_id;
  Z3.Symbol.mk_int state.ctx n
  |> tap (fun x -> 
    dbg D_trace @@ lazy (sprintf "New entry: var %s is SMT %s" (Var.to_string var) (Z3.Symbol.to_string x))
  )

(* Caching version *)
let var_to_z3symbol state var =
  match VMap.find_opt var state.var_map with
  | Some x -> x
  | None -> 
    let result = var_to_z3symbol state var in
    state.var_map <- VMap.add var result state.var_map;
    state.rev_symbol_map <- IntMap.add (state.var_id - 1) (Var var) state.rev_symbol_map;
    result



let type_to_z3sort' state typ =
  (* Map symbols to z3 symbols in range [0*2^28, 1*2^28[ *)
  let n = Symbol.get_fast_key typ + range_symb_l in
  assert (n < range_symb_u);
  (* let symb' = Z3.Symbol.mk_int state.ctx n in *)
  let symb' = Z3.Symbol.mk_string state.ctx (string_of_int n) in
  dbg D_trace @@ lazy (sprintf "New entry: type symb %s is SMT %s" (Symbol.to_string typ) (Z3.Symbol.to_string symb'));
  Z3.Sort.mk_uninterpreted state.ctx symb'

let type_to_z3sort state typ =
  match SMap.find_opt typ state.sort_map with
  | Some x -> x
  | None ->
    let result = type_to_z3sort' state typ in
    state.sort_map <- SMap.add typ result state.sort_map;
    result
    (* |> tap (fun x -> state.sort_map <- SMap.add typ x state.sort_map) *)



let func_to_z3funcdecl' state symb =
  (* Map symbols to z3 symbols in range [0*2^28, 1*2^28[ *)
  let n = Symbol.get_fast_key symb + range_symb_l in
  assert (n < range_symb_u);
  let symb' = Z3.Symbol.mk_int state.ctx n in
  (* dbg D_trace @@ lazy (sprintf "New entry: symb %s is SMT %s" (Symbol.to_string symb) (Z3.Symbol.to_string symb')); *)

  let args_typ, value_typ = Symbol.get_stype_args_val_def symb in
  let args_typ' = List.map (type_to_z3sort state) args_typ in
  let value_typ' = type_to_z3sort state value_typ in

  dbg D_trace @@ lazy (sprintf "New entry: func_decl %s : %s > %s is SMT %s : %s > %s" 
    (Symbol.to_string symb)      
    (List.X.to_string ~first:"" ~last:"" ~sep:" * " Symbol.to_string args_typ) 
    (Symbol.to_string value_typ)

    (Z3.Symbol.to_string symb')
    (List.X.to_string ~first:"" ~last:"" ~sep:" * " Z3.Sort.to_string args_typ')
    (Z3.Sort.to_string value_typ')
  );

  Z3.FuncDecl.mk_func_decl state.ctx symb' args_typ' value_typ'

let func_to_z3funcdecl state symb =
  match SMap.find_opt symb state.funcdecl_map with
  | Some x -> x
  | None ->
    let result = func_to_z3funcdecl' state symb in
    state.funcdecl_map <- SMap.add symb result state.funcdecl_map;
    state.rev_symbol_map <- IntMap.add (Symbol.get_fast_key symb) (Symb symb) state.rev_symbol_map;
    result



let rec term_to_z3expr' state t =
  match t with 
  | Term.Fun (symb, args, _) ->
    (* Handle equality predicate *)
    if symb == Symbol.symb_typed_equality then (
      let (typ, l,r) = Term.get_3_args args in
      let l' = term_to_z3expr state l in
      let r' = term_to_z3expr state r in
      Z3.Boolean.mk_eq state.ctx l' r'
    (* Handle negation *)
    ) else if symb == Symbol.symb_neg then (
      let x = Term.get_1_args args in
      let x' = term_to_z3expr state x in
      Z3.Boolean.mk_not state.ctx x'
    ) else begin match Symbol.get_property symb with
    (* Handle theory function *)
    | Theory -> 
      let func' = SMap.find symb state.functheory_map in
      let args' = List.map (term_to_z3expr state) (Term.arg_to_list args) in
      (* TODO dassert correct sort *)
      func' args'
    (* Handle numeral *)
    | Num_int _ -> 
      Z3.Arithmetic.Integer.mk_numeral_s state.ctx (Symbol.get_name symb)
    (* | Num_rat (_, n, d) -> 
      let n = Z3.Arithmetic.Real.mk_numeral_s state.ctx n in 
      let d = Z3.Arithmetic.Real.mk_numeral_s state.ctx d in 
      Z3.Arithmetic.mk_div state.ctx n d *)
    | Num_rat _ -> 
      Z3.Arithmetic.Real.mk_numeral_s state.ctx (Symbol.get_name symb)
    | Num_real -> 
      Z3.Arithmetic.Real.mk_numeral_s state.ctx (Symbol.get_name symb)
    (* Handle regular predicate/function *)
    | _ ->
      let args' = List.map (term_to_z3expr state) (Term.arg_to_list args) in
      let func_decl' = func_to_z3funcdecl state symb in
      Z3.Expr.mk_app state.ctx func_decl' args'
    end
  | Term.Var (var, _) ->
    let sort' = type_to_z3sort state (Var.get_type var) in
    let var' = var_to_z3symbol state var in
    Z3.Expr.mk_const state.ctx var' sort'

and term_to_z3expr state t =
  match TMap.find_opt t state.term_map with
  | Some Term_map.{regular=Some x} -> 
    x
  (* | Some (None  , y) -> *)
  | Some (Term_map.{regular=None} as value) -> 
    let result = term_to_z3expr' state t in
    (* let value = (Some result, y) in *)
    let value = {value with regular = Some result} in
    state.term_map <- TMap.add t value state.term_map;
    result
  | None -> 
    let result = term_to_z3expr' state t in
    let value = {Term_map.empty with regular = Some result} in
    state.term_map <- TMap.add t value state.term_map;
    result



let lit_to_smt = term_to_z3expr

let clause_to_smt state clause =
  let lits = Clause.get_lits clause in
  match lits with 
  | [lit] ->
    term_to_z3expr state lit
  | lits ->
    Z3.Boolean.mk_or state.ctx (List.map (term_to_z3expr state) lits)



let term_to_z3expr_prop' state t = 
  let n = state.prop_term_id in
  assert (n < range_prop_u);
  state.prop_term_id <- succ state.prop_term_id;
  Z3.Symbol.mk_int state.ctx n 
  |> Z3.Boolean.mk_const state.ctx

let term_to_z3expr_prop state t = 
  dbg D_trace @@ lazy "term_to_z3expr_prop";
  let sign, atom = Term.split_sign_lit t in
  let prop_atom = 
    match TMap.find_opt t state.term_map with
    | Some Term_map.{prop=Some x} -> 
      x
    | Some (Term_map.{prop=None} as value) ->
      let result = term_to_z3expr_prop' state t in
      let value = {value with prop = Some result} in
      state.term_map <- TMap.add t value state.term_map;
      result
    | None -> 
      let result = term_to_z3expr_prop' state t in
      let value = {Term_map.empty with prop = Some result} in
      state.term_map <- TMap.add t value state.term_map;
      result
  in
  (
  if sign then
    prop_atom
  else
    Z3.Boolean.mk_not state.ctx prop_atom
  ) |> tap (fun _ -> dbg_env D_trace (fun () ->
    dbg D_trace @@ lazy "term_map:"; 
    state.term_map |> TMap.iter (fun key Term_map.{regular; prop; quant} ->
      dbg D_trace @@ lazy (sprintf "%s -> (%s , %s, %s)" 
        (Term.to_string key) 
        (Option.to_string Z3.Expr.to_string regular)
        (Option.to_string Z3.Expr.to_string prop)
        (Option.to_string Z3.Expr.to_string quant)
      )
    )
  ))


  
let lit_to_smt_prop = term_to_z3expr_prop

let clause_to_smt_prop state clause =
  let lits = Clause.get_lits clause in
  match lits with
  | [lit] -> 
    term_to_z3expr_prop state lit
  | lits -> 
    Z3.Boolean.mk_or state.ctx (List.map (term_to_z3expr_prop state) lits)



(*
let int_to_smt_prop state sign n = 
  dbg D_trace @@ lazy (sprintf "int_to_smt_prop: %c%d" (if sign then '+' else '-') (n));
  let z3_atom = 
    (* let n = state.prop_term_id + (2 lsl 28) in *)
    (* state.prop_term_id <- succ state.prop_term_id; *)
    let n = n + range_intprop_l in
    assert (n < range_intprop_u);
    Z3.Symbol.mk_int state.ctx n 
    |> Z3.Boolean.mk_const state.ctx
  in
  (if sign then
    z3_atom
  else
    Z3.Boolean.mk_not state.ctx z3_atom
  ) |> tap (fun x -> dbg D_trace @@ lazy (sprintf "becomes %s" (Z3.Expr.to_string x)))
*)





let rec term_to_z3expr_quant' state t =
  match t with 
  | Term.Fun (symb, args, _) ->
    (* Handle equality predicate *)
    if symb == Symbol.symb_typed_equality then (
      let (typ, l,r) = Term.get_3_args args in
      let l' = term_to_z3expr_quant state l in
      let r' = term_to_z3expr_quant state r in
      Z3.Boolean.mk_eq state.ctx l' r'
    (* Handle negation *)
    ) else if symb == Symbol.symb_neg then (
      let x = Term.get_1_args args in
      let x' = term_to_z3expr state x in
      Z3.Boolean.mk_not state.ctx x'
    (* Handle regular predicate/ *)
    ) else (
      let args' = List.map (term_to_z3expr_quant state) (Term.arg_to_list args) in
      let func_decl' = func_to_z3funcdecl state symb in
      Z3.Expr.mk_app state.ctx func_decl' args'
    )
  | Term.Var (var, _) ->
    failwith "unimplemented"

and term_to_z3expr_quant state t =
  match TMap.find_opt t state.term_map with
  | Some Term_map.{quant=Some x} -> 
    x
  | Some (Term_map.{quant=None} as value) -> 
    let result = term_to_z3expr_quant' state t in
    let value = {value with quant = Some result} in
    state.term_map <- TMap.add t value state.term_map;
    result
  | None -> 
    let result = term_to_z3expr_quant' state t in
    let value = {Term_map.empty with quant = Some result} in
    state.term_map <- TMap.add t value state.term_map;
    result

let lit_to_smt_quant = term_to_z3expr_quant

let clause_to_smt_quant state clause =
  let lits = Clause.get_lits clause in
  match lits with
  | [lit] ->
    term_to_z3expr_quant state lit
  | lits ->
    Z3.Boolean.mk_or state.ctx (List.map (term_to_z3expr_quant state) lits)





exception Return

let rec term_of_z3expr state term = 
  let[@inline] typed args args' fi fr = 
    let term = List.hd args in
    if Z3.Arithmetic.is_int term then
      add_fun_term fi args'
    else (
      dassert (fun () -> Z3.Arithmetic.is_real term);
      add_fun_term fr args'
    )
  in
  let[@inline] typed_variadic args args' fi fr = 
    let term = List.hd args in
    if Z3.Arithmetic.is_int term then
      AC.mk_term fi args'
    else (
      dassert (fun () -> Z3.Arithmetic.is_real term);
      AC.mk_term fr args'
    )
  in

  dbg D_trace @@ lazy (sprintf "term_of_z3expr: %s" (Z3.Expr.to_string term));
  let args = Z3.Expr.get_args term in
  let args' = List.map (term_of_z3expr state) args in

  let result = 

  if Z3.Boolean.is_true term then
    add_fun_term Symbol.symb_true_fun []
  else if Z3.Boolean.is_false term then
    add_fun_term Symbol.symb_false_fun []

  else if Z3.Arithmetic.is_add term then
    typed_variadic args args' Symbol.symb_sum_int Symbol.symb_sum_real
  else if Z3.Arithmetic.is_sub term then
    typed_variadic args args' Symbol.symb_difference_int Symbol.symb_difference_real
  else if Z3.Arithmetic.is_mul term then
    typed_variadic args args' Symbol.symb_product_int Symbol.symb_product_real
  else if Z3.Arithmetic.is_div term then
    (dassert (fun () -> Z3.Arithmetic.is_real term); add_fun_term Symbol.symb_quotient_real args')
  else if Z3.Arithmetic.is_uminus term then
    typed args args' Symbol.symb_uminus_int Symbol.symb_uminus_real
  else if Z3.Arithmetic.is_lt term then
    typed args args' Symbol.symb_less_int Symbol.symb_less_real
  (* These three should not occur in normalised terms? *)
  else if Z3.Arithmetic.is_le term then
    typed args args' Symbol.symb_lesseq_int Symbol.symb_lesseq_real
  else if Z3.Arithmetic.is_gt term then
    typed args args' Symbol.symb_greater_int Symbol.symb_greater_real
  else if Z3.Arithmetic.is_ge term then
    typed args args' Symbol.symb_greatereq_int Symbol.symb_greatereq_real

  else if Z3.Arithmetic.is_int_numeral term then
    let name = Z3.Arithmetic.Integer.numeral_to_string term in
    let symb = create_symbol_property name (Symbol.create_stype [] Symbol.symb_int_type) (Symbol.Num_int (Z.of_string name)) in
    add_fun_term symb []
  else if Z3.Arithmetic.is_rat_numeral term then
    let name = Z3.Arithmetic.Real.numeral_to_string term in
    let symb = create_symbol_property name (Symbol.create_stype [] Symbol.symb_real_type) (Symbol.Num_real (*Q.of_string name*)) in
    add_fun_term symb []

  else (
    dbg D_trace @@ lazy (sprintf "%d" (term |> Z3.Expr.get_func_decl |> Z3.FuncDecl.get_decl_kind |> Z3enums.int_of_decl_kind));
    dbg D_trace @@ lazy (sprintf "%s" (term |> Z3.Expr.get_func_decl |> Z3.FuncDecl.to_string));
    dbg D_trace @@ lazy (sprintf "%s" (term |> Z3.Expr.get_func_decl |> Z3.FuncDecl.get_name |> Z3.Symbol.to_string));
    let symb = term |> Z3.Expr.get_func_decl |> Z3.FuncDecl.get_name |> Z3.Symbol.get_int in
    (* let symb' = IntMap.find symb state.rev_symbol_map in *)
    match IntMap.find symb state.rev_symbol_map with
    | Symb symb' -> add_fun_term symb' args'
    | Var var' -> add_var_term var'
  )

  in dbg D_trace @@ lazy (sprintf "term_of_z3expr: %s to %s" (Z3.Expr.to_string term) (Term.to_string result)); result

let rec lit_of_z3expr state lit = 
  let[@inline] typed args args' fi fr = 
    let term = List.hd args in
    if Z3.Arithmetic.is_int term then
      add_fun_term fi args'
    else (
      dassert (fun () -> Z3.Arithmetic.is_real term);
      add_fun_term fr args'
    )
  in

  dbg D_trace @@ lazy (sprintf "lit_of_z3expr: %s" (Z3.Expr.to_string lit));
  let args = Z3.Expr.get_args lit in
  if Z3.Boolean.is_not lit then
    let [x] = args in
    add_compl_lit (lit_of_z3expr state x)
  else if Z3.Boolean.is_eq lit then
    let [x;y] = args in
    let x' = term_of_z3expr state x in
    let y' = term_of_z3expr state y in
    (* let typ = undefined() in
    add_lit_eq true typ x' y' *)
    let typ = Term.get_term_type x' in
    dassert (fun () -> typ == Term.get_term_type y');
    add_typed_equality_sym typ x' y'
  else if Z3.Arithmetic.is_lt lit then
    let args' = List.map (term_of_z3expr state) args in
    typed args args' Symbol.symb_less_int Symbol.symb_less_real
  (* These three should not occur in normalised terms? *)
  else if Z3.Arithmetic.is_le lit then
    let args' = List.map (term_of_z3expr state) args in
    typed args args' Symbol.symb_lesseq_int Symbol.symb_lesseq_real
  else if Z3.Arithmetic.is_gt lit then
    let args' = List.map (term_of_z3expr state) args in
    typed args args' Symbol.symb_greater_int Symbol.symb_greater_real
  else if Z3.Arithmetic.is_ge lit then
    let args' = List.map (term_of_z3expr state) args in
    typed args args' Symbol.symb_greatereq_int Symbol.symb_greatereq_real

  else
    term_of_z3expr state lit

let lit_of_z3expr state lit = 
  let lit' = lit_of_z3expr state lit in
  if Term.get_top_symb lit' == Symbol.symb_true_fun then
    raise_notrace Return
  else if Term.get_top_symb lit' == Symbol.symb_false_fun then
    None
  else
    (*  *)
    Some lit'

let clause_of_z3expr state clause = 
  dbg D_trace @@ lazy (sprintf "clause_of_z3expr: %s" (Z3.Expr.to_string clause));
  dbg_env D_trace (fun () -> 
    dbg D_trace @@ lazy "rev_map";
    state.rev_symbol_map |> IntMap.iter (fun k v -> 
      dbg D_trace @@ lazy (sprintf "  %d %s" k (match v with Symb s -> Symbol.to_string s | Var v -> Var.to_string v));
    )
  );
  if Z3.Boolean.is_or clause then
    Z3.Expr.get_args clause
    |> List.X.filter_map (lit_of_z3expr state)
  else
    match lit_of_z3expr state clause with Some x -> [x] | None -> []

let term_of_smt = term_of_z3expr

let lit_of_smt state lit = 
  match lit_of_z3expr state lit with
  | Some lit' -> lit'
  | None -> term_false
  | exception Return -> term_true

let clause_of_smt state clause = 
  try Some (clause_of_z3expr state clause)
  with Return -> None





let term_to_unit_clause state x = 
  x

let term_list_to_clause state l =
  dbg D_trace @@ lazy (sprintf "formula_list_to_formula: id=%d" (Obj.magic state : int));
  dbg_env D_trace (fun () ->
    l |> List.iteri (fun i x ->
      (* eprintf "%d: %s\n" (i) (Z3.Sort.to_string (Z3.Expr.get_sort x)) *)
      dbg D_trace @@ lazy (sprintf "[%d] %s: %s id=%d" (i) (Z3.Expr.to_string x) (Z3.Sort.to_string (Z3.Expr.get_sort x)) (Obj.magic x : int))
    )
  );
  match l with
  | [] -> invalid_arg "formula_list_to_formula: empty list"
  | [x] -> x
  | _::_::_ -> 
      let [@warning "-26"] rec check l' =
        match l' with
        | a::b::tl -> 
            dbg D_trace @@ lazy (sprintf "checking %s = %s" (Z3.Sort.to_string @@ Z3.Expr.get_sort a) (Z3.Sort.to_string @@ Z3.Expr.get_sort b));
            (* assert (Z3.Expr.get_sort a != Z3.Expr.get_sort b); check (b::tl) *)
            assert (Z3.Sort.equal (Z3.Expr.get_sort a) (Z3.Expr.get_sort b)); check (b::tl)
        | _ -> ()
      in
    dbg D_trace @@ lazy "fltf: foo";
    (* [%dbg D_trace "foo"] *)
    (* check l;  (* l has all same type *)
    l |> List.iter (fun x -> assert (Z3.Boolean.is_bool x));  (* and that type is clearly bool *) *)
    dbg D_trace @@ lazy "fltf: bar";
    Z3.Boolean.mk_or state.ctx (l |> tap (fun x -> dbg D_trace @@ lazy (sprintf "len=%d" (List.length x))))
    (* then why "Sort mismatch at argument #1 for function (declare-fun or (Bool Bool) Bool) supplied sort is Bool"?? *)
    |> tap (fun _ -> dbg D_trace @@ lazy "fltf: qux")

let tagged_formula state id clause =
  let id_term = 
    let n = id + range_tag_l in
    assert (n < range_tag_u);
    Z3.Symbol.mk_int state.ctx n
    |> Z3.Boolean.mk_const state.ctx
  in
  Z3.Boolean.mk_or state.ctx [id_term; clause]





type problem = Z3.Solver.solver

let make_problem state =
  (* Z3.Solver.mk_simple_solver state.ctx *)
  Z3.Solver.mk_solver state.ctx None





let add solver clause =
  dbg D_trace @@ lazy (sprintf "add: %s" (Z3.Expr.to_string clause));
  (* try *)
  Z3.Solver.add solver [clause]
  (* with Z3.Error _ -> dbg D_trace @@ lazy "ERROR!" *)

let add_many solver clauses =
  dbg_env D_trace (fun () ->
    clauses |> List.iter (fun x ->
      dbg D_trace @@ lazy (sprintf "add_many: %s" (Z3.Expr.to_string x));
    )
  );
  Z3.Solver.add solver clauses

let push solver = 
  Z3.Solver.push solver

let pop solver = 
  Z3.Solver.pop solver 1

(* pp_ *)

let clear solver = 
  Z3.Solver.reset solver





type result = Sat | Unsat | Unknown
(* INVARIANT: Cannot have fields (`of stuff`) *)

let z3status_to_result x =
  match x with
  | Z3.Solver.SATISFIABLE   -> Sat
  | Z3.Solver.UNKNOWN       -> Unknown
  | Z3.Solver.UNSATISFIABLE -> Unsat

let check_assumptions solver assumptions =
 Statistics.(time smt_solver_time) @@ fun () -> 

  (* dbg D_trace @@ lazy (sprintf "assumptions = %a\n" (List.X.output ~sep:"\n" Z3.Expr.to_string) assumptions); *)
  dbg D_trace @@ lazy (sprintf "clauses = %s" (List.X.to_string ~sep:"\n" Z3.Expr.to_string (Z3.Solver.get_assertions solver)));
  dbg D_trace @@ lazy (sprintf "assumptions = %s" (List.X.to_string ~sep:"\n" Z3.Expr.to_string assumptions));
  Statistics.(bump_int_stat smt_solver_calls);
  let status = Z3.Solver.check solver assumptions in
  dbg D_trace @@ lazy (sprintf "result = %s" (Z3.Solver.string_of_status status));
  z3status_to_result status

let check solver =
  check_assumptions solver []

(* Fast check *)
(* Method 1: try to simplify assumptions to tautology *)
(* let fast_check_assumptions solver assumptions = 
  dbg D_trace @@ lazy (sprintf "fast_check");
  dbg D_trace @@ lazy (sprintf "clauses = %s" (List.X.to_string ~sep:"\n" Z3.Expr.to_string (Z3.Solver.get_assertions solver)));
  dbg D_trace @@ lazy (sprintf "assumptions = %s" (List.X.to_string ~sep:"\n" Z3.Expr.to_string assumptions));

  Statistics.(bump_int_stat smt_fast_solver_calls);
  let start_time = Unix.gettimeofday() in
  let result = 
    List.for_all (fun c ->
      dbg D_trace @@ lazy (sprintf "c  %s" (Z3.Expr.to_string c));
      let c' = Z3.Expr.simplify c None in
      dbg D_trace @@ lazy (sprintf "c' %s" (Z3.Expr.to_string c'));
      Z3.Boolean.is_false c'
    ) assumptions
  in
  let end_time = Unix.gettimeofday() in
  Statistics.(add_float_stat (end_time -. start_time) smt_fast_solver_time);
  match result with
  | true -> Unsat
  | false -> Unknown *)





type model = Z3.Model.model

let model solver = 
  None



module Uc = struct
  type problem = {
    prob: Z3.Solver.solver; 
    mutable next_number: int;
    ctx: Z3.context;
  }

  type tag = Z3.Expr.expr

  let tag_equal = Z3.Expr.equal

  let make_problem state = {
    prob = make_problem state;
    next_number = range_uctag_l;
    ctx = state.ctx;
  }

  let next_numbering (solver:problem) =
    let n = solver.next_number in
    solver.next_number <- succ solver.next_number;
    Z3.Symbol.mk_int solver.ctx n
    |> Z3.Boolean.mk_const solver.ctx

  let add (solver:problem) clause : tag =
    let tag = next_numbering solver in
    Z3.Solver.assert_and_track solver.prob clause tag;
    tag

  let add_many (solver:problem) clauses : tag list =
    (* clauses |> List.map (fun c ->
      let tag = next_numbering solver in
      Z3.Solver.assert_and_track solver.prob c tag;
      tag
    ) *)
    let tags = List.map (fun _ -> next_numbering solver) clauses in
    Z3.Solver.assert_and_track_l solver.prob clauses tags;
    tags

  let check solver = check solver.prob

  let check_assumptions solver assumptions = check_assumptions solver.prob assumptions

  let push solver = push solver.prob

  let pop solver = pop solver.prob

  let clear solver = clear solver.prob

  let unsat_core solver =
    Z3.Solver.get_unsat_core solver.prob
end



let make_problem_fast state = 
  (* Z3.Solver.mk_solver state.ctx None *)
  Z3.Solver.mk_solver_t state.ctx state.fast_tactic

(* module Fast = struct
  type _problem = problem
  type problem = _problem

  let make_problem = make_problem_fast

  let add = add

  let add_many = add_many

  let check = check

  let check_assumptions = check_assumptions

  let push = push

  let pop = pop

  let clear = clear
end *)



let eval_term model x =
  match
  Z3.Model.eval model x false (* ??? what is the last parameter, docs don't say. UPDATE: it's "completion, default false" *)
  with
  | Some x -> x
  | None -> failwith "SMTSolver_Z3.eval: cannot evaluate"

let eval_term_bool model x =
  let r = eval_term model x in
  match Z3.Boolean.get_bool_value r with
  | Z3enums.L_FALSE -> Some false
  | Z3enums.L_TRUE  -> Some true
  | Z3enums.L_UNDEF -> None

let eval_clause = eval_term

let eval_clause_bool = eval_term_bool



let simplify_term (*state*) x =
  Z3.Expr.simplify x None

let simplify_clause = simplify_term





module Term = struct
  type t = term

  let equal a b = 
    Z3.Expr.equal a b

  let compare a b = 
    Z3.Expr.compare a b

  let hash x = 
    failwith "unimplemented" [@@alert unimplemented "unimplemented"]
end

module Clause = struct
  type t = clause

  let equal a b = 
    Z3.Expr.equal a b

  let compare a b = 
    Z3.Expr.compare a b

  let hash x = 
    failwith "unimplemented" [@@alert unimplemented "unimplemented"]
end
