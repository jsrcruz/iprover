open Lib

type term = Term.term
type atom = Term.atom
type lit  = Term.lit

type symbol = Symbol.symbol

let uid_ref = ref 0
let get_next_uid () = 
  let r = !uid_ref in
  incr uid_ref;
  r

type t = {
  uid: int;
  terms: term -> term -> PartialOrd.t;
  atoms: atom -> atom -> PartialOrd.t;
  lits: lit -> lit -> PartialOrd.t;
  oriented: lit -> PartialOrd.t;
}

type terms = term -> term -> PartialOrd.t
type atoms = atom -> atom -> PartialOrd.t
type lits = lit -> lit -> PartialOrd.t
type oriented = lit -> PartialOrd.t

let make ~terms ~atoms ~lits ~oriented = {
  uid = get_next_uid ();
  terms; atoms; lits; oriented;
}

