open Lib
open Logic_interface



(*----- debug modifiable part-----*)

let dbg_flag = false

type dbg_gr = 
  | D_context
  | D_contextsize
  | D_triv
  | D_subs
  | D_sub_subs
  | D_fw_subs
  | D_bw_subs
  | D_fw_subs_res
  | D_bw_subs_res
  | D_fw_sub_subs
  | D_bw_sub_subs
  | D_unit_subs
  (* | D_hybrid_subs *)
  | D_fw_unit_subs
  | D_bw_unit_subs
  (* | D_fw_hybrid_subs
  | D_bw_hybrid_subs *)
  | D_demod
  | D_demod2
  | D_lightnorm
  | D_joinability
  | D_joinability2
  | D_acnorm
  | D_smt
  | D_fw_ac_demod
  | D_fw_ac_demod2
  | D_bw_ac_demod

let dbg_gr_to_str = function 
  | D_context -> "context"
  | D_contextsize -> "contextsize"
  | D_triv -> "triv"
  | D_subs -> "subs"
  | D_sub_subs -> "sub_subs"
  | D_fw_subs -> "fw_subs"
  | D_bw_subs -> "bw_subs"
  | D_fw_subs_res -> "fw_subs_res"
  | D_bw_subs_res -> "bw_subs_res"
  | D_fw_sub_subs -> "fw_sub_subs"
  | D_bw_sub_subs -> "bw_sub_subs"
  | D_unit_subs -> "unit_subs"
  (* | D_hybrid_subs -> "hybrid_subs" *)
  | D_fw_unit_subs -> "fw_unit_subs"
  | D_bw_unit_subs -> "bw_unit_subs"
  (* | D_fw_hybrid_subs -> "fw_hybrid_subs"
  | D_bw_hybrid_subs -> "bw_hybrid_subs" *)
  | D_demod -> "demod"
  | D_demod2 -> "demod"
  | D_lightnorm -> "lightnorm"
  | D_joinability -> "joinability"
  | D_joinability2 -> "joinability"
  | D_acnorm -> "acnorm"
  | D_smt -> "smt"
  | D_fw_ac_demod -> "fw_ac_demod"
  | D_fw_ac_demod2 -> "fw_ac_demod"
  | D_bw_ac_demod -> "bw_ac_demod"

let dbg_groups = [

  D_context;
  (* D_contextsize; *)
  D_triv;
  D_subs;

  D_sub_subs;
  D_fw_subs;
  D_bw_subs;
  D_fw_subs_res;
  D_bw_subs_res;
  D_fw_sub_subs;
  D_bw_sub_subs;
  D_unit_subs;
  (* D_hybrid_subs; *)
  D_fw_unit_subs;
  D_bw_unit_subs;
  (* D_fw_hybrid_subs; *)
  (* D_bw_hybrid_subs; *)

  D_demod;
  D_demod2;
  D_lightnorm;
  D_joinability;
  D_joinability2;
  D_acnorm;
  D_smt;
  D_fw_ac_demod;
  D_bw_ac_demod;
  D_fw_ac_demod2;
]
    
let module_name = "simplify_new"

(*----- debug fixed part --------*)

let () = dbg_flag_msg dbg_flag module_name

let dbg group str_lazy = 
  Lib.dbg_out_pref dbg_flag dbg_groups group dbg_gr_to_str module_name str_lazy

let dbg_env group f = 
  Lib.dbg_env_set dbg_flag dbg_groups group f
  
(*----- debug -----*)



(* ------- *)
(* Indices *)
(* ------- *)

(* Modules *)
module FwDemodIndex_M = DemodulationIndexPerfect
module BwDemodIndex_M = DemodulationIndexPerfect
module SubsumptionIndexTop_M = SubsumptionIndex
module SubsumptionIndex_M = SubsumptionIndexTop_M.SCFVIndexNoTrack
module SubsetSubsumptionIndex_M = SubsetSubsume_new
module LightNormIndex_M = LightNormalisationIndex
(* module HybridSubsumptionIndex_M = HybridSubsumptionIndex *)
module UnitSubsumptionIndex_M = UnitSubsumptionIndex
module ACDemodIndex_M = ACDemodulationIndex

(* Index tag *)
module Index_tag = struct
  (* Types of index *)
  type t = 
    | FwDemod
    | BwDemod
    (* | Subs *)
    | SubsetSubs
    | LightNorm
    | LightNormNoreduce
    (* | ProbProps *)
    | SMTIncr
    | SMTSet
    | UnitSubs
    | NonunitSubs
    (* | HybridSubs *)
    | FwACDemod
    | BwACDemod

  let to_string = function
    | FwDemod -> "FwDemod"
    | BwDemod -> "BwDemod"
    (* | Subs -> "Subs" *)
    | SubsetSubs -> "SubsetSubs"
    | LightNorm -> "LightNorm"
    | LightNormNoreduce -> "LightNormNoreduce"
    (* | ProbProps -> "ProbProps" *)
    | SMTIncr -> "SMTIncr"
    | SMTSet -> "SMTSet"
    | UnitSubs -> "UnitSubs"
    | NonunitSubs -> "NonunitSubs"
    (* | HybridSubs -> "HybridSubs" *)
    | FwACDemod -> "FwACDemod"
    | BwACDemod -> "BwACDemod"

  let to_int = function
    | FwDemod -> 1
    | BwDemod -> 2
    (* | Subs -> 3 *)
    | SubsetSubs -> 3
    | LightNorm -> 4
    | LightNormNoreduce -> 5
    (* | ProbProps -> 6 *)
    | SMTIncr -> 6
    | SMTSet -> 7
    | UnitSubs -> 8
    | NonunitSubs -> 9
    (* | HybridSubs -> 10 *)
    | FwACDemod -> 10
    | BwACDemod -> 11

  let of_int = function
    | 1 -> FwDemod
    | 2 -> BwDemod
    (* | 3 -> Subs *)
    | 3 -> SubsetSubs
    | 4 -> LightNorm
    | 5 -> LightNormNoreduce
    (* | 6 -> ProbProps *)
    | 6 -> SMTIncr
    | 7 -> SMTSet
    | 8 -> UnitSubs
    | 9 -> NonunitSubs
    (* | 10 -> HybridSubs *)
    | 10 -> FwACDemod
    | 11 -> BwACDemod
    | x -> invalid_arg (sprintf "Simplify.Index_tag.of_int: %d is not in range" x)

  let list = [
    FwDemod;
    BwDemod;
    (* Subs; *)
    SubsetSubs;
    LightNorm;
    LightNormNoreduce;
    (* ProbProps; *)
    SMTIncr;
    SMTSet;
    UnitSubs;
    NonunitSubs;
    (* HybridSubs; *)
    FwACDemod;
    BwACDemod;
  ]

  let iter f = 
    f 1 FwDemod;
    f 2 BwDemod;
    (* Subs; *)
    f 3 SubsetSubs;
    f 4 LightNorm;
    f 5 LightNormNoreduce;
    (* ProbProps; *)
    f 6 SMTIncr;
    f 7 SMTSet;
    f 8 UnitSubs;
    f 9 NonunitSubs;
    (* HybridSubs; *)
    f 10 FwACDemod;
    f 11 BwACDemod;
    ()

  let () = dassert (fun () ->
    list |> List.iter (fun x -> dassert (fun () -> x == (x |> to_int |> of_int)));
    iter (fun i x -> dassert (fun () -> i == to_int x && of_int i == x));
    true
  )
end

(* (* Forward declaration of Set is needed here (just types though) *)
module type Set = sig
  module Context : sig
    type elt = {
      clause: clause;
      param: Bit_vec.bit_vec;
    }

    type t = elt BCHtbl.t
  end

  type t = {
    (* Indices *)
    fw_demod_index: FwDemodIndex_M.t;
    bw_demod_index: BwDemodIndex_M.t;
    subsumption_index: SubsumptionIndex_M.index;
    subset_subsumption_index: SubsetSubsumptionIndex_M.index;

    (* Context *)
    context: Context.t;
  }
end *)

(* module Index *)



(* ------------------ *)
(* Simplification set *)
(* ------------------ *)

(* Options *)
type options = {
  order: ordering;
  demod_completeness_check: Options.Demod_check.t;
  eq_types: Symbol.sym_set;
  demod_use_ground: bool;
  ac_symbols: AC.Table.t (* ref *);
  smt_check_interval: int;
  subs_bck_mult: int;
}

module Set = struct
  module Context = struct
    type elt = {
      clause: clause;
      param: Bit_vec.bit_vec;
    }

    type t = {
      mutable map: elt BCMap.t;
      mutable size: int;
      mutable dead: int;
    }

    (* More informative exceptions, for pattern matching *)
    exception Is_mem
    exception Is_dead
    exception Not_mem

    let empty () = {
      map = BCMap.empty; (* 5323 *) (* 16127 *)
      size = 0;
      dead = 0;
    }

    let incr_size context = context.size <- context.size + 1
    let incr_dead context = context.dead <- context.dead + 1

    (* exception Clause_in_index *)

    (* Set the [index] flag on [clause] in context, adding to context if it wasn't there at all. Raises Failure if clause already in index. *)
    let add context index clause = 
      (* [%dbg D_context (sprintf "added %s to %s" (Clause.to_string_tptp clause) (Index_tag.to_string index))]; *)
      let index_n = Index_tag.to_int index in
      let bv_old = 
        match BCMap.find_opt clause context.map with
        | Some x ->
          if Bit_vec.get index_n x.param then (
            dbg D_context @@ lazy (sprintf "Simplify.Set.add: already in index %s" (Index_tag.to_string index));
            raise Is_mem
          )
          else if Bit_vec.get 0 x.param then (
            dbg D_context @@ lazy "Simplify.Set.add: already dead";
            raise Is_dead
          )
          else
            x.param
        | None ->
          Bit_vec.false_vec
      in
      let elt_new = {
        clause = clause; 
        param = Bit_vec.set true index_n bv_old;
      } 
      in
      context.map <- BCMap.add clause elt_new context.map;
      incr_size context

    (* Add a new "dead" clause. This is for e.g. demodulation intermediate steps. *)
    let add_dead context clause = 
      context.map <- context.map |> BCMap.update clause (fun x ->
        match x with
        | Some _ ->
          dbg D_context @@ lazy (sprintf "Simplify.Set.add_dead: already in context");
          raise Is_mem
        | None ->
          let elt = {
            clause = clause; 
            param = Bit_vec.false_vec |> Bit_vec.set true 0;
          } 
          in
          Some elt 
      );
      incr_size context;
      incr_dead context

    (* Set the "dead" flag on [clause] in context. Raises Failure if already dead or not in context. *)
    let set_dead context clause = 
      context.map <- context.map |> BCMap.update clause (fun y ->
        let elt_old = 
          match y with
          | Some x when Bit_vec.get 0 x.param ->
            dbg D_context @@ lazy "Simplify.Set.set_dead: already dead";
            raise Is_dead
          | Some x ->
            x
          | None ->
            dbg D_context @@ lazy "Simplify.Set.set_dead: not in context";
            raise Not_mem
        in
        let elt_new = {elt_old with 
          param = Bit_vec.set true 0 elt_old.param;
        } 
        in
        Some elt_new
      );
      incr_dead context

    (* let set_dead_and_remove_indices context clause =
      assert false [@@ppwarning "unimplemented"] *)

    (* Check if [index] flag in [context] is set. [true] if yes, [false] if unset or not in context. *)
    let mem context index clause =
      try
        (BCMap.find clause context.map)
        .param
        |> Bit_vec.get (Index_tag.to_int index)
      with Not_found ->
        false

    (* Check if clause is in context (in any index, that is). *)
    let mem_any context clause =
      BCMap.mem clause context.map

    (* Check if "dead" flag in [context] is set. [true] if yes, [false] if unset or not in context. *)
    let is_dead context clause =
      try
        (BCMap.find clause context.map)
        .param
        |> Bit_vec.get 0
      with Not_found ->
        false

    type status = Live | Dead | NotMem
    let status context clause = 
      try
        (BCMap.find clause context.map)
        .param
        |> Bit_vec.get 0
        |> function true -> Dead | false -> Live
      with Not_found ->
        NotMem


    let list_all context : clause list =
      BCMap.fold (fun _ {clause} acc ->
        clause :: acc
      ) context.map []

    let list_dead context : clause list =
      BCMap.fold (fun _ {param;clause} acc ->
        if Bit_vec.get 0 param then
          clause :: acc
        else
          acc
      ) context.map []

    let list_nondead context : clause list =
      BCMap.fold (fun _ {param;clause} acc ->
        if Bit_vec.get 0 param then
          acc
        else
          clause :: acc
      ) context.map []

    let size context = 
      let res = context.size in
      dassert (fun () -> 
        let res_dbg = BCMap.cardinal context.map in
        dbg D_contextsize @@ lazy (sprintf "Simplify_new.Set.size: res=%d res_dbg=%d" res res_dbg);
        res = res_dbg
      );
      res

    let size_dead context = 
      let res = context.dead in
      dassert (fun () -> 
        let res_dbg = 
          BCMap.fold (fun _ {param;_} acc ->
            if Bit_vec.get 0 param then
              acc + 1
            else
              acc
          ) context.map 0
        in
        dbg D_contextsize @@ lazy (sprintf "Simplify_new.Set.size_dead: res=%d res_dbg=%d" res res_dbg);
        res = res_dbg
      );
      res

    let size_nondead context = 
      let res = context.size - context.dead in
      dassert (fun () -> 
        let res_dbg = 
          BCMap.fold (fun _ {param;_} acc ->
            if Bit_vec.get 0 param then
              acc
            else
              acc + 1
          ) context.map 0
        in
        dbg D_contextsize @@ lazy (sprintf "Simplify_new.Set.size_nondead: res=%d res_dbg=%d" res res_dbg);
        res = res_dbg
      );
      res


    (* Iterative interface *)
    let iter (f: clause -> bool -> unit) context = 
      context.map |> BCMap.iter (fun _ {param;clause} ->
        let is_dead = Bit_vec.get 0 param in
        f clause is_dead
      )


    (* Unset the [index] flag on [clause] in context. Raises Failure if clause already not in index. *)
    let remove context index clause = 
      let index_n = Index_tag.to_int index in
      context.map <- context.map |> BCMap.update clause (fun y ->
        let bv_old = 
          match y with
          | Some x when not @@ Bit_vec.get index_n x.param ->
            dbg D_context @@ lazy (sprintf "Simplify.Set.remove: already not in index %s" (Index_tag.to_string index));
            raise Not_mem
          | Some x ->
            x.param
          | None ->
            dbg D_context @@ lazy (sprintf "Simplify.Set.remove: already not in context %s" (Index_tag.to_string index));
            raise Not_mem
        in
        let elt_new = {
          clause = clause;
          param = Bit_vec.set false index_n bv_old;
        } 
        in
        Some elt_new
      )

    (* let remove_full context clause =
      context.map <- BCMap.remove clause context.map *)

    (* List of indices where clause is in *)
    let indices context clause : Index_tag.t list =
      try
        let bv = (BCMap.find clause context.map).param in
        Index_tag.list |> List.filter (fun x ->
          Bit_vec.get (Index_tag.to_int x) bv
        )
      with Not_found -> 
        []
  end



  (* Disable for release builds! *)
  let debug_index_flag = false

  (* For efficiency reasons this has to be stored here, and therefore it has 
     to be declared at this point *)
  exception Return of (term * (clause * Subst.subst) option)
  let demod_func ~order = fun candidates s ->
    try 
      candidates (fun _term subst lst ->
        lst |> List.iter (fun (pos, eq_lit, eq_clause) ->
          (* if Clause.Bc.(clause == eq_clause) then () else *)
          let l,r = Term.Eq.decompose_atom_exn eq_lit in
          let l,r = Term.Eq.regularize_pos pos l r in
          dassert (fun () -> _term == l);
          dbg D_demod @@ lazy (sprintf "func: l: %s r: %s s: %s" (Term.to_string l) (Term.to_string r) (Term.to_string s));
          let result = Inference_rules.demodulation_bare_nomatch ~order eq_lit l r s subst in
          begin match result with
          | Some y -> 
            dbg D_demod @@ lazy (sprintf "Hit %s to %s" (Term.to_string s) (Term.to_string y));
            (* dbg D_demod @@ lazy (sprintf "via %s" (Clause.to_string_tptp eq_clause)); *)
            (* Need also to return lhs of original equation for encompassment ordering *)
            raise_notrace @@ Return (y, Some (eq_clause, subst))
          | None -> ()
          end
        )
      );
      (s, None)
    with Return x -> x

  type t = {
    unique_id: int;

    order: ordering;

    (* Indices *)
    fw_demod_index: FwDemodIndex_M.t;
    bw_demod_index: BwDemodIndex_M.t;
    fw_demod_func: ((term -> Subst.subst -> (int * lit * clause) list -> unit) -> unit) -> term -> term * (clause * Subst.subst) option;
    (* fw_demod_func: (term * Subst.subst * (int * lit * clause) list) list -> term -> term * (clause * Subst.subst) option; *)
    subsumption_index: SubsumptionIndex_M.index;
    subset_subsumption_index: SubsetSubsumptionIndex_M.index;
    lightnorm_index: LightNormIndex_M.t;
    ac_symbols: AC.Table.t (* ref *);
    smt_incremental: SMT_incremental.set;
    smt_set: SMTSolver.problem;
    mutable smt_set_count: int;
    (* hybrid_subs_index: HybridSubsumptionIndex_M.t; *)
    unit_subs_index: UnitSubsumptionIndex_M.t;
    ac_demod_index: ACDemodIndex_M.t;

    fw_demod_index_debug: DemodulationIndex.t;

    (* Context *)
    (* mutable *) context: Context.t;
    options: options;
  }

  exception Is_mem  = Context.Is_mem
  exception Is_dead = Context.Is_dead
  exception Not_mem = Context.Not_mem

  let next_unique_id = ref 0

  let create (options:options) = let order = options.order in {
    unique_id = (let x = !next_unique_id in incr next_unique_id; x);
    order;
    fw_demod_index = FwDemodIndex_M.create ~order ~eq_types:options.eq_types ~use_ground:options.demod_use_ground ();
    bw_demod_index = BwDemodIndex_M.create ~order ~eq_types:options.eq_types ~use_ground:options.demod_use_ground ();
    fw_demod_func = demod_func ~order;
    subsumption_index = SubsumptionIndex_M.create();
    subset_subsumption_index = SubsetSubsumptionIndex_M.create();
    lightnorm_index = LightNormIndex_M.create ~order ~eq_types:options.eq_types ();
    ac_symbols = options.ac_symbols;
    smt_incremental = SMT_incremental.make_set !GlobalSMT.state;
    smt_set = SMTSolver.make_problem !GlobalSMT.state;
    smt_set_count = 0;
    (* hybrid_subs_index = HybridSubsumptionIndex_M.create(); *)
    unit_subs_index = UnitSubsumptionIndex_M.create();
    ac_demod_index = ACDemodIndex_M.create ~ac_symbols:options.ac_symbols ~order ~subs_bck_mult:options.subs_bck_mult ~complete:(options.demod_completeness_check != Options.Demod_check.Off) ();

    fw_demod_index_debug = DemodulationIndex.create ~order ~eq_types:options.eq_types ~use_ground:options.demod_use_ground ();

    context = Context.empty ();
    options;
  }

  let clear x = 
    dbg D_context @@ lazy (sprintf "[id %d] clear" (x.unique_id));
    FwDemodIndex_M.clear x.fw_demod_index;
    BwDemodIndex_M.clear x.bw_demod_index;
    LightNormIndex_M.clear x.lightnorm_index;
    if debug_index_flag then DemodulationIndex.clear x.fw_demod_index_debug;
    (* HybridSubsumptionIndex_M.clear x.hybrid_subs_index; *)
    UnitSubsumptionIndex_M.clear x.unit_subs_index;
    { 
      x with
      subsumption_index = SubsumptionIndex_M.create();
      subset_subsumption_index = SubsetSubsumptionIndex_M.create();
      context = Context.empty ();
    }

  let add state index clause =
    dbg D_context @@ lazy (sprintf "[id %d] add %s to %s" (state.unique_id) (Clause.to_string_tptp clause) (Index_tag.to_string index));
    (* state.context <- *) Context.add state.context index clause

  let remove state index clause =
    dbg D_context @@ lazy (sprintf "[id %d] remove %s" (state.unique_id) (Clause.to_string_tptp clause));
    (* state.context <- *) Context.remove state.context index clause

  (* let remove_full state clause =
    dbg D_context @@ lazy (sprintf "[id %d] remove_full %s" (state.unique_id) (Clause.to_string_tptp clause));
    (* state.context <- *) Context.remove_full state.context clause *)

  let add_dead state clause = 
    dbg D_context @@ lazy (sprintf "[id %d] add %s as dead" (state.unique_id) (Clause.to_string_tptp clause));
    (* state.context <- *) Context.add_dead state.context clause

  let set_dead state clause = 
    dbg D_context @@ lazy (sprintf "[id %d] set_dead %s" (state.unique_id) (Clause.to_string_tptp clause));
    (* state.context <- *) Context.set_dead state.context clause

(*
  let add_dead state clause = 
    state.context <- Context.add_dead state.context clause
*)
  (* let set_dead_and_remove state clause =
    set_dead *)

  let is_dead state clause =
    let res = Context.is_dead state.context clause in
    dbg D_context @@ lazy (sprintf "[id %d] is_dead %s = %B" (state.unique_id) (Clause.to_string_tptp clause) (res));
    res

  type status = Context.status = Live | Dead | NotMem
  let status state clause =
    Context.status state.context clause

  let list_dead state =
    Context.list_dead state.context 

  let list_all state =
    Context.list_all state.context 

  let list_nondead state = 
    Context.list_nondead state.context

  let list_nondead state = 
    Context.list_nondead state.context

  let size state = 
    Context.size state.context

  let size_dead state = 
    Context.size_dead state.context

  let size_nondead state = 
    Context.size_nondead state.context

  let iter f state = 
    Context.iter f state.context

  let mem state index clause =
    let res = Context.mem state.context index clause in
    dbg D_context @@ lazy (sprintf "[id %d] mem %s = %B" (state.unique_id) (Clause.to_string_tptp clause) (res));
    res

  let mem_any state clause = 
    let res = Context.mem_any state.context clause in
    dbg D_context @@ lazy (sprintf "[id %d] mem_any %s = %B" (state.unique_id) (Clause.to_string_tptp clause) (res));
    res

  let indices state clause = 
    Context.indices state.context clause

  let unique_id state = 
    state.unique_id
end

type set = Set.t



(* ------- *)
(* Indexes *)
(* ------- *)

(* Unified interface for indices *)
module type Index = sig
  (* Type of index that this rule reads/writes to *)
  val index : Index_tag.t
  (* Adds clause to index in set *)
  val add : Set.t -> clause -> unit
  (* Removes clause from index in set *)
  val remove : Set.t -> clause -> unit
  (* (* Checks if clause is in set *)
  val mem : Set.t -> clause -> bool *)
end



(* Exported modules handle adding to context *)
module MakeIndex (Index: Index) : sig include Index val add' : set -> clause -> unit val remove' : set -> clause -> unit end = struct
  let index = Index.index

  let add' = Index.add

  let remove' = Index.remove

  let add state clause = 
    (* undefined () *)
    (* dbg D_triv @@ lazy "warning: add"; *)
    Index.add state clause;
    Set.add state index clause

  let remove state clause = 
    (* undefined () *)
    (* dbg D_triv @@ lazy "warning: remove"; *)
    Index.remove state clause;
    Set.remove state index clause
end

module NonunitSubsIndex = MakeIndex(struct
  let index = Index_tag.NonunitSubs

  (* Add to / remove from fv index *)
  let add_fv state clause = 
    (* let feature_list = (get_feature_list clause) in *)
    (* let com_feature_list = compress_feature_list feature_list in *)
    dbg D_subs @@ lazy (sprintf "add: %s" (Clause.to_string_tptp clause));
    SubsumptionIndex_M.add_clause Set.(state.subsumption_index) clause

  let remove_fv state clause =
    dbg D_subs @@ lazy (sprintf "remove: %s" (Clause.to_string_tptp clause));
    (* try with Not_found -> () *)
    SubsumptionIndex_M.remove_clause Set.(state.subsumption_index) clause

  let add state clause = 
    if not @@ Clause.is_unit clause then add_fv state clause

  let remove state clause = 
    if not @@ Clause.is_unit clause then remove_fv state clause
end)

module UnitSubsIndex = MakeIndex(struct
  let index = Index_tag.UnitSubs

  let add state clause = 
    dbg D_unit_subs @@ lazy (sprintf "add: %s" (Clause.to_string_tptp clause));
    UnitSubsumptionIndex_M.add_clause Set.(state.unit_subs_index) clause

  let remove state clause =
    dbg D_unit_subs @@ lazy (sprintf "remove: %s" (Clause.to_string_tptp clause));
    UnitSubsumptionIndex_M.remove_clause Set.(state.unit_subs_index) clause
end)

(* Add to unit index, and if nonunit add also to fv *)
(* module SubsumptionIndex = struct
  let index = unit  (* Depends on two indices... *)

  let add state clause = 
    if Clause.is_unit clause then (
      UnitSubsIndex.add state clause;
    ) else (
      UnitSubsIndex.add state clause;
      FVSubsumptionIndex.add state clause;
    )

  let remove state clause = 
    if Clause.is_unit clause then (
      UnitSubsIndex.remove state clause;
    ) else (
      UnitSubsIndex.remove state clause;
      FVSubsumptionIndex.remove state clause;
    )
end *)



module SubsetSubsumptionIndex = MakeIndex(struct
  let index = Index_tag.SubsetSubs

  let add state clause = 
    dbg D_sub_subs @@ lazy (sprintf "add: %s" (Clause.to_string_tptp clause));
    dbg_env D_sub_subs (fun () ->
      try 
        let by_clause = SubsetSubsumptionIndex_M.is_subsumed Set.(state.subset_subsumption_index) clause in
        dbg D_fw_sub_subs @@ lazy (sprintf "add subsumed: %s by: %s" (Clause.to_string_tptp clause) (Clause.to_string_tptp by_clause));
      with Not_found -> ()
    );
    dbg_env D_sub_subs (fun () ->
      try
        let subsumed_clauses = 
          SubsetSubsumptionIndex_M.find_subsumed Set.(state.subset_subsumption_index) clause
          |> List.X.remove_all ~eq:Clause.Bc.equal clause 
        in
        subsumed_clauses |> List.iter (fun x -> 
          dbg D_bw_sub_subs @@ lazy (sprintf "add subsumed: %s by: %s" (Clause.to_string_tptp x) (Clause.to_string_tptp clause));
        )
      with SubsetSubsumptionIndex_M.No_subsumed -> ()
    );
    SubsetSubsumptionIndex_M.add_clause Set.(state.subset_subsumption_index) clause

  let remove state clause =
    dbg D_sub_subs @@ lazy (sprintf "remove: %s" (Clause.to_string_tptp clause));
    SubsetSubsumptionIndex_M.remove Set.(state.subset_subsumption_index) clause
end)



module FwDemodIndex = MakeIndex(struct
  let index = Index_tag.FwDemod

  (** Add to list of unit equations *)
  let add state clause =
    dbg D_demod @@ lazy (sprintf "fw add: %s" (Clause.to_string_tptp clause));
    FwDemodIndex_M.add_equation Set.(state.fw_demod_index) clause;
    if Set.debug_index_flag then DemodulationIndex.add_equation Set.(state.fw_demod_index_debug) clause

  let remove state clause =
    dbg D_demod @@ lazy (sprintf "fw remove: %s" (Clause.to_string_tptp clause));
    try 
      FwDemodIndex_M.elim_equation Set.(state.fw_demod_index) clause;
      if Set.debug_index_flag then DemodulationIndex.elim_equation Set.(state.fw_demod_index_debug) clause
    with Not_found -> ()
end)

module BwDemodIndex = MakeIndex(struct
  let index = Index_tag.BwDemod

  (** Add to list of bwd clauses *)
  let add state clause =
    dbg D_demod @@ lazy (sprintf "bw add: %s" (Clause.to_string_tptp clause));
    BwDemodIndex_M.add_bwd_clause Set.(state.bw_demod_index) clause

  let remove state clause =
    dbg D_demod @@ lazy (sprintf "bw remove: %s" (Clause.to_string_tptp clause));
    try 
      BwDemodIndex_M.elim_bwd_clause Set.(state.bw_demod_index) clause;
    with Not_found -> ()
end)

module LightNormIndex = MakeIndex(struct
  let index = Index_tag.LightNorm

  let add state clause =
    dbg D_lightnorm @@ lazy (sprintf "add: %s" (Clause.to_string_tptp clause));
    LightNormIndex_M.add_and_update Set.(state.lightnorm_index) clause

  let remove state clause =
    dbg D_lightnorm @@ lazy (sprintf "remove: %s" (Clause.to_string_tptp clause));
    try LightNormIndex_M.remove Set.(state.lightnorm_index) clause with Not_found -> ()
end)

module LightNormIndexNoreduce = MakeIndex(struct
  (* include LightNormIndex *)
  let index = Index_tag.LightNormNoreduce

  let add state clause =
    dbg D_lightnorm @@ lazy (sprintf "add: %s (noreduce)" (Clause.to_string_tptp clause));
    LightNormIndex_M.add_bare Set.(state.lightnorm_index) clause

  let remove state clause =
    dbg D_lightnorm @@ lazy (sprintf "remove: %s (noreduce)" (Clause.to_string_tptp clause));
    try LightNormIndex_M.remove Set.(state.lightnorm_index) clause with Not_found -> ()
end)

module SMTIncrIndex = MakeIndex(struct
  let index = Index_tag.SMTIncr

  let add state clause =
    dbg D_smt @@ lazy (sprintf "add: %s" (Clause.to_string_tptp clause));
    let unsat = SMT_incremental.add Set.(state.smt_incremental) clause in
    if unsat then (
      (* Currently, when we find SMT contradiction, we just dump the non-dead clauses and let the UC solver find an unsat core. This may be refined in the future *)
      raise @@ Unsatisfiable_gr_smt_na (Set.list_nondead state)
    )

  let remove state clause =
    dbg D_smt @@ lazy (sprintf "[UNIMPLEMENTED] remove: %s" (Clause.to_string_tptp clause));
    ()
end)

module SMTSetIndex = MakeIndex(struct
  let index = Index_tag.SMTSet

  let add state clause =
    dbg D_smt @@ lazy (sprintf "add: %s" (Clause.to_string_tptp clause));
    SMTSolver.add Set.(state.smt_set) (SMTSolver.clause_to_smt !GlobalSMT.state clause);
    state.smt_set_count <- state.smt_set_count + 1;
    if state.smt_set_count > state.options.smt_check_interval then (
      begin match SMTSolver.check state.smt_set with
      | SMTSolver.Unsat -> raise @@ Unsatisfiable_gr_smt_na (Set.list_all state);
      | SMTSolver.Sat | SMTSolver.Unknown -> ()
      end;
      state.smt_set_count <- state.smt_set_count - state.options.smt_check_interval;
    )

  let remove state clause =
    dbg D_smt @@ lazy (sprintf "[UNIMPLEMENTED] remove: %s" (Clause.to_string_tptp clause));
    ()
end)

(* module HybridSubsIndex = struct
  let index = Index_tag.HybridSubs

  let add state clause = 
    dbg D_hybrid_subs @@ lazy (sprintf "add: %s" (Clause.to_string_tptp clause));
    HybridSubsumptionIndex_M.add_clause Set.(state.hybrid_subs_index) clause

  let remove state clause =
    dbg D_hybrid_subs @@ lazy (sprintf "remove: %s" (Clause.to_string_tptp clause));
    HybridSubsumptionIndex_M.remove_clause Set.(state.hybrid_subs_index) clause
end *)

module FwACDemodIndex = MakeIndex(struct
  let index = Index_tag.FwACDemod

  let add (state: Set.t) clause =
    dbg D_fw_ac_demod @@ lazy (sprintf "add: %s" (Clause.to_string_tptp clause));
    ACDemodIndex_M.add_equation Set.(state.ac_demod_index) clause

  let remove state clause =
    dbg D_fw_ac_demod @@ lazy (sprintf "remove: %s" (Clause.to_string_tptp clause));
    ACDemodIndex_M.elim_equation Set.(state.ac_demod_index) clause
end)

module BwACDemodIndex = MakeIndex(struct
  let index = Index_tag.BwACDemod

  let add state clause =
    dbg D_bw_ac_demod @@ lazy (sprintf "add: %s" (Clause.to_string_tptp clause));
    ACDemodIndex_M.add_bwd_clause Set.(state.ac_demod_index) clause

  let remove state clause =
    dbg D_bw_ac_demod @@ lazy (sprintf "remove: %s" (Clause.to_string_tptp clause));
    ACDemodIndex_M.elim_bwd_clause Set.(state.ac_demod_index) clause
end)



(* Due to the difficulty in making mutually recursive modules, the "set_dead_and_remove" function has to be kind of outside *)
let set_dead_and_remove (state:Set.t) (clause:clause) : unit =
  dbg D_context @@ lazy (sprintf "[id %d] set_dead_and_remove: %s" (state.unique_id) (Clause.to_string_tptp clause));

  let context = state.context in
  context.map <- context.map |> BCMap.update clause (fun (value: Set.Context.elt option) ->
    let elt_old = 
      match value with
      | Some x when Bit_vec.get 0 x.param ->
        dbg D_context @@ lazy "Simplify.Set.set_dead_and_remove: already dead";
        raise Set.Is_dead
      | Some x ->
        x
      | None ->
        dbg D_context @@ lazy "Simplify.Set.set_dead_and_remove: not in context";
        raise Set.Not_mem
    in

    Index_tag.iter (fun i x ->
      if Bit_vec.get i elt_old.param then (
        match x with
        | FwDemod -> FwDemodIndex.remove' state clause
        | BwDemod -> BwDemodIndex.remove' state clause
        (* | Subs -> assert false (* FVSubsumptionIndex.remove' state clause *) *)
        | SubsetSubs -> SubsetSubsumptionIndex.remove' state clause
        | LightNorm -> LightNormIndex.remove' state clause
        | LightNormNoreduce -> LightNormIndexNoreduce.remove' state clause
        (* | ProbProps -> () *)
        | SMTIncr -> SMTIncrIndex.remove' state clause
        | SMTSet -> SMTSetIndex.remove' state clause
        | UnitSubs -> UnitSubsIndex.remove' state clause
        | NonunitSubs -> NonunitSubsIndex.remove' state clause
        (* | HybridSubs -> HybridSubsIndex.remove' state clause *)
        | FwACDemod -> FwACDemodIndex.remove' state clause
        | BwACDemod -> BwACDemodIndex.remove' state clause
      )
    );

    let elt_new = {elt_old with 
      param = Bit_vec.false_vec |> Bit_vec.set true 0;
    } 
    in
    Some elt_new
  );
  Set.Context.incr_dead context

(* As above, but if not in index then set dead, and if already dead ignore *)
let force_dead_and_remove (state: Set.t) clause = 
  dbg D_context @@ lazy (sprintf "[id %d] force_dead_and_remove: %s" (state.unique_id) (Clause.to_string_tptp clause));

  let context = state.context in
  context.map <- context.map |> BCMap.update clause (fun (value: Set.Context.elt option) ->
    match value with
    (* Already dead *)
    | Some elt_old when Bit_vec.get 0 elt_old.param ->
      value

    (* Already non-dead *)
    | Some elt_old ->
      Set.Context.incr_dead context;
      Index_tag.iter (fun i x ->
        if Bit_vec.get i elt_old.param then (
          match x with
          | FwDemod -> FwDemodIndex.remove' state clause
          | BwDemod -> BwDemodIndex.remove' state clause
          (* | Subs -> assert false (* FVSubsumptionIndex.remove' state clause *) *)
          | SubsetSubs -> SubsetSubsumptionIndex.remove' state clause
          | LightNorm -> LightNormIndex.remove' state clause
          | LightNormNoreduce -> LightNormIndexNoreduce.remove' state clause
          (* | ProbProps -> () *)
          | SMTIncr -> SMTIncrIndex.remove' state clause
          | SMTSet -> SMTSetIndex.remove' state clause
          | UnitSubs -> UnitSubsIndex.remove' state clause
          | NonunitSubs -> NonunitSubsIndex.remove' state clause
          (* | HybridSubs -> HybridSubsIndex.remove' state clause *)
          | FwACDemod -> FwACDemodIndex.remove' state clause
          | BwACDemod -> BwACDemodIndex.remove' state clause
        )
      );

      let elt_new = {elt_old with 
        param = Bit_vec.false_vec |> Bit_vec.set true 0;
      } 
      in
      Some elt_new

    (* Not in context *)
    | None ->
      Set.Context.incr_size context;
      Set.Context.incr_dead context;
      let elt_new = Set.Context.{
        clause = clause;
        param = Bit_vec.false_vec |> Bit_vec.set true 0;
      }
      in
      Some elt_new
  )

let add_to_indices (state:Set.t) indices clause =
  dbg D_context @@ lazy (sprintf "[id %d] add_to_indices: %s" (state.unique_id) (Clause.to_string_tptp clause));

  let context = state.context in
  context.map <- context.map |> BCMap.update clause (fun (value:Set.Context.elt option) ->
    let elt_old = 
      match value with
      | Some x ->
        if Bit_vec.get 0 x.param then (
          dbg D_context @@ lazy "Simplify.add_to_indices: already dead";
          raise Set.Is_dead
        ) else (
          x
        )
      | None ->
        Set.Context.incr_size context;
        {param=Bit_vec.false_vec; clause}
    in

    let param_new = indices |> List.fold_left (fun param index ->
      let open Index_tag in
      if Bit_vec.get (to_int index) param then (
        (* Allow attempt to add to lightnorm twice, by just ignoring *)
        (* if index = LightNorm then (
          param
        ) else ( *)
          dbg D_context @@ lazy (sprintf "Simplify.add_to_indices: already in index %s" (to_string index));
          raise Set.Is_mem
        (* ) *)
      ) else (
        dbg D_context @@ lazy (sprintf "Adding to %s" (to_string index));
        begin match index with
        | FwDemod -> FwDemodIndex.add' state clause
        | BwDemod -> BwDemodIndex.add' state clause
        (* | Subs -> assert false (* FVSubsumptionIndex.add' state clause *) *)
        | SubsetSubs -> SubsetSubsumptionIndex.add' state clause
        | LightNorm -> LightNormIndex.add' state clause 
        | LightNormNoreduce -> LightNormIndexNoreduce.add' state clause 
        (* | ProbProps -> () *)
        | SMTIncr -> SMTIncrIndex.add' state clause
        | SMTSet -> SMTSetIndex.add' state clause
        | UnitSubs -> UnitSubsIndex.add' state clause
        | NonunitSubs -> NonunitSubsIndex.add' state clause
        (* | HybridSubs -> HybridSubsIndex.add' state clause *)
        | FwACDemod -> FwACDemodIndex.add' state clause
        | BwACDemod -> BwACDemodIndex.add' state clause
        end;
        Bit_vec.set true (to_int index) param
      )
    ) elt_old.param
    in

    let elt_new = {elt_old with param = param_new} in
    Some elt_new
  )



(* ---------- *)
(* Interfaces *)
(* ---------- *)

(* Results of simplifications *)

type fw_result =
  (* | Same  (* TODO Is this good to have? *) *)
  | Simplified of clause
  | Eliminated of clause list

module Fw_result = struct
  type t = fw_result

  (* Monadic bind. Useful to succinctly chain simplifications: in [simp1 x >>= simp2]
     if [simp1] returns [Simplified c], the c is fed to [simp2], if it returns 
     [Eliminated parents], the whole expression returns [Eliminated parents] 
     without trying [simp2]. *)
  let (>>=) x f = 
    match x with
    | Simplified c -> f c
    | Eliminated _ -> x

  (* Monadic return, for completeness *)
  let return x = Simplified x

  let rec fold l x =
    match l with
    | [] -> Simplified x
    | [hd] -> hd x
    | hd::tl -> 
      let result = hd x in
      begin match result with
      | Simplified x' -> 
        fold tl x'
      | Eliminated _ -> 
        result
      end

  let to_string_tptp = function
    | Simplified c -> Clause.to_string_tptp c
    | Eliminated _ -> "eliminated"

  (* let same a b =
    match a,b with
    | Simplified x, Simplified y -> x == y
    | _, _ -> false *)

  (* let equal ~eq a b =
    match a,b with
    | Simplified x, Simplified y -> eq x y
    | Eliminated x, Eliminated y -> BCSet.equal (BCSet.of_list x) (BCSet.of_list y)
    | Simplified _, Eliminated _ -> false
    | Eliminated _, Simplified _ -> false *)

  module O = struct 
    (* let (==) = 
      equal ~eq:(==)

    let (!=) a b = 
      not (a == b) *)

    let (>>=) = (>>=)

    let ( let* ) = (>>=)
  end



  (* Run until fixpoint *)
  (* Crude version *)
  let rec fix_point f x = 
    let result = f x in
    match result with
    | Simplified x' -> 
      if x' != x then fix_point f x' else result
    | Eliminated _ -> 
      result
end



(* More fine grained control: clause [from] is to be replaced by [add]. If 
   we have a big list of added clauses and removed clauses we don't know
   which corresponds to which. *)
type bw_result_elt = 
  | BwSimplified of {from: clause; into: clause}
  | BwEliminated of clause

type bw_result = bw_result_elt list

module Bw_result = struct
  type elt = bw_result_elt

  type t = bw_result

  let empty = []

  let (@) = (@)

  (* let rec fold l x =
    let rec f acc l x =
      match l with
      | [] -> acc
      | hd::tl -> 
        let result : t = hd x in
        let acc' : t = result @ acc in
        f acc' tl x
    in 
    match l with
    | [] -> {add=[]; remove=[]}
    | hd::tl -> 
      let result = hd x in
      f result tl x *)

  (* let map fs x = 
    List.map (fun f -> f x) fs *)

  let fold funcs clause = 
    List.concat_map (fun f -> f clause) funcs

  (* let to_string_tptp {add; remove} =
    sprintf "{\n  add" *)

  module O = struct
    let (@) = (@)
  end

  let handle_elt ~remove ~add x = 
    match x with
    | BwSimplified {from; into} -> 
      remove from; add into
    | BwEliminated c -> 
      remove c

  let handle ~remove ~add xs = 
    List.iter (handle_elt ~remove ~add) xs
end



(* type fwbw_result = {
  fw_result: fw_result;
  bw_result: bw_result;
} *)



(* Trivial rule, with no index *)
module type TrivRule = sig
  val simplify : clause -> fw_result
end

(* Fw simplification rule *)
module type FwRule = sig
  (* include Index *)
  (* val index : Index_tag.t *)
  val simplify : Set.t -> clause -> fw_result
end

(* Bw simplification rule *)
module type BwRule = sig
  (* include Index *)
  (* val index : Index_tag.t *)
  val simplify : Set.t -> clause -> bw_result
end

(* Fw and bw simplification rule *)
(* module type FwBwRule = sig
  include Rule
  val simplify_fw : Set.t -> clause -> fw_result
  val simplify_bw : Set.t -> clause -> bw_result
end *)

(* The "special" case of subset subsumption. Fw and bw simplification rule
   that triggers automatically on adding. *)
(* module type AutoFwBwRule = sig
  val add_and_simplify : set -> clause -> (fw_result * bw_result)
  val remove : set -> clause -> unit  (* TODO Also may need to do bw simp? *)
end *)



(* -------------------- *)
(* Simplification Rules *)
(* -------------------- *)

(* module EqResolution : TrivRule = struct
  let simplify clause =
    let clause' = Inference_rules.unflatten_clause clause in
    if clause' == clause then (
      Simplified clause'
    ) else (
      (* incr_int_stat 1 res_num_eq_res_simplified; *)
      Statistics.(bump_int_stat sim_eq_res);
      dbg D_triv @@ lazy (sprintf "EqResolution: %s" (Clause.to_string_tptp clause));
      Simplified clause'
    )
end *)
 
module EqResolutionSimp : TrivRule = struct
  let simplify clause =
    Statistics.(time sim_time_eq_res_simp) @@ fun () ->

    let clause' = Inference_rules.equality_resolution_simp clause in
    if clause' != clause then (
      dassert (fun () -> Clause.Bc.(clause' != clause));
      (* incr_int_stat 1 res_num_eq_res_simplified; *)
      Statistics.(bump_int_stat sim_eq_res_simp);
      dbg D_triv @@ lazy (sprintf "EqResolutionSimp: %s" (Clause.to_string_tptp clause));
    );
    Simplified clause'
end



module PropSubs : TrivRule = struct
  let simplify clause =
    let clause' = Prop_solver_exchange.prop_subsumption clause in
    if clause' != clause then (
      (* incr_int_stat 1 res_num_eq_res_simplified; *)
      dbg D_triv @@ lazy (sprintf "PropSubs: %s new: %s" (Clause.to_string_tptp clause) (Clause.to_string_tptp clause'));
      dassert (fun () -> Clause.length clause' < Clause.length clause);
     );
    Simplified clause'
end

module SMTSubs : FwRule = struct
  include SMTIncrIndex

  let simplify state clause =
    Statistics.(time sim_time_smt_subs) @@ fun () ->

    let clause' = SMT_incremental.global_smt_subsumption Set.(state.smt_incremental) clause in
    if clause' != clause then (
      dbg D_smt @@ lazy (sprintf "SMTSubs: %s" (Clause.to_string_tptp clause));
      dbg D_smt @@ lazy (sprintf "to:      %s" (Clause.to_string_tptp clause'));
      Statistics.(bump_int_stat sim_smt_subsumption);
    );
    Simplified clause'
  let simplify state clause =
    dbg D_smt @@ lazy "smt_subsumption";
    simplify state clause
end

(* module SMTSet : FwRule = struct
  include SMTSetIndex

  let simplify state clause = 
    if Set.(state.c)
end *)



module TautologyElim : TrivRule = struct
  let simplify clause = 
    Statistics.(time sim_time_tautology_del) @@ fun () ->

    if not @@ Inference_rules.is_tautology clause then (
      Simplified clause
    ) else (
      (* incr_int_stat 1 res_tautology_del; *)
      Statistics.(bump_int_stat sim_tautology_del);
      dbg D_triv @@ lazy (sprintf "TautologyElim: %s" (Clause.to_string_tptp clause));
      Eliminated []
    )
end

(* Cannot use with axiomtic equality! Only in preprocessing before eq axioms are added. *)
module EqTautologyElim : TrivRule = struct
  let simplify clause = 
    Statistics.(time sim_time_eq_tautology_del) @@ fun () ->

    if not @@ Inference_rules.is_eq_tautology clause then (
      Simplified clause
    ) else (
      (* incr_int_stat 1 res_tautology_del; *)
      Statistics.(bump_int_stat sim_eq_tautology_del);
      dbg D_triv @@ lazy (sprintf "EqTautologyElim: %s" (Clause.to_string_tptp clause));
      Eliminated []
    )
end

(* Pseudo-rule encapsulates all "cheap" triv simplifications. *)
module TrivRules : TrivRule = struct
  let simplify clause =
    let open Fw_result in
    TautologyElim.simplify clause
    >>= EqTautologyElim.simplify
    >>= EqResolutionSimp.simplify 
end

module Unflattening : TrivRule = struct
  let simplify clause = 
    let clause' = Inference_rules.unflatten_clause clause in
    Simplified clause'
end

(* Check if in set *)
(* module CheckMem : FwRule = struct
  let simplify state clause =
    if Set.mem_any state clause 
    (* || Simplify.forward_subset_subsume sim_state clause != clause *)
    then (
      dbg D_trace @@ lazy "[existing] or subsumed";
      Simplify.Eliminated
    ) else (
      Simplify.Simplified clause
    )
end *)

module SMTSimplify : TrivRule = struct
  let simplify_v1 clause = 
    dbg D_smt @@ lazy (sprintf "smt_simplify: %s" (Clause.to_string_tptp clause));
    if not !Parser_types.has_ext_theory_names then (
      dbg D_smt @@ lazy (sprintf "no theory in input");
      Simplified clause 
    ) else if Clause.is_ext_axiom clause then (
      dbg D_smt @@ lazy (sprintf "is axiom");
      Simplified clause 
    ) else (
      let clause_smt = SMTSolver.clause_to_smt(* _quant *) !GlobalSMT.state clause in
      let clause_smt' = SMTSolver.simplify_clause clause_smt in
      if SMTSolver.Clause.equal clause_smt clause_smt' then
        Simplified clause
      else
        (* let lits = SMTSolver.clause_of_smt !GlobalSMT.state clause_smt' in *)
        match SMTSolver.clause_of_smt !GlobalSMT.state clause_smt' with
        | Some lits -> 
          let source = Clause.tstp_source_smt_theory_normalisation clause in
          let clause' = create_clause ~normalise_eqs:true source lits in
          dbg D_smt @@ lazy (sprintf "SMT simplified %s" (Clause.to_string_tptp clause));
          if Clause.Bc.(clause' != clause) then (
            Statistics.(bump_int_stat sim_smt_simplified);
            dbg D_smt @@ lazy (sprintf "to             %s" (Clause.to_string_tptp clause'));
            Simplified clause'
          ) else (
            dbg D_smt @@ lazy (sprintf "to             same");
            Simplified clause
          )
        | None -> 
          Statistics.(bump_int_stat sim_smt_simplified);
          dbg D_smt @@ lazy (sprintf "SMT simplified %s" (Clause.to_string_tptp clause));
          dbg D_smt @@ lazy (sprintf "to             tautology");
          Eliminated [clause]
    )

  exception Return
  let simplify_v2 clause = 
    dbg D_smt @@ lazy (sprintf "smt_simplify: %s" (Clause.to_string_tptp clause));
    if not !Parser_types.has_ext_theory_names then (
      dbg D_smt @@ lazy (sprintf "no theory in input");
      Simplified clause 
    ) else (
      let any_changed = ref false in
      let lits = 
        Clause.get_lits clause |> List.filter_map (fun lit -> 
          let lit_smt = SMTSolver.lit_to_smt !GlobalSMT.state lit in
          let lit_smt' = SMTSolver.simplify_term lit_smt in
          if SMTSolver.Term.equal lit_smt lit_smt' then (
            Some lit
          ) else (
            (* dbg D_smt @@ lazy (sprintf "SMT: %s to %s"); *)
            let lit' = SMTSolver.lit_of_smt !GlobalSMT.state lit_smt' in
            dbg D_smt @@ lazy (sprintf "%s to %s" (Term.to_string lit) (Term.to_string lit'));
            if lit' == term_true then 
              (* raise_notrace Return *)
              Some lit
            else if lit' == term_false then
              None
            else
              if Term.get_num_of_symb lit > Term.get_num_of_symb lit' then (
                any_changed := true; Some lit'
              ) else (
                Some lit
              )
          )
        )
      in
      if !any_changed then (
        let source = Clause.tstp_source_smt_theory_normalisation clause in
        let clause' = create_clause ~normalise_eqs:true source lits in
        dassert (fun () -> Clause.Bc.(clause' != clause));
        Statistics.(bump_int_stat sim_smt_simplified);
        dbg D_smt @@ lazy (sprintf "SMT simplified %s" (Clause.to_string_tptp clause));
        dbg D_smt @@ lazy (sprintf "to             %s" (Clause.to_string_tptp clause'));
        Simplified clause'
      ) else (
        Simplified clause
      )
    )

  let simplify = simplify_v2
end





(* -- Subsumption -- *)

module FwSubsumptionPrecond(M : sig val pre_cond : cl_in:clause -> cl_by:clause -> bool end) : FwRule = struct
  (* include NonunitSubsIndex *)

  let forward_subs state clause = 
    Statistics.(time sim_time_fw_subs) @@ fun () ->

    (* do not need light simplifications since light backward *)
    (* assert (state.sim_opt.sim_use_sub_index); *)
    match
      SubsumptionIndex_M.is_subsumed ~subs_bck_mult:Set.(state.options.subs_bck_mult)
        ~pre_cond:M.pre_cond Set.(state.subsumption_index) clause
    with
    | Some (by_cl, _subst) -> 
      Statistics.(bump_int_stat sim_fw_subsumed);
      (* if not (Clause.equal_bc by_cl clause) then set_dead_and_remove state clause; *)
      dbg D_fw_subs @@ lazy (sprintf "%s by %s" (Clause.to_string_tptp clause) (Clause.to_string_tptp by_cl));
      Eliminated [by_cl]
    | None -> 
      Simplified clause

  let simplify state clause = 
    dbg D_fw_subs @@ lazy "fw_subsumption";
    forward_subs state clause

  (* Let's assume that clauses in nonunitsubsindex are also in unitsubsindex. So if this clause is 
     unit we don't even bother checking bw simplification, since we would have already checked it
     with the unit index, more efficiently. Ditto also for the BwSubs, FwSubsRes, BwSubsRes. *)
  let simplify state clause = 
    if Clause.is_unit clause then Simplified clause else simplify state clause
end

module FwSubsumptionNonStrict = FwSubsumptionPrecond(struct 
  let pre_cond_true_fun = BasicSubsumptionIndex.pre_cond_true_fun
  let pre_cond = pre_cond_true_fun 
end)

module FwSubsumption = FwSubsumptionPrecond(struct 
  (* let pre_cond_subs_strict ~cl_in ~cl_by = Clause.length cl_by < Clause.length cl_in  *)
  let pre_cond_subs_strict ~cl_in ~cl_by = cl_by != cl_in 
  let pre_cond = pre_cond_subs_strict 
end)

module BwSubsumption : BwRule = struct
  include NonunitSubsIndex

  let backward_subs_full state clause =
    Statistics.(time sim_time_bw_subs) @@ fun () ->

    (* assert(state.sim_opt.sim_use_sub_index); *)
    (*let was_in_index = SubsumptionIndex_M.in_subs_index Set.(state.subsumption_index) clause in*)
    let was_in_index = Set.mem state NonunitSubsIndex.index clause in
    (* TODO check if this is not unnecessary *)

    let b_subsumed_list =
      SubsumptionIndex_M.find_subsumed ~subs_bck_mult:Set.(state.options.subs_bck_mult) Set.(state.subsumption_index) clause
      |> ListExtra.removeq_all clause  (* TODO should be Clause.Bc.(==) instead? *)
    in
    dassert (fun () -> List.for_all (fun x -> Clause.Bc.(x != clause)) b_subsumed_list);

    (* Set.add state index clause; *)   (* TODO ver *)
    (* Add back the clause itself *)
    if was_in_index then SubsumptionIndex_M.add_clause state.subsumption_index clause;
 
    b_subsumed_list |> List.iter (fun subsumed ->
      Statistics.(bump_int_stat sim_bw_subsumed);
      dbg D_bw_subs @@ lazy (sprintf "%s by %s with Clause.Bc.equal=%B"  (* TODO check *)
        (Clause.to_string_tptp subsumed) (Clause.to_string_tptp clause) (Clause.equal_bc subsumed clause)
      );
      (* Clause already removed by [SubsumptionIndex_M.find_subsumed], mark so in context param *)
      Set.remove state Index_tag.NonunitSubs subsumed;
      set_dead_and_remove state subsumed; 
    );

    b_subsumed_list

  let backward_subs_by_length state length clause =
    if Clause.length clause <= length then
      backward_subs_full state clause
    else 
      []

  let simplify state clause = 
    dbg D_fw_subs @@ lazy "bw_subsumption";
    let remove = backward_subs_full state clause in 
    List.map (fun x -> BwEliminated x) remove

  let simplify state clause = 
    if Clause.is_unit clause then [] else simplify state clause
end

module FwSubsumptionRes : FwRule = struct
  include NonunitSubsIndex

  (* Can raise Unsatisfiable, Eliminated *)
  let forward_subs_res state clause =
    Statistics.(time sim_time_fw_subs_res) @@ fun () ->

    (* let lits = get_lits clause in *)
    let new_lits, parents = SubsumptionIndex_M.fw_subsres ~subs_bck_mult:Set.(state.options.subs_bck_mult)  Set.(state.subsumption_index) clause in
    if List.X.is_nonempty parents then (
      let tstp_source = Clause.tstp_source_forward_subsumption_resolution clause parents in
      let clause' = create_clause ~normalise_eqs:true tstp_source new_lits in
      assert (Clause.Bc.(clause' != clause));
      dbg D_fw_subs_res @@ lazy (sprintf "%s to %s" (Clause.to_string_tptp clause) (Clause.to_string_tptp clause'));
      dbg_env D_fw_subs_res (fun () -> 
        parents |> List.iter (fun x -> dbg D_fw_subs_res @@ lazy (sprintf "by %s" (Clause.to_string_tptp x)))
      );
      Statistics.(incr_int_stat (List.length parents) sim_fw_subsumption_res);
      (* forward; should not assign dead without returing subsumed to the caller *)
      (* set_dead_and_remove state clause; *)
      clause'
    ) else (
      clause
    )

  let simplify state clause =
    dbg D_fw_subs @@ lazy "fw_subsumption_res";
    try
      Simplified (forward_subs_res state clause)
    with Lib.Eliminated ->
      assert false

  let simplify state clause = 
    if Clause.is_unit clause then Simplified clause else simplify state clause
end

module BwSubsumptionRes : BwRule = struct
  include NonunitSubsIndex

  let simplify state clause = 
    dbg D_fw_subs @@ lazy "bw_subsumption_res";
    (* let remove, add = backward_subs_res state clause in *)
    let remove, add = SubsumptionIndex_M.bw_subsres ~subs_bck_mult:Set.(state.options.subs_bck_mult) Set.(state.subsumption_index) clause in
    List.map2 (fun from into_lits -> 
      let tstp_source = Clause.tstp_source_backward_subsumption_resolution from [clause] in
      let into = create_clause ~normalise_eqs:true tstp_source into_lits in
      dbg D_bw_subs_res @@ lazy (sprintf "%s to %s" (Clause.to_string_tptp from) (Clause.to_string_tptp into));
      dbg D_bw_subs_res @@ lazy (sprintf "by %s" (Clause.to_string_tptp clause));
      Statistics.(bump_int_stat sim_bw_subsumption_res);
      (* ALREADY REMOVED FROM SUBS! *)
      (* set_dead_and_remove state from; *)
      (*dassert (fun () -> SubsumptionIndex_M.in_subs_index state.subsumption_index from == false);*)
      (* dassert (fun () -> Set.mem state NonunitSubsIndex.index clause); *)
      (* TODO assert from not in SubsumptionIndex_M index? *)
      Set.remove state Index_tag.NonunitSubs from;
      set_dead_and_remove state from;
      BwSimplified {from; into}
    ) remove add

  let simplify state clause = 
    if Clause.is_unit clause then [] else simplify state clause
end



(****************)
(* Hybrid index *)
(****************)

(* module FwHybridSubs : FwRule = struct
  let index = Index_tag.HybridSubs

  let simplify state clause = 
    dbg D_fw_hybrid_subs @@ lazy "fw_hybrid_subsumption";
    match HybridSubsumptionIndex_M.is_subsumed  ~subs_bck_mult:Set.(state.options.subs_bck_mult) Set.(state.hybrid_subs_index) clause with
    | Some x -> 
      if x != clause then (
        dbg D_fw_hybrid_subs @@ lazy (sprintf "%s to %s" (Clause.to_string_tptp clause) (Clause.to_string_tptp x));
        dbg_env D_fw_hybrid_subs (fun () ->
          Clause.get_parents x |> List.iter (fun c -> if c != clause then dbg D_fw_hybrid_subs @@ lazy (sprintf "by %s" (Clause.to_string_tptp c)))
        )
      );
      Simplified x
    | None -> 
      dbg D_fw_hybrid_subs @@ lazy (sprintf "subsumed %s" (Clause.to_string_tptp clause));
      Eliminated
end

module BwHybridSubs : BwRule = struct
  let index = Index_tag.HybridSubs

  let simplify state clause = 
    dbg D_bw_hybrid_subs @@ lazy "bw_hybrid_subsumption";
    let subs, subs_res = HybridSubsumptionIndex_M.subsumes  ~subs_bck_mult:Set.(state.options.subs_bck_mult) Set.(state.hybrid_subs_index) clause in
    let subs' = subs |> List.map (fun x -> 
      dbg D_bw_hybrid_subs @@ lazy (sprintf "subsumed %s" (Clause.to_string_tptp x));
      Set.remove state Index_tag.HybridSubs x;
      set_dead_and_remove state x;
      BwEliminated x
    ) in
    let subs_res' = subs_res |> List.map (fun (from, into) -> 
      dbg D_bw_hybrid_subs @@ lazy (sprintf "%s to %s" (Clause.to_string_tptp from) (Clause.to_string_tptp into));
      dbg D_bw_hybrid_subs @@ lazy (sprintf "by %s" (Clause.to_string_tptp clause));
      (* ALREADY REMOVED FROM SUBS! *)
      (* set_dead_and_remove state from; *)
      (* dassert (fun () -> SubsumptionIndex_M.in_subs_index state.hybrid_subs_index. from == false); *)
      Set.remove state Index_tag.HybridSubs from;
      set_dead_and_remove state from;
      BwSimplified {from; into}
    ) in
    subs' @ subs_res'
end *)

module FwUnitSubs : FwRule = struct
  let simplify state clause = 
    Statistics.(time sim_time_fw_unit_subs) @@ fun () ->
     
    dbg D_fw_unit_subs @@ lazy "fw_unit_subsumption";
    match UnitSubsumptionIndex_M.is_subsumed Set.(state.unit_subs_index) clause with
    | UnitSubsumptionIndex_M.Nothing ->
      Simplified clause
    | UnitSubsumptionIndex_M.Subs by_clause ->
      dbg D_fw_unit_subs @@ lazy (sprintf "subsumed %s" (Clause.to_string_tptp clause));
      Eliminated [by_clause]
    | UnitSubsumptionIndex_M.SubsRes (parents, lits) ->
      let tstp_source = Clause.tstp_source_forward_subsumption_resolution clause parents in
      let clause' = create_clause ~normalise_eqs:true tstp_source lits in
      Statistics.(bump_int_stat sim_fw_unit_subs);
      dassert (fun () -> Clause.Bc.(clause' != clause));
      dbg D_fw_unit_subs @@ lazy (sprintf "%s to %s" (Clause.to_string_tptp clause) (Clause.to_string_tptp clause'));
      dbg_env D_fw_unit_subs (fun () ->
        parents |> List.iter (fun c -> if c != clause then dbg D_fw_unit_subs @@ lazy (sprintf "by %s" (Clause.to_string_tptp c)))
      );
      Simplified clause'
end

module BwUnitSubs : BwRule = struct
  let simplify state clause = 
    Statistics.(time sim_time_bw_unit_subs) @@ fun () ->
 
    dbg D_bw_unit_subs @@ lazy "bw_unit_subsumption";
    match Clause.get_lits clause with
    | [lit] ->
      let subs, subs_res = UnitSubsumptionIndex_M.subsumes Set.(state.unit_subs_index) lit in
      dassert (fun () -> List.for_all (fun c -> c != clause) subs);
      let subs = subs |> List.map (fun c -> 
        dbg D_bw_unit_subs @@ lazy (sprintf "subsumed %s" (Clause.to_string_tptp c));
        Statistics.(bump_int_stat sim_bw_unit_subs);
        Set.remove state Index_tag.UnitSubs c;
        set_dead_and_remove state c;
        BwEliminated c
      ) in
      let subs_res = subs_res |> List.map (fun (from, into_lits) ->
        let tstp_source = Clause.tstp_source_backward_subsumption_resolution from [clause] in
        let into = create_clause ~normalise_eqs:true tstp_source into_lits in
        dbg D_bw_unit_subs @@ lazy (sprintf "%s to %s" (Clause.to_string_tptp from) (Clause.to_string_tptp into));
        dbg D_bw_unit_subs @@ lazy (sprintf "by %s" (Clause.to_string_tptp clause));
        (* ALREADY REMOVED FROM SUBS! *)
        (* set_dead_and_remove state from; *)
        (* dassert (fun () -> SubsumptionIndex_M.in_subs_index state.unit_subs_index. from == false); *)
        Statistics.(bump_int_stat sim_bw_unit_subs);
        Set.remove state Index_tag.UnitSubs from;
        set_dead_and_remove state from;
        BwSimplified {from; into}
      ) in
      subs @ subs_res
    | _ -> 
      dbg D_bw_unit_subs @@ lazy "not a unit clause";
      []
end

(* module FwFullSubsAndRes : FwRule = struct
  let index = unit

  let simplify state clause = 
    FwUnitSubs.simplify state clause
    >>= FwSubsumption.simplify state
    >>= FwSubsumptionRes.simplify state
end

module BwFullSubsAndRes : BwRule = struct
  let index = unit

  let simplify state clause = 
    BwUnitSubs.simplify state clause
    >>= BwSubsumption.simplify state
    >>= BwSubsumptionRes.simplify state
end *)





(* ------------------ *)
(* Subset Subsumption *)
(* ------------------ *)

module FwSubsetSubsumption : FwRule = struct
  let simplify state clause = 
    Statistics.(time sim_time_fw_subset_subs) @@ fun () ->

    try
      let by_clause = SubsetSubsumptionIndex_M.is_subsumed Set.(state.subset_subsumption_index) clause in
      dbg D_fw_sub_subs @@ lazy (sprintf "subsumed: %s by: %s" (Clause.to_string_tptp clause) (Clause.to_string_tptp by_clause));
      Statistics.(bump_int_stat sim_fw_subset_subsumed);
      Eliminated [by_clause]
    with Not_found -> 
      Simplified clause
  let simplify state clause =
    dbg D_fw_sub_subs @@ lazy "fw_subset_subsumption";
    simplify state clause
end

module BwSubsetSubsumption : BwRule = struct
  let simplify state main_clause =
    Statistics.(time sim_time_bw_subset_subs) @@ fun () ->
    try
      let subsumed_clauses = 
        SubsetSubsumptionIndex_M.find_subsumed Set.(state.subset_subsumption_index) main_clause
        |> List.X.remove_all ~eq:Clause.Bc.equal main_clause
      in
      if List.X.is_nonempty subsumed_clauses then (
        Statistics.(incr_int_stat (List.length subsumed_clauses) sim_bw_subset_subsumed);
        subsumed_clauses |> List.iter (fun x -> 
          dbg D_bw_sub_subs @@ lazy (sprintf "subsumed: %s by: %s" (Clause.to_string_tptp x) (Clause.to_string_tptp main_clause));
          set_dead_and_remove state x
        )
      ) else (
        ()
      );
      subsumed_clauses
    with SubsetSubsumptionIndex_M.No_subsumed -> 
      []
  let simplify state clause =
    dbg D_bw_sub_subs @@ lazy "bw_subset_subsumption";
    let remove = simplify state clause in
    List.map (fun x -> BwEliminated x) remove
end



(* ------------ *)
(* Demodulation *)
(* ------------ *)

module FwDemod = struct
  (* include FwDemodIndex *)

  (* Memoisation now handled in index itself *)
  (* module State = struct
    type t = {
      mutable terms: term TMap.t;
      mutable parents: BCSet.t;
    }
  end *)

  module State = struct
    type t = { mutable parents: BCSet.t }

    let empty() = { parents = BCSet.empty }

    let add x state = 
      match x with
      | None -> ()
      | Some (x, _) -> state.parents <- BCSet.add x state.parents
  end

  (* TODO: how to handle cases where some top term appears as subterm 
     elsewhere in the clause? E.g.: 
       a=b | f(a) = c
     `a` can actually be demodulated. Not clear how to do this efficiently. 
     Scan all subterms? *)

  (* Solution: if a top term doesn't pass the completeness test, still allow it to be tried again *)

  (* TODO: Also an issue: if we assume equations in the index aren't 
     necessarily reduced, then we should recurse into demodulated subterms *)

  (** The function that tries to demodulate term [s] from clause [clause] via
      equations in [index]. *)
  (* Moved to set *)
  (* let func = undefined() *)

  (* As func but tests instead of stopping on the first rewrite, tries all and chooses the smallest one? *)
  (* let func_all candidares s = undefined() *)

  (* let func_nocheck = func (* (fun _ _ _ -> true) *) *)



  (* Settings for traversal: *)
  let demodulate_term_before_args = true
  let demodulate_term_after_args  = true
  let recurse_into_demodulated_subterms = true
  let assert_we_get_fixpoint = false

  (** Demodulate top (no ordering checks, no recurse into subterms) *)
  let demodulate_top (* check *) state index clause func s = 
    dbg D_demod @@ lazy (sprintf "demodulate_top: %s" (Term.to_string s));
    match s with
    | Term.Fun _ -> 
      (* State.get state (func index clause) s *)
      let s', data = FwDemodIndex_M.iter_fwd_caching index s func in
      begin match data with
      | Some (parent_clause, subst) when Clause.Bc.(parent_clause != clause) ->
        State.add data state;
        assert (s != s');
        s', Some subst
      | _ -> 
        s, None
      end
    | Term.Var _ -> s, None

  (** Demodulate recursively (no ordering checks) *)
  let rec demodulate_subterms state index (* clause *) func_nocheck s = 
    match s with
    | Term.Fun (sym, args, _) ->
      dbg D_demod @@ lazy (sprintf "demodulate_subterms: %s" (Term.to_string s));

      (* First, demodulate term itself *)
      let s' = 
        (* let s' = State.get state (func index clause) s in *)
        if demodulate_term_before_args then (
          let s', data = FwDemodIndex_M.iter_fwd_caching index s func_nocheck in
          State.add data state;
          s'
        ) else (
          s
        )
      in

      (* If successful, try again until fixpoint *)
      if s' != s then (
        (* dbg D_demod @@ lazy (sprintf "demod_sub: %s becomes %s" (Term.to_string s) (Term.to_string s')); *)
        if recurse_into_demodulated_subterms then
          demodulate_subterms state index func_nocheck s'
        else
          s'
      ) 

      (* Then demodulate all args *)
      else (
        (* let args = Term.arg_to_list (if s == s' then args else Term.get_args s') in *)
        let args = dassert (fun () -> s == s'); Term.arg_to_list args in
        let args' = List.map (demodulate_subterms state index func_nocheck) args in

        (* If same, then we're done *)
        if List.for_all2 (==) args args' then
          s'

        (* If changed, then we also try to re-demodulate the term itself *)
        else
          let s'' = add_term_db @@ Term.create_fun_term sym args' in
          (* State.get state (func index clause) s'' *)
          let s''' = 
            if demodulate_term_after_args then (
              let s''', data = FwDemodIndex_M.iter_fwd_caching index s'' func_nocheck in
              State.add data state;
              s'''
            ) else (
              s''
            )
          in

          (* And again, if successful, try again until fixpoint *)
          if recurse_into_demodulated_subterms && s''' != s'' then 
            demodulate_subterms state index func_nocheck s'''
          else
            s'''
      )
    | Term.Var _ -> s

  (** Demodulate args recursively (no ordering checks) *)
  let demodulate_args state index (* clause *) func_nocheck s =
    match s with
    | Term.Fun (sym, args, _) ->
      dbg D_demod @@ lazy (sprintf "demodulate_args: %s" (Term.to_string s));
      let args = Term.arg_to_list args in
      let args' = List.map (demodulate_subterms state index func_nocheck) args in
      if List.for_all2 (==) args args' then
        s
      else
        add_term_db @@ Term.create_fun_term sym args'
    | Term.Var _ -> s


  (* Controls whether to record encompassment statistics or not *)
  let record_statistics = false
  let _ = if record_statistics then (out_warning "simplify_new:can be expensive:record_statistics=true")

  let demod_completeness_check ~(order:ordering) check_type typ clause l l' r subst =
    (* Old version *)
    (* let is_renaming lsigma l = 
      lsigma == l || (try ignore @@ Unif.matches lsigma l; true with Unif.Matching_failed -> false)
    in *)
    
    let[@inline] check_renaming subst = 
      not (Subst.is_renaming subst)
    in
    let[@inline] check_order_fast ~(order:ordering) r l' = 
      order.terms r l' == GT  (* TODO: This can actually go in the orderings_cache *)
    in
    let[@inline] check_order_full ~(order:ordering) typ l l' clause = 
      let eq_term' = add_lit_eq true typ l l' in
      List.exists (fun x -> order.lits x eq_term' == GT) (Clause.get_lits clause)
    in
    
    let open Options.Demod_check in
    match check_type with
    | Off -> 
      true

    | FullOld ->
      dbg D_demod @@ lazy "really checking demod order (full)";
      dbg D_demod2 @@ lazy (sprintf "Rules: ( %s = %s ) < %s" (Term.to_string l) (Term.to_string l') (Clause.to_string_tptp clause));
      check_order_full ~order typ l l' clause
    | FastOld ->
      dbg D_demod @@ lazy "really checking demod order (fast)";
      (* dbg D_demod2 @@ lazy (sprintf "Rules: %s cannot be a renaming of %s" (Term.to_string l) (Term.to_string orig_l)); *)
      dbg D_demod2 @@ lazy (sprintf "Rules: %s < %s" (Term.to_string l') (Term.to_string r));
      check_order_fast ~order r l'
    
    | Full ->
      dbg D_demod @@ lazy "really checking demod order (full + renaming)";
      (* dbg D_demod2 @@ lazy (sprintf "Rules: %s cannot be a renaming of %s" (Term.to_string l) (Term.to_string orig_l)); *)
      dbg D_demod2 @@ lazy (sprintf "Rules: %s cannot be a renaming" (Subst.to_string subst));
      dbg D_demod2 @@ lazy (sprintf "or     ( %s = %s ) < %s" (Term.to_string l) (Term.to_string l') (Clause.to_string_tptp clause));
      let result = 
        if check_renaming subst then (
          if record_statistics && not (check_order_full ~order typ l l' clause) then Statistics.(bump_int_stat sim_encompassment_demod);
          true
        ) else (
          check_order_full ~order typ l l' clause
        )
      in
      dbg_env D_demod (fun () ->
        let a = check_renaming subst in
        let b = check_order_full ~order typ l l' clause in
        dbg D_demod @@ lazy (sprintf "demod order result: %B %B" a b)
      );
      result
    | Fast ->
      dbg D_demod @@ lazy "really checking demod order (fast + renaming)";
      (* dbg D_demod2 @@ lazy (sprintf "Rules: %s cannot be a renaming of %s" (Term.to_string l) (Term.to_string orig_l)); *)
      dbg D_demod2 @@ lazy (sprintf "Rules: %s cannot be a renaming" (Subst.to_string subst));
      dbg D_demod2 @@ lazy (sprintf "or     %s < %s" (Term.to_string l') (Term.to_string r));
      let result = 
        if check_renaming subst then (
          if record_statistics && not (check_order_fast ~order r l') then Statistics.(bump_int_stat sim_encompassment_demod);
          true
        ) else (
          check_order_fast ~order r l'
        )
      in
      dbg_env D_demod (fun () ->
        let a = check_renaming subst in
        let b = check_order_fast ~order r l' in
        dbg D_demod @@ lazy (sprintf "demod order result: %B %B" a b)
      );
      result

  exception Return of clause list

  (** Demodulate clause via equations in index. Returns [Some demodulated_clause] if possible, or [None] if no demodulation can be done *)
  let demodulate_forward ~order ~demod_completeness_check_type ~func index clause =
    Statistics.(time sim_time_fw_demod) @@ fun () ->

    dbg D_demod @@ lazy "demodulate_forward";

    (* Idea: 
       - Keep a map of subterms -> normal_forms
       - First check if any non-equality or negative literal in the clause: 
         if yes, [no_check] is true and we don't need to check ordering for 
         completeness
         is false, since equalities will always be smaller
       - For all literals in clause, starting at the top, first check if the 
         term appears in the map. 
         - if yes, that's its normal form
         - if not, then demodulate
           - if at a subterm position, no need to do ordering_check
           - if at the top, *first* try to match, then only if matching 
             succeeds do we need to check ordering 
         - if demodulation was successful, return
         - otherwise, recurse into subterms *)

    (* Demodulation ordering check
       - Clause being rewritten is non-unit: no check, else
       - Literal is non-eq: no check, else
       - Literal is negative: no check, else
       - At this point we need ordering check, but only at the top. 
         If we use eq a=b to rewrite l=r to l'=r, where l = a\sigma and l' = b\sigma, then
         - If l<r: no check
         - If l matches a, that is, that l\theta = a, that is, l and a are variants: no check
         - Else, check that l'<r. *)

    let state = State.empty() in
    (* No check needed at all if (any) literal is predicate or negative *)
    (* Or if clause is non-unit *)
    let no_check = 
      (* Clause.get_lits clause
      |> List.exists (fun x -> 
        not (Term.Eq.is_eq x) || Term.Eq.is_predicate_eq x || Term.is_neg_lit x
      ) *)
      match Clause.get_lits clause with
      | [x] ->
        begin match Term.Eq.decompose_lit x with
        | Some (sign, l,r) -> sign == false || r == SystemDBs.top_term
        | None -> true
        end
      | _ -> true
    in

    let lits = Clause.get_lits clause in
    let lits' = 
      lits |> List.map (fun lit ->
        match Term.Eq.decompose_lit_type lit with
        | Some (sign, typ,l,r) ->
          let[@inline] if_eq_return sign typ l r = if sign then raise_notrace (Return (BCSet.elements state.parents)) else add_lit_eq sign typ l r in

          (* Rather than check is_oriented first, just demodulate first without 
             checks, then manually check if needed (and only at the top). *)
          let l', subst_l = demodulate_top state index clause func l in
          let r', subst_r = demodulate_top state index clause func r in

          (* Ordering checks *)
          let l', r' = 
            (* If unchanged, or if no_check = false no need to check *)
            if l' == l && r' == r 
            || no_check 
            then
              l', r'
            (* If at least one has changed, then we need to see if it was on a smaller side or not *)
            else
              let dmc[@inline] = demod_completeness_check ~order demod_completeness_check_type typ clause in
              match order.oriented lit with
              | GT -> 
                if l' == l || dmc l l' r (Option.get subst_l)
                then
                  l', r'
                else
                  l , r'
              | LT -> 
                if r' == r || dmc r r' l (Option.get subst_r)
                then
                  l', r'
                else
                  l', r
              | INC -> 
                let l_okay = l' == l || dmc l l' r (Option.get subst_l) in
                let r_okay = r' == r || dmc r r' l (Option.get subst_r) in
                (if l_okay then l' else l), (if r_okay then r' else r)
              | EQ -> invalid_arg "demodulate_forward: attempting to simplify s=s or s!=s"
          in

          let l' = 
            if recurse_into_demodulated_subterms && l' != l then
              (* let rec loop x = let x' =  *)
              fix_point (fst %% demodulate_top state index clause func) l'
            else
              l'
          in
          let r' = 
            if recurse_into_demodulated_subterms && r' != r then
              fix_point (fst %% demodulate_top state index clause func) r'
            else
              r'
          in

          if l' == r' then if_eq_return sign typ l' r' else

          let l'' = demodulate_args state index (* clause *) func l' in
          let r'' = demodulate_args state index (* clause *) func r' in

          if l'' == r'' then if_eq_return sign typ l'' r'' else

          let l''' = 
            if l'' == l' then
              l''
            else if recurse_into_demodulated_subterms then
              fix_point ((* fst %% *) demodulate_subterms state index (* clause *) func) l''
            else
              fst @@ demodulate_top state index clause func l''
          in
          let r''' = 
            if r'' == r' then
              r''
            else if recurse_into_demodulated_subterms then
              fix_point ((* fst %% *) demodulate_subterms state index (* clause *) func) r''
            else
              fst @@ demodulate_top state index clause func r''
          in

          if l''' == l && r''' == r then
            lit
          else if l''' == r''' then
            if_eq_return sign typ l''' r'''
          else
            add_lit_eq sign typ l''' r'''

        (* Non-equality literal *)
        | None ->
          demodulate_args state index (* clause *) func lit
      )
    in

    if List.for_all2 (==) lits lits' then (
      clause
    ) else (
      (* Only here do we actually build the clause *)
      Statistics.(bump_int_stat sim_fw_demodulated);  (* Just counts total number of demodulated clauses, not comparable to previous versions *)
      let source = Clause.tstp_source_demodulation ~main:clause ~eqs:(BCSet.elements state.parents) in
      let conclusion = create_clause ~normalise_eqs:true source lits' in
      dbg D_demod @@ lazy (sprintf "Forward demodulated %s" (Clause.to_string_tptp clause));
      dbg D_demod @@ lazy (sprintf "to                  %s" (Clause.to_string_tptp conclusion));
      dbg_env D_demod (fun () ->
        match BCSet.elements state.parents with
        | [] -> assert false
        | [x] -> 
          dbg D_demod @@ lazy (sprintf "via                 %s" (Clause.to_string_tptp x));
        | hd::tl -> 
          dbg D_demod @@ lazy (sprintf "via                 %s" (Clause.to_string_tptp hd));
          tl |> List.iter (fun x -> 
            dbg D_demod @@ lazy (sprintf "                    %s" (Clause.to_string_tptp x));
          )
      );
      conclusion
    )

(*-----------KK: demodulate_forward_new -------*)
(*
  type cl_demod_state = 
      {(* dres: stores result of demodulation of subterms *)
       (* dres: s -> Some ((r',[l=r])) if subterm s demod to r'; l=r is used for proof *)
       (*       s -> None if there is no matching l in demod index *)
       (*       s is not in; then we have not tried to demod s *)
             
       mutable dres : (term * clause list) TMap.t; 

(* TODO: new_ueqs that were generated during e.g. shortening demod paths *)
(* TODO: whether record demod instances in light norm *)
       mutable dueq_new : clause list;
     }


(*-------*)        
  let demodulate_forward_new ~demod_completeness_check_type index (clause:clause) : unit =

    dbg D_demod @@ lazy "demodulate_forward";

(*-- demod top down --*)

    let demod_term_update ~check_fun cl_ds s = (* is is a term but below literal *)
      try 
        match TMap.find s cl_ds.dres with 
        | Some _ -> cl_ds (* already there *)
        | None   -> cl_ds (* alredy tried *)            
      with 
        Not_found ->        
          
          let candidates = 
            DemodulationIndex.get_fwd_candidates index s |>
              (* sort small ueq first *)
            List.sort (fun (_,(_,_,ueq_1)) (_,(_,_,ueq_2)) -> Clause.cmp_num_symb ueq_1 ueq_2)             
          in
          candidates |> List.iter (fun (_, lst) ->
            (* dbg D_demod @@ lazy (sprintf "length lst %d" (List.length lst)); *)
            lst |> List.iter (fun (pos, ueq_lit, ueq_clause) ->
              if not @@ Clause.equal_bc clause ueq_clause then (
                match Term.decompose_eq_atom ueq_lit with
                | [_;l;r] ->
                    let l,r = Term.Eq.regularize_pos pos l r in
                    
                    Inference_rules.demodulation
                      ~demod_completeness_check_type ~to_check eq_clause eq_lit l r s clause 
                |_ -> assert fail 
               )
                             )
                  )            
    in ()
*)
(*----------*)

  let exists_some_candidate_dbg state clause = 
    let some_nonempty = ref false in
    clause |> Clause.iter (fun lit -> 
      lit |> Term.iter_preorder_novar (fun t -> 
        (* if List.X.is_nonempty @@ DemodulationIndex.get_fwd_candidates Set.(state.fw_demod_index_debug) t then
          some_nonempty := true *)
        DemodulationIndex.get_fwd_candidates Set.(state.fw_demod_index_debug) t 
        |> List.iter (fun (term, _lst) ->
          if (try ignore @@ Unif.matches term t; true with Unif.Matching_failed -> false) then (
            dbg D_demod2 @@ lazy (sprintf "Rewrite %s with %s confirmed" (Term.to_string t) (Term.to_string term));
            some_nonempty := true
          )
        )
      )
    );
    if not !some_nonempty then dbg D_demod2 @@ lazy (sprintf "FAILED TO REWRITE %s" (Clause.to_string_tptp clause));
    !some_nonempty

  let assert_some_candidate_dbg state clause clause' = 
    if Set.debug_index_flag then (
      assert (clause' == clause || exists_some_candidate_dbg state clause)
    )

  let simplify state clause =
    try
    (* assert (state.sim_opt.sim_use_demod); *)
    (* match Set.(demodulate_forward state.fw_demod_index) clause with
    | Some new_clause -> new_clause
    | None -> clause *)
    let demod_completeness_check_type = Set.(state.options.demod_completeness_check) in
    let clause' = demodulate_forward ~order:state.order ~demod_completeness_check_type ~func:state.fw_demod_func Set.(state.fw_demod_index) clause in
    assert_some_candidate_dbg state clause clause';
    (* Simplified clause' *)
    if Clause.Bc.(clause' != clause) then
      (* Important! Here pattern match to ensure that if retention test tells us that clause is eliminated, parents are still recorded *)
      match TrivRules.simplify clause' with
      | Simplified _ as result -> 
        if assert_we_get_fixpoint then dassert (fun () -> 
          let clause'' = demodulate_forward ~order:state.order ~demod_completeness_check_type ~func:state.fw_demod_func Set.(state.fw_demod_index) clause' in
          clause'' == clause'
        );
        result
      | Eliminated parents -> 
        Eliminated (parents @ Clause.get_demod_eqs clause')
    else
      Simplified clause
    with Return parents -> 
      dbg D_demod @@ lazy (sprintf "Forward demodulated %s" (Clause.to_string_tptp clause));
      dbg D_demod @@ lazy (sprintf "to                  tautology");
      Eliminated parents
end

module FwDemodLoop (RetentionTest : TrivRule) : FwRule = struct

  include FwDemodIndex
  let make_intermediate_dead state clause =
    dbg D_demod @@ lazy (sprintf "FwDemodLoop:make_intermediate_dead %s" (Clause.to_string_tptp clause));
    try Set.add_dead state clause
    with Set.Is_mem -> try set_dead_and_remove state clause
    with Set.Is_dead -> ()
  let make_intermediate_dead state clause = ()

  let rec loop ~check ~parents_acc state clause =
    dbg D_demod @@ lazy "FwDemodLoop:loop";
    let demod_completeness_check_type = Set.(state.options.demod_completeness_check) in
    if check && Set.mem_any state clause then (
      Eliminated parents_acc
    ) else (
      let open Fw_result.O in
      try
      let clause' = FwDemod.demodulate_forward ~order:state.order ~demod_completeness_check_type ~func:state.fw_demod_func Set.(state.fw_demod_index) clause in
      FwDemod.assert_some_candidate_dbg state clause clause';
      if Clause.Bc.(clause' == clause) then (
        Simplified clause
      ) else (
        (* Clause was demodulated, let's apply retention tests and go for another round *)
        make_intermediate_dead state clause;
        match RetentionTest.simplify clause' with
        | Simplified clause'' ->
          if Clause.Bc.(clause'' != clause') then make_intermediate_dead state clause';
          let parents_acc = Clause.get_demod_eqs clause' @ parents_acc in
          loop (* ~demod_completeness_check *) ~check:true ~parents_acc state clause''
        | Eliminated parents -> 
          Eliminated (parents @ parents_acc)
      )
      with FwDemod.Return parents -> 
        dbg D_demod @@ lazy (sprintf "Forward demodulated %s" (Clause.to_string_tptp clause));
        dbg D_demod @@ lazy (sprintf "to                  tautology");
        Eliminated parents
    )

  let simplify (* ~demod_completeness_check *) state clause =
    loop (* ~demod_completeness_check *) ~check:false ~parents_acc:[] state clause
end

module FwDemodLoopTriv = FwDemodLoop(TrivRules)

module BwDemod : BwRule = struct
  include BwDemodIndex

  (** Demodulate clauses in index via equality clause eq. Returns list of demodulated clauses *)
  let simplify state eq_clause =
    Statistics.(time sim_time_bw_demod) @@ fun () ->

    (* assert (state.sim_opt.sim_use_demod); *)
    dbg D_demod @@ lazy "demodulate_backward";
    let demod_completeness_check_type = Set.(state.options.demod_completeness_check) in
    let use_nonoriented_for_bw_demod = true in
    
    let demodulate_via old_ref new_ref eq_lit l r = 
      let candidates = BwDemodIndex_M.iter_bwd Set.(state.bw_demod_index) l in
      candidates (fun s subst lst ->
        (* dbg D_demod @@ lazy (sprintf "length lst %d" (List.length lst)); *)
        lst |> List.iter (fun (clause, do_check) ->
          (* TODO: If same clause demodulated in several ways, what should do? *)
          if clause != eq_clause
          && not (List.memq clause !old_ref) then (
            (* let conclusion = clause |> fix_point (fun x ->
              dbg D_demod @@ lazy (sprintf "iter x=%s" (Clause.to_string_tptp x));
              try
                Inference_rules.demodulation eq_clause eq_lit l r s x; x
              with Inference_rules.Return x' ->
                x'
            )
            in *)

            (* Old interface: just ordering check *)
            (*
            begin try
              Inference_rules.demodulation ~demod_completeness_check_type ~do_check eq_clause eq_lit l r s clause;
              ()
            with Inference_rules.Return conclusion -> 
              Statistics.(bump_int_stat sim_bw_demodulated);
              dbg D_demod @@ lazy (sprintf "Backward demodulated %s" (Clause.to_string_tptp clause));
              dbg D_demod @@ lazy (sprintf "to                   %s" (Clause.to_string_tptp conclusion));
              (* Get list of indices *)
              (* let indices = Set.indices state clause in *)
              (* Remove old clause from sim_state *)
              set_dead_and_remove state clause;
              (* Add new clause to sim_state *)
              (* ignore @@ sim_add_clause state conclusion; *)
              (* add_to_indices state indices conclusion; *)
              (* Cons new clause onto new_ref *)
              ListExtra.cons_ref conclusion new_ref;
              ListExtra.cons_ref clause old_ref;
            end
            *)

            let result = Inference_rules.demodulation_bare_nomatch ~order:state.order eq_lit l r s subst in
            match result with
            | None -> ()
            | Some s' -> 
              dbg D_demod @@ lazy (sprintf "Bw %s to %s" (Term.to_string s) (Term.to_string s'));
              (* Demodulated clause[s]
                 To          clause[s']
                 Via         l = r *)
              let checked = 
                match do_check with
                | None -> true
                | Some t -> 
                  FwDemod.demod_completeness_check 
                    ~order:state.order demod_completeness_check_type 
                    (Term.Eq.eq_type eq_lit) clause s s' t subst
              in
              if checked then (
                let source = Clause.tstp_source_demodulation ~main:clause ~eqs:[eq_clause] in
                let lits' = 
                  Clause.get_lits clause
                  |> List.map (Term.replace s s')
                in
                let conclusion = create_clause ~normalise_eqs:true source lits' in
                (* TODO also TrivialRules? *)

                Statistics.(bump_int_stat sim_bw_demodulated);
                dbg D_demod @@ lazy (sprintf "Backward demodulated %s" (Clause.to_string_tptp clause));
                dbg D_demod @@ lazy (sprintf "to                   %s" (Clause.to_string_tptp conclusion));
                dbg D_demod @@ lazy (sprintf "via                  %s" (Clause.to_string_tptp eq_clause));
                set_dead_and_remove state clause;
                ListExtra.cons_ref conclusion new_ref;
                ListExtra.cons_ref clause old_ref;
              )
          )
        )
      )
    in

    (* Has to be unit positive equality *)
    match Clause.get_lits eq_clause with
    | [eq_lit] ->
      begin match Term.Eq.decompose_atom eq_lit with
      | Some (l,r) -> 
          let old_ref = ref [] in
          let new_ref = ref [] in
          
          begin match state.order.oriented eq_lit with
          | GT ->
            demodulate_via old_ref new_ref eq_lit l r;
          | LT ->
            demodulate_via old_ref new_ref eq_lit r l;
          | INC ->
            (* Could also choose not to try via unoriented equations *)
            if use_nonoriented_for_bw_demod then (
              if Term.var_subset r l then demodulate_via old_ref new_ref eq_lit l r;
              if Term.var_subset l r then demodulate_via old_ref new_ref eq_lit r l;
            )
          | EQ -> invalid_arg "Uncaught s=s"
          end;

          List.map2 (fun from into -> BwSimplified {from; into}) !old_ref !new_ref

      | None -> []
      end
    | _ -> []
end

(* module Demod : FwBwRule = struct
  let add state clause = 
    FwDemod.add state clause;
    BwDemod.add state clause

  let remove state clause = 
    FwDemod.remove state clause;
    BwDemod.remove state clause

  let fw_simplify = FwDemod.simplify 

  let bw_simplify = BwDemod.simplify 
end *)

module FwLightNorm : FwRule = struct
  include LightNormIndex

  let simplify state clause = 
    Statistics.(time sim_time_light_norm) @@ fun () ->

    let any_changed = ref false in
    let equalities = ref BCSet.empty in
    let lits = 
      Clause.get_lits clause
      |> List.map (fun x -> 
        let x', new_equalities = LightNormIndex_M.normalise Set.(state.lightnorm_index) x in
        if x != x' then (
          any_changed := true;
          equalities := BCSet.union !equalities new_equalities;
        );
        (* TODO: early return if s=s *)
        x'
      )
      |> List.map normalise_eq_lit
    in
    if !any_changed then (
      dassert (fun () -> not @@ BCSet.is_empty !equalities);
      let tstp_source = Clause.tstp_source_lightnorm ~main:clause ~eqs:(BCSet.elements !equalities) in
      let conclusion = create_clause ~normalise_eqs:true tstp_source lits in
      dbg D_lightnorm @@ lazy (sprintf "Light normalised %s" (Clause.to_string_tptp clause    ));
      dbg D_lightnorm @@ lazy (sprintf "to               %s" (Clause.to_string_tptp conclusion));
      BCSet.elements !equalities |> List.iter (fun x ->
        dbg D_lightnorm @@ lazy (sprintf "via              %s" (Clause.to_string_tptp x));
      );
      Statistics.(bump_int_stat sim_light_normalised);

      (* Important! Here pattern match to ensure that if retention test tells us that clause is eliminated, parents are still recorded *)
      match TrivRules.simplify conclusion with
      | Simplified _ as result -> result
      | Eliminated parents -> Eliminated (parents @ Clause.get_demod_eqs conclusion)
    ) else (
      Simplified clause
    )

  let simplify state clause = 
    let fixpoint = true in
    (* Due to renaming, here we also need to re *)
    if fixpoint then
      Fw_result.fix_point (simplify state) clause
    else
      simplify state clause
end

module FwDemodLightNormLoop (RetentionTest : TrivRule) : FwRule = struct
  let make_intermediate_dead state clause =
    dbg D_demod @@ lazy (sprintf "FwDemodLightNormLoop:make_intermediate_dead %s" (Clause.to_string_tptp clause));
    try Set.add_dead state clause
    with Set.Is_mem -> try set_dead_and_remove state clause
    with Set.Is_dead -> ()
  let make_intermediate_dead state clause = ()

  let make_intermediate_dead_if_different state old_clause clause =
    if clause != old_clause then (
      dbg D_demod @@ lazy (sprintf "FwDemodLightNormLoop:make_intermediate_dead %s" (Clause.to_string_tptp clause));
      make_intermediate_dead state old_clause
    )

  let check_existing_and_do_retention state parents clause =
    if Set.mem_any state clause then (
      dbg D_demod @@ lazy "Avoided repeated demod/lightnorm";
      Eliminated parents
    ) else (
      RetentionTest.simplify clause
    )
  (* let check_existing_and_do_retention state clause = Simplified clause *)

  let check_existing_and_do_retention_if_different state parents old_clause clause =
    if clause != old_clause then
      check_existing_and_do_retention state parents clause
    else
      Simplified clause

  let rec simplify ~first ~parents_acc state clause =
    let open Fw_result.O in

    (* The original clause, don't check if it is repeated, otherwise do *)
    if not first && Set.mem_any state clause then (
      dbg D_demod @@ lazy "Avoided repeated demod/lightnorm";
      Eliminated parents_acc
    ) 

    else (
      (* Light normalise *)
      let* clause' = FwLightNorm.simplify state clause in
      (* TODO if not first also we're done? because now also no need for iterating until fixpoint on demodulation *)
      (* dassert to check that *)
      if Clause.Bc.(clause' != clause) then make_intermediate_dead state clause;
      let parents_acc = if clause' != clause then Clause.get_demod_eqs clause' @ parents_acc else parents_acc in

      (* Moved to FwLightNorm *)
      (* check_existing_and_do_retention_if_different state parents_acc clause clause' >>= fun clause'_ret ->
      if Clause.Bc.(clause'_ret != clause') then make_intermediate_dead state clause'; *)
      
      (* Demodulate *)
      let* clause'' = FwDemod.simplify state clause' in
      FwDemod.assert_some_candidate_dbg state clause' clause'';
      let parents_acc = if clause'' != clause' then Clause.get_demod_eqs clause'' @ parents_acc else parents_acc in

      (* If no demodulation, we are done *)
      if Clause.Bc.(clause'' == clause') then (
        Simplified clause'
      ) 

      (* If clause was demodulated, apply retention tests and go for another round *)
      else (
        (* Also, store the previous as dead (because it has been demodulated to this) *)
        make_intermediate_dead state clause';

        (* Moved to FwDemod *)
        (* (* RetentionTest.simplify *) check_existing_and_do_retention state parents_acc clause'' >>= fun clause''_ret ->
        if Clause.Bc.(clause''_ret != clause'') then make_intermediate_dead state clause''; *)

        simplify ~first:false ~parents_acc state clause''
      )
    )
  let simplify state clause =
    simplify ~first:true ~parents_acc:[] state clause
end

module FwDemodLightNormLoopTriv = FwDemodLightNormLoop(TrivRules)



(* module BwLightNorm : BwRule = struct
  let index = Index_tag.LightNorm

  let simplify state clause = 
    let open Fw_result.O in
    let context = Set.(state.context) in
    let new_ref = ref [] in
    let old_ref = ref [] in
    context |> BCMap.iter (fun clause param ->
      (* let clause' = FwLightNorm.simplify clause in *)
      match FwLightNorm.simplify state clause with
      | Simplified clause' ->
        if Clause.Bc.(clause' != clause) then (
          (* [clause] bw removed, [clause'] bw added *)
          set_dead_and_remove state clause;
          ListExtra.cons_ref clause  old_ref;
          ListExtra.cons_ref clause' new_ref;
        )
      | Eliminated -> ()
    );
    {add=(!new_ref); remove=(!old_ref)}
end



module Orient : TrivRule = struct
  let simplify clause =
    let changed = ref false in
    let new_lits = Clause.get_lits clause |> List.map (fun lit ->
      try 
        let sign, (typ,l,r) = Term.Eq.decompose_lit_type lit in
        if Term.get_fast_key l < Term.get_fast_key r then (
          lit
        ) else (
          changed := true;
          add_lit_eq sign typ r l
        )
      with Invalid_argument _ ->
        lit
    ) in
    if !changed then (
      let tstp_source = Clause.TSTP_inference_record (Clause.EqFlip, [clause]) in
      let clause' = create_clause tstp_source new_lits in
      Statistics.(bump_int_stat sim_light_normalised);
      clause'
    ) else (
      clause
    )
  let simplify clause = 
    Simplified (simplify clause)
end *)



module ACJoinability : FwRule = struct
  exception Tautology_exn

  let simplify (state: Set.t) clause =
    Statistics.(time sim_time_joinable) @@ fun () ->

    dbg D_joinability @@ lazy "ac_joinability";

    dbg_env D_joinability2 (fun () ->
      if Clause.is_ac_axiom clause then (
        dbg D_joinability2 @@ lazy (sprintf "X Is axiom: %s" (Clause.to_string_tptp clause));
      );
      if not (AC.Table.has_ac state.ac_symbols) then (
        dbg D_joinability2 @@ lazy (sprintf "X No-ac symbols");
      );
    );
    
    if not (AC.Table.has_ac state.ac_symbols) 
    || Clause.is_ac_axiom clause
    then
      Simplified clause
    else try
      let ops = state.ac_symbols.ac in
      let some_deleted = ref false in
      let lits' = 
        Clause.get_lits clause
        |> List.filter (fun lit ->
          match Term.Eq.decompose_lit lit with
          | Some (sign, l,r) -> 
            (* let l' = AC.normalise_ac ops l in
            let r' = AC.normalise_ac ops r in
            dbg D_joinability2 @@ lazy (sprintf "  orig: %s %s" (Term.to_string l ) (Term.to_string r ));
            dbg D_joinability2 @@ lazy (sprintf "  norm: %s %s" (Term.to_string l') (Term.to_string r'));
            if l' == r' then ( *)
            if AC.equal_mod_ac ops l r then (
              if sign then (
                dbg D_joinability @@ lazy (sprintf "%s ↓ %s (pos)" (Term.to_string l) (Term.to_string r));
                raise_notrace Tautology_exn
              ) else (
                dbg D_joinability @@ lazy (sprintf "%s ↓ %s (neg)" (Term.to_string l) (Term.to_string r));
                some_deleted:= true; false
              )
            ) else (
              true
            )
          | None -> 
            true
        )
      in
      if !some_deleted then (
        dbg D_joinability @@ lazy (sprintf "Joinable neg: %s" (Clause.to_string_tptp clause));
        let source = Clause.tstp_source_theory_normalisation ~main:clause ~axioms:state.ac_symbols.axiom_list in
        let clause' = create_clause ~normalise_eqs:true source lits' in
        Statistics.(incr_int_stat (Clause.length clause - Clause.length clause') sim_ac_joinable_simp);
        Simplified clause'
      ) else (
        Simplified clause
      )

    with Tautology_exn -> 
      Statistics.(bump_int_stat sim_ac_joinable_taut);
      dbg D_joinability @@ lazy (sprintf "Joinable pos: %s" (Clause.to_string_tptp clause));
      Eliminated state.ac_symbols.axiom_list
end


module ACNormalisation : FwRule = struct
  exception Tautology_exn

  let simplify (state: Set.t) clause =
    Statistics.(time sim_time_ac_norm) @@ fun () ->

    dbg D_acnorm @@ lazy "ac_normalisation";

    let[@inline] normalise_term ops x = 
      AC.normalise_ac_complete ~order_terms:state.order.terms ~order_uid:state.order.uid ops x
    in

    let ops = state.ac_symbols.ac in

    (* Don't touch axioms themselves *)
    if SMap.is_empty ops
    || Clause.is_ac_axiom clause 
    then (
      (* dbg D_acnorm @@ lazy (sprintf "foo %d" (SMap.cardinal ops)); *)
      Simplified clause
    ) else (
      try
        let any_change = ref false in
        let lits' = 
          Clause.get_lits clause
          |> List.X.filter_map (fun lit ->
            match Term.Eq.decompose_lit_type lit with
            | Some (sign, typ, l,r) ->
              let l' = normalise_term ops l in
              let r' = normalise_term ops r in

              (* If unchanged *)
              if l == l' && r == r' then (
                if AC.equal_mod_ac ops l r then 
                  if sign then raise_notrace Tautology_exn else None
                else
                  Some lit
              (* If modified *)
              ) else (
                any_change := true;
                (* Check if we now have a tautology s=s or contradiction s!=s *)
                if l' == r' then (
                  if sign then raise_notrace Tautology_exn else None
                (* Else we really have to construct a new eq term *)
                ) else (
                  (* But we will also check joinability first (which AC normalisation doesn't fully cover since it must be complete) *)
                  (* TODO fully split this, it doesn't need to be coupled *)
                  dbg D_acnorm @@ lazy (sprintf "checking if %s %s joinable" (Term.to_string l') (Term.to_string r'));
                  if AC.equal_mod_ac ops l' r' then (
                    dbg D_acnorm @@ lazy (sprintf "%s %s normalised to %s %s but joinable" 
                      (Term.to_string l) (Term.to_string r) (Term.to_string l') (Term.to_string r'));
                    if sign then raise_notrace Tautology_exn else None
                  ) else (
                    Statistics.(bump_int_stat sim_ac_normalised);  (* Other cases are sim_ac_joinable *)
                    Some (add_lit_eq sign typ l' r')
                  )
                )
              )
            | None ->
              let lit' = normalise_term ops lit in
              if lit == lit' then (
                Some lit
              ) else (
                any_change := true;
                Some lit'  (* already added to termDB *)
              )
          )
        in
        if !any_change then (
          (* Currently puts all AC axioms as parents. We can refine to only put as parents the specific
             ones who were used to rewrite, but this is not necessary in general. *)
          let parents = state.ac_symbols.axiom_list in
          let tstp_source = Clause.tstp_source_theory_normalisation ~main:clause ~axioms:parents in
          let clause' = create_clause ~normalise_eqs:true tstp_source lits' in
          dbg D_acnorm @@ lazy (sprintf "AC normalised %s" (Clause.to_string_tptp clause));
          dbg D_acnorm @@ lazy (sprintf "to            %s" (Clause.to_string_tptp clause'));
          let deleted_lits = Clause.length clause - Clause.length clause' in
          Statistics.(incr_int_stat deleted_lits sim_ac_joinable_simp);
          Simplified clause'
        ) else (
          Simplified clause
        )
      with Tautology_exn -> 
        dbg D_acnorm @@ lazy (sprintf "AC normalised %s" (Clause.to_string_tptp clause));
        dbg D_acnorm @@ lazy (sprintf "to            tautology");
        Statistics.(bump_int_stat sim_ac_joinable_taut);
        let parents = state.ac_symbols.axiom_list in
        Eliminated parents
    )
end



module FwACDemod = struct
  let index = FwACDemodIndex.index

  let rewrite_term state parents t = 
    match ACDemodIndex_M.get_fwd_special Set.(state.ac_demod_index) t with
    | Some (t', clause) -> 
      dbg D_fw_ac_demod @@ lazy (sprintf "%s -> %s (special)" (Term.to_string t) (Term.to_string t'));
      parents := clause :: !parents;
      t'
    | None -> 
    match ACDemodIndex_M.get_fwd Set.(state.ac_demod_index) t with
    | Some (lhs, rhs, subst, clause) -> 
      dbg D_fw_ac_demod2 @@ lazy (sprintf "t %s" (Term.to_string t));
      let t_subterms = AC.ac_subterms t in
      dbg D_fw_ac_demod2 @@ lazy (sprintf "t_subterms %s" (List.X.to_string Term.to_string t_subterms));
      let lhs_subterms = AC.ac_subterms lhs in
      dbg D_fw_ac_demod2 @@ lazy (sprintf "lhs_subterms %s" (List.X.to_string Term.to_string lhs_subterms));
      (* Assert completeness *)
      (* TODO: if we want to be able to not put ac terms into normal demod index, we have to treat completeness properly here *)
      (* dassert (fun () -> List.compare_lengths lhs_subterms t_subterms == Ord.lt || not (Subst.is_renaming subst)); *)
      (* if List.compare_lengths lhs_subterms t_subterms <> Ord.lt && Subst.is_renaming subst then t else ( *)
      if false then t else (
        let t_subterms' = 
          lhs_subterms 
          |> List.fold_left (fun acc x ->
            let x' = subst_apply subst x in
            let acc' = List.X.removeq x' acc in
            acc'
          ) t_subterms
        in
        dbg D_fw_ac_demod2 @@ lazy (sprintf "t_subterms' %s" (List.X.to_string Term.to_string t_subterms'));
        let t_subterms'' = t_subterms' @ [subst_apply subst rhs] in
        dbg D_fw_ac_demod2 @@ lazy (sprintf "t_subterms'' %s" (List.X.to_string Term.to_string t_subterms''));
        let t' = AC.mk_term (Term.get_top_symb t) t_subterms'' in
        dbg D_fw_ac_demod @@ lazy (sprintf "%s -> %s" (Term.to_string t) (Term.to_string t'));
        parents := clause :: !parents;
        t'
      ) 
    | None -> 
      t
      
  let rec demod_term (state: Set.t) parents t = 
    match t with
    | Term.Fun (sym, args, _) ->
      if state.ac_symbols.ac |> SMap.mem sym then (
        let subterms = AC.ac_subterms t in
        dbg D_fw_ac_demod @@ lazy (sprintf "demod_term: %s = %s" (Term.to_string t) (List.X.to_string Term.to_string subterms));
        let subterms' = List.map (demod_term state parents) subterms in
        dbg D_fw_ac_demod @@ lazy (sprintf "to %s" (List.X.to_string Term.to_string subterms'));
        let t' = 
          if List.X.equal ~eq:(==) subterms subterms' then
            t
          else
            AC.mk_term sym subterms'
        in
        let t'' = rewrite_term state parents t' in
        dbg D_fw_ac_demod @@ lazy (sprintf "and to %s" (Term.to_string t''));
        t''
      ) 
      else (
        dbg D_fw_ac_demod @@ lazy (sprintf "demod_term: %s = nonac" (Term.to_string t));
        let args = Term.arg_to_list args in
        let args' = List.map (demod_term state parents) args in
        if List.X.equal ~eq:(==) args args' then
          t
        else
          add_fun_term sym args'
      )
    | Term.Var _ -> t

  let demod_args (state: Set.t) parents t = 
    match t with
    | Term.Fun (sym, args, _) ->
      begin match ACDemodIndex_M.get_fwd_special Set.(state.ac_demod_index) t with
      | Some (t', clause) -> 
        dbg D_fw_ac_demod2 @@ lazy (sprintf "%s to %s (special)" (Term.to_string t) (Term.to_string t'));
        parents := clause :: !parents;
        t'
      | None -> 
        let args = Term.arg_to_list args in
        let args' = List.map (demod_term state parents) args in
        if List.X.equal ~eq:(==) args args' then
          t
        else
          add_fun_term sym args'
      end
    | Term.Var _ -> t

  exception Return
  let simplify (state: Set.t) clause = 
    Statistics.(time sim_time_fw_ac_demod) @@ fun () ->
    dbg D_fw_ac_demod @@ lazy "fw_acdemod";
    if not (AC.Table.has_ac state.ac_symbols) 
    || ACDemodulationIndex.is_special_axiom clause 
    then
      Simplified clause
    else (
      let parents = ref [] in
      try
        let lits = Clause.get_lits clause in
        (* let lits' = List.map (demod_term state parents) lits in *)
        let lits' = 
          lits |> List.filter_map (fun x -> 
            match Term.Eq.decompose_lit_type x with
            | Some (sign, etype, l, r) ->
              let complete = match state.options.demod_completeness_check with Options.Demod_check.Off -> false | _ -> true in
              let l' = if complete then demod_args state parents l else demod_term state parents l in
              let r' = if complete then demod_args state parents r else demod_term state parents r in
              if l' == r' then
                if sign then raise_notrace Return else None
              else if l != l' || r != r' then
                Some (add_lit_eq sign etype l' r')
              else 
                Some x
            | None -> 
              Some (demod_term state parents x)
          )
        in
        if List.X.is_empty !parents then
          Simplified clause
        else (
          Statistics.(bump_int_stat sim_fw_ac_demod);
          let source = Clause.tstp_source_ac_demodulation ~main:clause ~axioms:state.ac_symbols.axiom_list ~eqs:!parents in
          let clause' = create_clause ~normalise_eqs:true source lits' in
          dbg D_fw_ac_demod @@ lazy (sprintf "Fw ACDemod %s" (Clause.to_string_tptp clause));
          dbg D_fw_ac_demod @@ lazy (sprintf "to         %s" (Clause.to_string_tptp clause'));
          dbg_env D_fw_ac_demod (fun () -> 
            !parents |> List.iter (fun x ->
              dbg D_fw_ac_demod @@ lazy (sprintf "via        %s" (Clause.to_string_tptp x));
            )
          );
          Simplified clause'
        )
      with Return -> 
        dbg D_fw_ac_demod @@ lazy (sprintf "Fw ACDemod %s" (Clause.to_string_tptp clause));
        dbg D_fw_ac_demod @@ lazy (sprintf "to         tautology");
        dbg_env D_fw_ac_demod (fun () -> 
          !parents |> List.iter (fun x ->
            dbg D_fw_ac_demod @@ lazy (sprintf "via        %s" (Clause.to_string_tptp x));
          )
        );
        Eliminated !parents
    )

  let simplify state clause = 
    let fixpoint = true in
    if fixpoint then 
      Fw_result.fix_point (simplify state) clause
    else
      simplify state clause
end

module BwACDemod = struct
  let index = Index_tag.BwACDemod
  
  let demod (state: Set.t) eq_clause l r = 
    let candidates = ACDemodIndex_M.get_bwd state.ac_demod_index l in
    let results = ref [] in
    candidates |> List.iter (fun (t, subst, lst) ->
      lst |> List.iter (fun clause -> if clause == eq_clause then () else (
        let t_subterms = AC.ac_subterms t in
        let l_subterms = AC.ac_subterms l in
        let t_subterms' = 
          l_subterms |> List.fold_left (fun acc x ->
            let x' = subst_apply subst x in
            let acc' = List.X.removeq x' acc in
            acc'
          ) t_subterms
        in
        let t_subterms'' = t_subterms' @ [subst_apply subst r] in
        let t' = AC.mk_term (Term.get_top_symb t) t_subterms'' in

        let source = Clause.tstp_source_ac_demodulation ~main:clause ~axioms:state.ac_symbols.axiom_list ~eqs:[eq_clause] in
        let lits' = Clause.get_lits clause |> List.map (Term.replace t t') in
        let clause' = create_clause ~normalise_eqs:true source lits' in
        Statistics.(bump_int_stat sim_bw_ac_demod);
        dbg D_bw_ac_demod @@ lazy (sprintf "Bw ACDemod %s" (Clause.to_string_tptp clause));
        dbg D_bw_ac_demod @@ lazy (sprintf "to         %s" (Clause.to_string_tptp clause'));
        dbg D_bw_ac_demod @@ lazy (sprintf "via        %s" (Clause.to_string_tptp eq_clause));
        let result = BwSimplified {from=clause; into=clause'} in
        List.X.cons_ref result results
      ))
    );
    !results

  let simplify (state: Set.t) eq_clause = 
    Statistics.(time sim_time_fw_ac_demod) @@ fun () ->
    match Clause.get_lits eq_clause with
    | [eq_lit] -> 
      begin match Term.Eq.decompose_atom eq_lit with
      | Some (l,r) -> 
        let condition l r = 
          SMap.mem (Term.get_top_symb l) state.ac_symbols.ac
          && Term.get_num_of_symb l > Term.get_num_of_symb r  (* TODO: replace by actual weight *)
        in
        begin match state.order.oriented eq_lit with
        | GT when condition l r ->
          demod state eq_clause l r
        | LT when condition r l ->
          demod state eq_clause r l
        | EQ -> assert false
        | _ -> []
        end
      | None -> []
      end
    | _ -> []
end



module FirstClass = struct
  type index = (module Index)
  type trivRule = (module TrivRule)
  type fwRule = (module FwRule)
  type bwRule = (module BwRule)
  (* type autoFwBwRule = (module AutoFwBwRule) *)

  (* let subsumptionIndex =
    (module SubsumptionIndex : Index) *)
  let subsetSubsumptionIndex =
    (module SubsetSubsumptionIndex : Index)
  let fwDemodIndex =
    (module FwDemodIndex : Index)
  let bwDemodIndex =
    (module BwDemodIndex : Index)
  let lightNormIndex =
    (module LightNormIndex : Index)
  let lightNormIndexNoreduce =
    (module LightNormIndexNoreduce : Index)
  let smtIncrIndex =
    (module SMTIncrIndex : Index)
  let smtSetIndex =
    (module SMTSetIndex : Index)
  let unitSubsIndex =
    (module UnitSubsIndex : Index)
  let nonunitSubsIndex =
    (module NonunitSubsIndex : Index)
  (* let hybridSubsIndex =
    (module HybridSubsIndex : Index) *)
  let fwACDemodIndex =
    (module FwACDemodIndex : Index)
  let bwACDemodIndex =
    (module BwACDemodIndex : Index)

  let eqResolutionSimp =
    (module EqResolutionSimp : TrivRule)
  let tautologyElim =
    (module TautologyElim : TrivRule)
  let eqTautologyElim =
    (module EqTautologyElim : TrivRule)
  let propSubs =
    (module PropSubs : TrivRule)

  let trivRules =
    (module TrivRules : TrivRule)

  let unflattening =
    (module Unflattening : TrivRule)

  let smtSimplify =
    (module SMTSimplify : TrivRule)

  (* let subsetSubsumption =
    (module SubsetSubsumption : AutoFwBwRule) *)
  let fwSubsetSubsumption =
    (module FwSubsetSubsumption : FwRule)
  let bwSubsetSubsumption =
    (module BwSubsetSubsumption : BwRule)

  (* let fwSubsumptionPrecond f =
    let arg = 
    (* (cl_in fwSubsumptionPrecond ::clause -> cl_by:clause -> bool) -> (module FwRule) *)
    module (FwSubsumptionPrecond(module )) *)
  let fwSubsumption =
    (module FwSubsumption : FwRule)
  let fwSubsumptionNonStrict =
    (module FwSubsumptionNonStrict : FwRule)
  let bwSubsumption =
    (module BwSubsumption : BwRule)

  let fwSubsumptionRes =
    (module FwSubsumptionRes : FwRule)
  let bwSubsumptionRes =
    (module BwSubsumptionRes : BwRule)

  let fwUnitSubs =
    (module FwUnitSubs : FwRule)
  let bwUnitSubs =
    (module BwUnitSubs : BwRule)

  (* let fwHybridSubs =
    (module FwHybridSubs : FwRule)
  let bwHybridSubs =
    (module BwHybridSubs : BwRule) *)

  let fwDemod =
    (module FwDemod : FwRule)
  (* let fwDemodLoop =
    (module FwDemodLoop : TrivRule) -> (module FwRule) *)
  let fwDemodLoopTriv =
    (module FwDemodLoopTriv : FwRule)
  let bwDemod =
    (module BwDemod : BwRule)

  let fwLightNorm =
    (module FwLightNorm : FwRule)
  let fwDemodLightNormLoopTriv =
    (module FwDemodLightNormLoopTriv : FwRule)

  let acJoinability =
    (module ACJoinability : FwRule)
  let acNormalisation =
    (module ACNormalisation : FwRule)
  let fwACDemod =
    (module FwACDemod : FwRule)
  let bwACDemod =
    (module BwACDemod : BwRule)

  let smtSubs =
    (module SMTSubs : FwRule)



  open Options.SupSimplificationSetup 
  let module_of_index = function
    (* | SubsumptionIndex -> subsumptionIndex *)
    (* | SubsetSubsumptionIndex -> subsetSubsumptionIndex *)
    | FwDemodIndex -> fwDemodIndex
    | BwDemodIndex -> bwDemodIndex
    | LightNormIndexReduce -> lightNormIndex
    | LightNormIndexNoreduce -> lightNormIndexNoreduce
    | SMTIncrIndex -> smtIncrIndex
    | SMTSetIndex -> smtSetIndex
    | UnitSubsumptionIndex -> unitSubsIndex
    | NonunitSubsumptionIndex -> nonunitSubsIndex
    | FwACDemodIndex -> fwACDemodIndex
    | BwACDemodIndex -> bwACDemodIndex
    (* | HybridSubsumptionIndex -> hybridSubsIndex *)

  let module_of_trivRule = function
    (* | EqResolutionSimp -> eqResolutionSimp
    | TautologyElim -> tautologyElim
    | EqTautologyElim -> eqTautologyElim
    | TrivRules -> trivRules *)
    | PropSubs -> propSubs
    | Unflattening -> unflattening
    | SMTSimplify -> smtSimplify

  let module_of_fwRule = function
    (* | FwSubsetSubsumption -> fwSubsetSubsumption *)
    | FwSubsumption -> fwSubsumption
    (* | FwSubsumptionNonStrict -> fwSubsumptionNonStrict *)
    | FwSubsumptionRes -> fwSubsumptionRes
    | FwUnitSubsumption -> fwUnitSubs
    (* | FwHybridSubsumption -> fwHybridSubs *)
    | FwDemod -> fwDemod
    | FwDemodLoopTriv -> fwDemodLoopTriv
    | FwLightNorm -> fwLightNorm
    | FwDemodLightNormLoopTriv -> fwDemodLightNormLoopTriv
    | ACJoinability -> acJoinability
    | ACNormalisation -> acNormalisation
    | SMTSubs -> smtSubs
    | FwACDemod -> fwACDemod

  let module_of_bwRule = function
    (* | BwSubsetSubsumption -> bwSubsetSubsumption *)
    | BwSubsumption -> bwSubsumption
    | BwSubsumptionRes -> bwSubsumptionRes
    | BwUnitSubsumption -> bwUnitSubs
    (* | BwHybridSubsumption -> bwHybridSubs *)
    | BwDemod -> bwDemod
    | BwACDemod -> bwACDemod



  (* Helper functions *)
  let remove_demod_index = 
    List.filter (fun m  -> not @@ List.memq m [fwDemodIndex; bwDemodIndex; lightNormIndex; lightNormIndexNoreduce; fwACDemodIndex; bwACDemodIndex])

  let remove_demod_fw = 
    List.filter (fun m -> not @@ List.memq m [fwDemod; fwDemodLoopTriv; fwLightNorm; fwDemodLightNormLoopTriv; fwACDemod])

  let remove_demod_bw = 
    List.filter (fun m -> not @@ List.memq m [bwDemod; bwACDemod])

  let remove_ac_index = 
    List.filter (fun m  -> not @@ List.memq m [fwACDemodIndex; bwACDemodIndex])

  let remove_ac_fw = 
    List.filter (fun m -> not @@ List.memq m [acJoinability; acNormalisation; fwACDemod])

  let remove_ac_bw = 
    List.filter (fun m -> not @@ List.memq m [bwACDemod])

  (* Deprecated, this is already automatic now *)
  (* let remove_subs_index = 
    List.filter (fun (module M : Index)  -> not @@ List.memq M.index Index_tag.[Subs; SubsetSubs])

  let remove_subs_fw = 
    List.filter (fun (module M : FwRule) -> not @@ List.memq M.index Index_tag.[Subs; SubsetSubs])

  let remove_subs_bw = 
    List.filter (fun (module M : BwRule) -> not @@ List.memq M.index Index_tag.[Subs; SubsetSubs]) *)
end
