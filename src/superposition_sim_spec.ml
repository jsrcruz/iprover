open Lib
open Logic_interface



module Simplify = Simplify_new



(* First step: turn [Options.SupSimplificationSetup] into [Simplify] modules.
   - Figure out indices required by rules
   - Optionally remove demod, or demod in immed simplifications *)

type spec' = {
  indices_passive' : (module Simplify.Index) list;
  indices_active'  : (module Simplify.Index) list;
  indices_immed'   : (module Simplify.Index) list;
  indices_input'   : (module Simplify.Index) list;

  full_triv' : (module Simplify.TrivRule) list;
  full_fw' : (module Simplify.FwRule) list;
  full_bw' : (module Simplify.BwRule) list;
  
  immed_triv' : (module Simplify.TrivRule) list;
  immed_fw_immed' : (module Simplify.FwRule) list;
  immed_bw_immed' : (module Simplify.BwRule) list;
  immed_fw_main' : (module Simplify.FwRule) list;
  immed_bw_main' : (module Simplify.BwRule) list;

  input_triv' : (module Simplify.TrivRule) list;
  input_fw' : (module Simplify.FwRule) list;
  input_bw' : (module Simplify.BwRule) list;
}



let indices_of_fw_rule x = 
  let open Options.SupSimplificationSetup in
  match x with
  | FwSubsumption 
  | FwSubsumptionRes -> [NonunitSubsumptionIndex]
  | FwUnitSubsumption -> [UnitSubsumptionIndex]
  | FwDemod
  | FwDemodLoopTriv -> [FwDemodIndex]
  | FwLightNorm -> [LightNormIndexNoreduce]
  | FwDemodLightNormLoopTriv -> [FwDemodIndex; LightNormIndexNoreduce]
  | ACJoinability
  | ACNormalisation -> []
  | SMTSubs -> [SMTIncrIndex]
  | FwACDemod -> [FwACDemodIndex]

let indices_of_bw_rule x = 
  let open Options.SupSimplificationSetup in
  match x with
  | BwSubsumption 
  | BwSubsumptionRes -> [NonunitSubsumptionIndex]
  | BwUnitSubsumption -> [UnitSubsumptionIndex]
  | BwDemod -> [BwDemodIndex]
  | BwACDemod -> [BwACDemodIndex]

let indices_of_rules fw bw = 
  List.append
    (List.concat_map indices_of_fw_rule fw)
    (List.concat_map indices_of_bw_rule bw)

let add_triv l =
  let open Simplify.FirstClass in
  match l with
  | [] -> [trivRules]
  | _::_ -> trivRules :: (l @ [trivRules])

let add_ss l = 
  let open Simplify.FirstClass in
  subsetSubsumptionIndex :: l



(* Make spec' from options *)
let mk_spec' (opts: Options.SupSimplificationSetup.spec) : spec' = 
  let uniq = List.X.sort_uniq (Ord.lift Obj.magic compare) in

  let indices_passive, indices_active = 
    List.append 
      (indices_of_rules opts.full_fw       opts.full_bw)
      (indices_of_rules opts.immed_fw_main opts.immed_bw_main)
    |> uniq
    |> List.partition (fun x -> List.memq x opts.indices_passive)
  in
  let indices_immed = indices_of_rules opts.immed_fw_immed opts.immed_bw_immed |> uniq in
  let indices_input = indices_of_rules opts.input_fw       opts.input_bw       |> uniq in

  Simplify.FirstClass.{
    indices_passive' = add_ss @@ List.map module_of_index indices_passive;
    indices_active'  =           List.map module_of_index indices_active;
    indices_immed'   = add_ss @@ List.map module_of_index indices_immed;
    indices_input'   = add_ss @@ List.map module_of_index indices_input;

    full_triv' = add_triv @@ List.map module_of_trivRule opts.full_triv;
    full_fw' = List.map module_of_fwRule opts.full_fw;
    full_bw' = List.map module_of_bwRule opts.full_bw;
    
    immed_triv' = add_triv @@ List.map module_of_trivRule opts.immed_triv;
    immed_fw_immed' = List.map module_of_fwRule opts.immed_fw_immed;
    immed_bw_immed' = List.map module_of_bwRule opts.immed_bw_immed;
    immed_fw_main' = List.map module_of_fwRule opts.immed_fw_main;
    immed_bw_main' = List.map module_of_bwRule opts.immed_bw_main;

    input_triv' = add_triv @@ List.map module_of_trivRule opts.input_triv;
    input_fw' = List.map module_of_fwRule opts.input_fw;
    input_bw' = List.map module_of_bwRule opts.input_bw;
  }



(* Remove FwDemod/BwDemod/LightNorm from a [spec'] *)
let remove_demod x =
  let open Simplify.FirstClass in
  {
    indices_passive' = remove_demod_index x.indices_passive';
    indices_active'  = remove_demod_index x.indices_active';
    indices_immed'   = remove_demod_index x.indices_immed';
    indices_input'   = remove_demod_index x.indices_input';

    full_triv' = x.full_triv';
    full_fw' = remove_demod_fw x.full_fw';
    full_bw' = remove_demod_bw x.full_bw';
    
    immed_triv' = x.immed_triv';
    immed_fw_immed' = remove_demod_fw x.immed_fw_immed';
    immed_bw_immed' = remove_demod_bw x.immed_bw_immed';
    immed_fw_main' = remove_demod_fw x.immed_fw_main';
    immed_bw_main' = remove_demod_bw x.immed_bw_main';

    input_triv' = x.input_triv';
    input_fw' = remove_demod_fw x.input_fw';
    input_bw' = remove_demod_bw x.input_bw';
  }

(* Remove Subsumption/SubsetSubsumption from a [spec'] *)
(* let remove_subs x =
  let open Simplify.FirstClass in
  {
    indices_passive' = remove_subs_index x.indices_passive';
    indices_active'  = remove_subs_index x.indices_active';
    indices_immed'   = remove_subs_index x.indices_immed';
    indices_input'   = remove_subs_index x.indices_input';

    full_triv' = x.full_triv';
    full_fw' = remove_subs_fw x.full_fw';
    full_bw' = remove_subs_bw x.full_bw';
    
    immed_triv' = x.immed_triv';
    immed_fw_main' = remove_subs_fw x.immed_fw_main';
    immed_fw_immed' = remove_subs_fw x.immed_fw_immed';
    immed_bw_main' = remove_subs_bw x.immed_bw_main';
    immed_bw_immed' = remove_subs_bw x.immed_bw_immed';

    input_triv' = x.input_triv';
    input_fw' = remove_subs_fw x.input_fw';
    input_bw' = remove_subs_bw x.input_bw';
  } *)

(* Remove FwDemod/BwDemod/LightNorm from a [spec'] *)
let remove_ac x =
  let open Simplify.FirstClass in
  {
    indices_passive' = remove_ac_index x.indices_passive';
    indices_active'  = remove_ac_index x.indices_active';
    indices_immed'   = remove_ac_index x.indices_immed';
    indices_input'   = remove_ac_index x.indices_input';

    full_triv' = x.full_triv';
    full_fw' = remove_ac_fw x.full_fw';
    full_bw' = remove_ac_bw x.full_bw';
    
    immed_triv' = x.immed_triv';
    immed_fw_immed' = remove_ac_fw x.immed_fw_immed';
    immed_bw_immed' = remove_ac_bw x.immed_bw_immed';
    immed_fw_main' = remove_ac_fw x.immed_fw_main';
    immed_bw_main' = remove_ac_bw x.immed_bw_main';

    input_triv' = x.input_triv';
    input_fw' = remove_ac_fw x.input_fw';
    input_bw' = remove_ac_bw x.input_bw';
  }

(* As [remove_demod] but only ffrom immed, and immed_bw_main *)
let remove_immeddemod x =
  let open Simplify.FirstClass in
  {
    indices_passive' = x.indices_passive';
    indices_active'  = x.indices_active';
    indices_immed'   = remove_demod_index x.indices_immed';
    indices_input'   = x.indices_input';

    full_triv' = x.full_triv';
    full_fw' = x.full_fw';
    full_bw' = x.full_bw';
    
    immed_triv' = x.immed_triv';
    immed_fw_immed' = remove_demod_fw x.immed_fw_immed';
    immed_bw_immed' = remove_demod_bw x.immed_bw_immed';
    immed_fw_main' = x.immed_fw_main';
    immed_bw_main' = remove_demod_bw x.immed_bw_main';

    input_triv' = x.input_triv';
    input_fw' = x.input_fw';
    input_bw' = x.input_bw';
  }



(* Second step: turn [Simplify] modules into functions.
   - indices_* will add the clause to those indices
   - full/immed/input_* will perform those simplifications in sequence, and 
     until fixpoint if options say so *)

type spec = {
  indices_passive : (clause -> unit);
  indices_active  : (clause -> unit);
  indices_immed   : Simplify.set -> (clause -> unit);
  indices_input   : (clause -> unit);

  full_triv : (clause -> Simplify.fw_result);
  full_fw : (clause -> Simplify.fw_result);
  full_bw : (clause -> Simplify.bw_result);
  
  immed_triv : (clause -> Simplify.fw_result);
  immed_fw_immed : Simplify.set -> (clause -> Simplify.fw_result);
  immed_bw_immed : Simplify.set -> (clause -> Simplify.bw_result);  
  immed_fw_main  : (* Simplify.set -> *) (clause -> Simplify.fw_result);
  immed_bw_main  : (* Simplify.set -> *) (clause -> Simplify.bw_result);  

  input_triv : (clause -> Simplify.fw_result);
  input_fw : (clause -> Simplify.fw_result);
  input_bw : (clause -> Simplify.bw_result);
}

let mk_spec_inner (x:spec') ~sim_state ~imsim_state (opts:Options.SupSimplificationSetup.spec) : spec =
  let index_module_to_func state l = 
    List.map (fun (module M : Simplify.Index) -> M.index) l
    |> Simplify.add_to_indices state
  in

  let triv_module_to_func l =
    List.map (fun (module M : Simplify.TrivRule) -> M.simplify) l
    |> Simplify.Fw_result.fold
  in
  
  let fw_module_to_func ~fixpoint state l =
    if List.X.is_empty l then
      Simplify.Fw_result.return
    else if not fixpoint then
      List.map (fun (module M : Simplify.FwRule) -> M.simplify state) l
      |> Simplify.Fw_result.fold
    else 
      let assert_local_fixpoint = true in
      let assert_global_fixpoint = true in
      let eq_simp x s = match s with Simplify.Simplified x' -> x == x' | Simplify.Eliminated _ -> false in
      (* let fp opt f = if opt then Simplify.Fw_result.fix_point f else f in *)
      let array_f : (clause -> Simplify.fw_result) array = 
        let p[@inline] = fun (module M : Simplify.FwRule) -> M.simplify state in
        l |> List.to_seq |> Seq.map p |> Array.of_seq
      in
      let array_results : clause array = 
        Array.make (Array.length array_f) (Obj.magic 0)
      in
      (* let array_dirty : bool array = 
        let p[@inline] = fun m -> not @@ List.memq m Simplify.FirstClass.[fwSubsumption; fwSubsumptionRes; fwUnitSubs; fwSubsumptionNonStrict] in
        l |> List.to_seq |> Seq.map p |> Array.of_seq
      in *)
      let n = Array.length array_f in

      let rec loop_2 (*dirty*) i x = 
        (* printf "loop_2 %b %d %s\n" dirty i (Clause.to_string_tptp x); *)
        if i = n then loop_2 (*dirty*) 0 x else
        if (* not dirty || *) Clause.Bc.(x == array_results.(i)) then (
          if assert_global_fixpoint then dassert (fun () -> 
            array_f |> Array.for_all (fun f -> eq_simp x (f x));
          );
          Simplify.Simplified x
        ) else (
          let x' = array_f.(i) x in
          match x' with
          | Simplify.Eliminated _ -> 
            x'
          | Simplify.Simplified x' -> 
            if assert_local_fixpoint && x' != x then dassert (fun () -> eq_simp x' (array_f.(i) x'));
            array_results.(i) <- x';
            (* let dirty' = dirty || (x' != x (*&& array_dirty.(i)*)) in *)
            loop_2 (*dirty'*) (i+1) x'
        )
      in

      let rec loop_1 (*dirty*) i x = 
        (* printf "loop_1 %b %d %s\n" dirty i (Clause.to_string_tptp x); *)
        if i = n then loop_2 (*dirty*) 0 x else
        let x' = array_f.(i) x in
        match x' with
        | Simplify.Eliminated _ -> 
          x'
        | Simplify.Simplified x' -> 
          if assert_local_fixpoint && x' != x then dassert (fun () -> eq_simp x' (array_f.(i) x'));
          array_results.(i) <- x';
          (* let dirty' = dirty || (x' != x (*&& array_dirty.(i)*)) in *)
          loop_1 (*dirty'*) (i+1) x'
      in

      fun c -> loop_1 (*false*) 0 c
  in
  
  let bw_module_to_func state l =
    List.map (fun (module M : Simplify.BwRule) -> M.simplify state) l
    |> Simplify.Bw_result.fold
  in
  
  {
    indices_passive = index_module_to_func sim_state x.indices_passive';
    indices_active  = index_module_to_func sim_state x.indices_active';
    indices_immed   = (fun s -> index_module_to_func s x.indices_immed');
    indices_input   = index_module_to_func imsim_state x.indices_input';

    full_triv = triv_module_to_func x.full_triv';
    full_fw = fw_module_to_func sim_state x.full_fw' ~fixpoint:opts.full_fixpoint;
    full_bw = bw_module_to_func sim_state x.full_bw';
    
    immed_triv = triv_module_to_func x.immed_triv';
    immed_fw_immed = (fun s -> fw_module_to_func s x.immed_fw_immed' ~fixpoint:opts.immed_fixpoint);
    immed_bw_immed = (fun s -> bw_module_to_func s x.immed_bw_immed');
    immed_fw_main  = fw_module_to_func sim_state x.immed_fw_main' ~fixpoint:opts.main_fixpoint;
    immed_bw_main  = bw_module_to_func sim_state x.immed_bw_main';

    input_triv = triv_module_to_func x.input_triv';
    input_fw = fw_module_to_func imsim_state x.input_fw' ~fixpoint:opts.input_fixpoint;
    input_bw = bw_module_to_func imsim_state x.input_bw';
  }



let mk_spec ~demod_flag (* ~subs_flag *) ~ac_flag ~sim_state ~imsim_state x =
  let spec' = 
    mk_spec' x
    |> (if demod_flag then Fun.id else remove_demod)
    |> (if ac_flag then Fun.id else remove_ac)
  in
  mk_spec_inner spec' ~sim_state ~imsim_state x
  (* match demod_flag, subs_flag with
  | true, true ->
    mk_spec_inner (mk_spec' x) ~sim_state ~imsim_state
  | false, true ->
    mk_spec_inner (remove_demod @@ mk_spec' x) ~sim_state ~imsim_state
  | true, false ->
    mk_spec_inner (remove_subs @@ mk_spec' x) ~sim_state ~imsim_state
  | false, false -> 
    assert false *)

let mk_spec_noimmeddemod ~sim_state ~imsim_state x = 
  let spec' = remove_immeddemod (mk_spec' x) in
  mk_spec_inner spec' ~sim_state ~imsim_state x

(* Disable immed inter-simplification *)
let disable_immed spec = { spec with
  indices_immed = (Simplify_new.SubsetSubsumptionIndex.add);
  (* immed_triv = []; *)  (* Triv should be kept *)
  immed_fw_immed = (fun _ -> Simplify.Fw_result.return);
  (* immed_bw_immed = (fun _ -> []); *)  (* And actually bw should be kept also, due to given clause simplification *)
}
