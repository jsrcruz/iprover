(*----------------------------------------------------------------------(C)-*)
(* Copyright (C) 2006-2016 Konstantin Korovin and The University of Manchester. 
   This file is part of iProver - a theorem prover for first-order logic.

   iProver is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 2 of the License, or 
   (at your option) any later version.
   iProver is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
   See the GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with iProver.  If not, see <http://www.gnu.org/licenses/>.         *)
(*----------------------------------------------------------------------[C]-*)



open Lib

(*----- debug modifiable part-----*)

let dbg_flag = false

type dbg_gr = 
  | D_trace
  | D_internals
  | D_symb

let dbg_gr_to_str = function 
  | D_trace -> "trace"
  | D_internals -> "internals"
  | D_symb -> "symb"
  
let dbg_groups = [
  D_trace;
  D_internals;
  D_symb;
]
    
let module_name = "orderings_opt"

(*----- debug fixed part --------*)

let () = dbg_flag_msg dbg_flag module_name

let dbg group str_lazy = 
  Lib.dbg_out_pref dbg_flag dbg_groups group dbg_gr_to_str module_name str_lazy

let dbg_env group f = 
  Lib.dbg_env_set dbg_flag dbg_groups group f
    
(*----- debug -----*)





open PartialOrd
type term = Term.term
type atom = Term.atom
type lit  = Term.lit
type symbol = Term.symbol



(* module TypesDB = struct
  type t = {
    map: int Symbol.Map.t;
    ntypes: int;
  }

  let empty = {
    map = Symbol.Map.empty;
    ntypes = 0;
  }

  let add db x =
    if Symbol.Map.mem x db.map then (
      db
    ) else (
      dbg D_internals @@ lazy (sprintf "type %s goes to %d" (Symbol.to_string x) ((* succ *) db.ntypes));
      {
        map = Symbol.Map.add x db.ntypes db.map;
        ntypes = succ db.ntypes;
      }
    )

  let to_int db x =
    Symbol.Map.find x db.map
end

let types = ref TypesDB.empty

(* External interface *)
let add_var_type x =
  types := TypesDB.add !types x *)


(** Variable balance:
    When comparing [s] and [t], in each index i, we add +1 for each occurrence of i-th variable in [s], and -1 for each occurrence of the i-th variable in [t]. *)
module VarBalance = struct
  type t = {
    mutable arr: int array;
    mutable dirty: bool;
    (* mutable neg_count: int; *)
    (* mutable pos_count: int; *)
  }

  let make n : t = {
    arr = Array.make n 0;
    dirty = false;
  }

  let resize n x = 
    dbg D_internals @@ lazy (sprintf "resize to %d" n);
    x.arr <- Array.make n 0;
    x.dirty <- false

  (* let make_from_term term = 
    make (Term.get_num_of_var term)

  let make_from_terms t s : t =
    dbg D_internals @@ lazy (sprintf "make_from_terms %s %s" (Term.to_string t) (Term.to_string s));
    let vars_t = Term.get_num_of_var t in
    let vars_s = Term.get_num_of_var s in
    dbg D_internals @@ lazy (sprintf "make_from_terms %d %d" (vars_t) (vars_s));
    make (10*(vars_t + vars_s))  (* TODO: improve bound *) *)

  let length_hard x =
    dbg D_internals @@ lazy (sprintf "length_hard %d" (Array.length x.arr));
    Array.length x.arr

  let length_soft _ = 
    let result = Var.max_var_index() * Symbol.get_num_types() in
    dbg D_internals @@ lazy (sprintf "length_soft %d" result);
    result

  let clear x = 
    let n = length_soft () in
    dbg D_internals @@ lazy (sprintf "clearing to %d" n);
    (* ignore @@ length_soft (); *)
    Array.fill x.arr 0 n 0;
    x.dirty <- false

  let change f balance var =
    (* Use a "dirty" flag to only clear when necessary (more specifically, 
       when terms are ground this is not needed) *)
    if balance.dirty then clear balance;
    let n_val = Var.get_var_val var in
    let n_type = 
      Var.get_type var 
      |> Symbol.get_basic_type_id
    in
     (*  try
        (* Type already assigned an int *)
        TypesDB.to_int !types (Var.get_type var)
      with Not_found ->
        (* New type, add to db *)
        types := TypesDB.add !types (Var.get_type var);
        (* Number of vars have changed, so we may have to stretch the array *)
        let length_soft = length_soft () in
        let length_hard = length_hard balance in
        if length_soft >= length_hard then (
          dbg D_internals @@ lazy (sprintf "blit ofs=%d len=%d " 0 length_hard);
          let arr' = Array.make (3 * length_soft) 0 in
          Array.blit balance.arr 0 arr' 0 length_hard;
          balance.arr <- arr'
        ) 
        else (
          dbg D_internals @@ lazy (sprintf "fill ofs=%d len=%d " (length_soft - Var.max_var_index()) (Var.max_var_index()));
          Array.fill balance.arr (length_soft - Var.max_var_index()) (Var.max_var_index()) 0;
        );
        pred Symbol.get_num_types()
    in *)

    let idx = n_type * Var.max_var_index() + n_val in
    dbg D_internals @@ lazy (sprintf "%d %d %d" n_val n_type idx);
    dbg D_internals @@ lazy (sprintf "max %d * %d = %d" 
      (Var.max_var_index()) (Symbol.get_num_types()) (length_soft balance)
    );
    dassert (fun () -> idx < length_soft balance);
    let n = balance.arr.(idx) in
    (* Update counters if sign is going to change *)
    (* if n = 0 then (
      balance.pos_count <- (succ|pred) balance.pos_count
    ) else if n = -1 then (
      balance.neg_count <- (pred|succ) balance.neg_count
    ); *)
    balance.arr.(idx) <- f n

  let incr = change succ

  let decr = change pred

  let upd pos = change (fun x -> x + pos)



  let rec update_lex balance pos terms =
    List.iter (update balance pos) terms

  and update balance pos term =
    match term with
    | Term.Var (var, _) ->
      upd pos balance var
      (* if Bool.O.(pos = true) then
        incr balance var
      else
        decr balance var *)
    | Term.Fun (_, args, _) ->
      if not (Term.is_ground term) then
        update_lex balance pos (Term.arg_to_list args)

  (* Updates the variable balance, and returns true if a variable x is contained in s *)
  let rec update_count_lex balance pos x terms =
    match terms with
    | [] -> false
    | hd::tl -> 
      let occurs = update_count balance pos x hd in
      if not occurs then (
        update_count_lex balance pos x tl
      ) else (
        (* We can revert to the update without counting, because we know the result is [true], but we still need to update for the rest of the terms in tl *)
        update_lex balance pos tl;
        true
      )

  and update_count balance pos x term =
    match term with
    | Term.Var (var, _) -> 
      upd pos balance var;
      (* if Bool.O.(pos = true) then
        incr balance var
      else 
        decr balance var *)
      dbg D_internals @@ lazy (sprintf "ree %s %s" (Var.to_string x) (Var.to_string var));
      Var.O.(x = var) |> tap (fun x -> dbg D_internals @@ lazy (sprintf "%B" x))
    | Term.Fun (_, args, _) ->
      if not (Term.is_ground term) then 
        update_count_lex balance pos x (Term.arg_to_list args)
      else
        false
      (* List.fold_left (fun acc term' -> 
        let res = update balance pos x term' in
        acc || res
      ) false (Term.arg_to_list args) *)



  let for_all' p arr n =
    let rec loop i =
      if i > n then 
        true
      else if p arr.(i) then 
        loop (succ i)
      else 
        false
    in
    loop 0

  let no_neg x =
    (* x.neg_count = 0 *)
    x.dirty ||
    for_all' (fun y -> y>=0) x.arr (length_soft x)

  let no_pos x =
    (* x.pos_count = 0 *)
    x.dirty ||
    for_all' (fun y -> y<=0) x.arr (length_soft x)
end

(* let var_check t s =
  let balance = VarBalance.make_from_terms t s in
  update balance true t;
  update balance false s; *)

(* module State = struct
  type t = {
    var_bal: VarBalance.t;
    mutable weight_bal: int
  }

  let make t s : t =
    let vars_t = Term.get_num_of_var t in
    let vars_s = Term.get_num_of_var s in
    {
      var_bal = VarBalance.make (vars_t + vars_s);  (* TODO: wasteful *)
      weight_bal = 0;
    }

  let incr balance t
end *)

(**  *)
(* let check_balances  t pos = *)

let global_balance = VarBalance.make 100



(***************)
(* Term weight *)
(***************)

(* Num of symbols (all symbols same weight) *)
(* let get_weight = Term.get_num_of_symb *)

(*********************)
(* Symbol comparison *)
(*********************)

(* TODO: compare by number of occurrences *)

(* Hack for now: a global flag *)
(***
let compare_symb_fun = ref compare_symb_arity_random

let set_compare_symb x =
  let open Options.Symb_ordering in
  match x with
  | Arity       -> compare_symb_fun := compare_symb_arity
  | ArityRev    -> compare_symb_fun := compare_symb_arity_rev
  | ArityRandom -> compare_symb_fun := compare_symb_arity_random
  | Random      -> compare_symb_fun := compare_symb_random

let get_compare_symb () = 
  let open Options.Symb_ordering in
  let x = !compare_symb_fun in
  if x == compare_symb_arity then
    Arity
  else if x == compare_symb_arity_rev then
    ArityRev
  else if x == compare_symb_arity_random then
    ArityRandom
  else if x == compare_symb_random then
    Random
  else
    assert false

let get_ordering_id () = 
  let x = !compare_symb_fun in
  if x == compare_symb_arity        then 0 else
  if x == compare_symb_arity_rev    then 1 else
  if x == compare_symb_arity_random then 2 else
  if x == compare_symb_random       then 3 else 
  assert false
  
let compare_symb x y = 
  let r = (!compare_symb_fun) x y in
  dbg D_symb @@ lazy (sprintf "compare_symb: %s %s %s" (Symbol.to_string x) (Ord.to_sign r) (Symbol.to_string y));
  r
***)





let rec kbo_lex ~weight ~symb_ordering balance t_list s_list =
  match t_list, s_list with
  | [], [] -> 
    EQ
  | t_hd::t_tl, s_hd::s_tl ->
    let res = kbo_terms ~weight ~symb_ordering balance t_hd s_hd in
    if PartialOrd.O.(res = EQ) then (
      kbo_lex ~weight ~symb_ordering balance t_tl s_tl
    ) else (
      (* Update the remaining terms *)
      VarBalance.update_lex balance (+1) t_tl;
      VarBalance.update_lex balance (-1) s_tl;
      res
    )
  | _ -> failwith "kbo_lex: mismatched list length"

and kbo_terms ~weight ~symb_ordering balance t s = 
  dbg D_trace @@ lazy (sprintf "stepping into %s / %s" (Term.to_string t) (Term.to_string s));
  if t == s then
    EQ

  else
    match t,s with
    | Term.Var (x, _), Var (y, _) ->
      VarBalance.incr balance x;
      VarBalance.decr balance y;
      if Var.O.(x = y) then 
        EQ
      else
        INC

    | Term.Fun (sym, _, _), Term.Var (y, _) ->
      (* if Symbol.is_smallest sym then LT else *)
      let occurs = VarBalance.update_count balance (+1) y t in
      VarBalance.decr balance y;
      if occurs then 
        GT
      else
        INC

    | Term.Var (x, _), Term.Fun (sym, _, _) ->
      (* if Symbol.is_smallest sym then GT else *)
      let occurs = VarBalance.update_count balance (-1) x s in
      VarBalance.incr balance x;
      if occurs then 
        LT
      else
        INC

    | Term.Fun (tsymb, targs, _), Term.Fun (ssymb, sargs, _) ->
      let targs = Term.arg_to_list targs in
      let sargs = Term.arg_to_list sargs in
      (* "Pacman lemma" optimisation *)
      begin match targs, sargs with
      | [targ], [sarg] when Symbol.equal tsymb ssymb ->
        kbo_terms ~weight ~symb_ordering balance targ sarg
      | _ ->
        let lex = 
          if tsymb == ssymb then (
            kbo_lex ~weight ~symb_ordering balance targs sargs
          ) else (
            if not (Term.is_ground t) then VarBalance.update_lex balance (+1) targs;
            if not (Term.is_ground s) then VarBalance.update_lex balance (-1) sargs;
            INC
          )
        in
        let[@inline] gt_or_inc() = if VarBalance.no_neg balance then GT else INC in
        let[@inline] lt_or_inc() = if VarBalance.no_pos balance then LT else INC in
        let weight_balance = weight t - weight s in
        if weight_balance > 0 then
          gt_or_inc()
        else if weight_balance < 0 then
          lt_or_inc()
        else 
        let symb_balance = symb_ordering tsymb ssymb in
        if symb_balance > 0 then
          gt_or_inc()
        else if symb_balance < 0 then
          lt_or_inc()
        (* else if symb_balance =  *)  (* TODO check *)
        else 
          match lex with
          | EQ -> EQ
          | GT -> gt_or_inc()
          | LT -> lt_or_inc()
          | INC -> INC
      end



(* Compare terms, given a weight and a symbol ordering *)
let[@inline] kbo_terms ~weight ~symb_ordering = fun s t ->
  Statistics.(time orderings_time) @@ fun () ->  

  dbg D_trace @@ lazy (sprintf "-- kbo_terms: %s , %s" (Term.to_string s) (Term.to_string t));
  (* If global_balance needs to be extended, do so *)
  let length_soft = VarBalance.length_soft () in
  let length_hard = VarBalance.length_hard global_balance in
  if length_soft >= length_hard then (
    dbg D_internals @@ lazy (sprintf "updated to size %d" (3 * length_soft));
    VarBalance.resize (3 * length_soft) global_balance
  );
  (* else (VarBalance.clear global_balance); *)

  Statistics.(bump_int_stat comparisons_done);
  let result = kbo_terms ~weight ~symb_ordering global_balance s t in

  dbg D_trace @@ lazy (sprintf "-- result terms: %s %s %s" (Term.to_string s) (PartialOrd.to_string result) (Term.to_string t));
  VarBalance.(global_balance.dirty <- true);
  result

(* Compare lhs and rhs of an equation/disequation *)
let[@inline] mk_kbo_oriented uid kbo_terms = 
  fun lit -> Term.Eq.oriented kbo_terms uid lit

(* Compare atoms, given a comparison on terms *)
let[@inline] kbo_atoms kbo_terms = fun p q -> 
  dbg D_trace @@ lazy (sprintf "-- kbo_atoms %s , %s" (Term.to_string p) (Term.to_string q));
  let first =
    lex_combination1
      (Ord.reverse_f Term.cmp_top)
      (* (Ord.reverse_f Term.cmp_split); *)  (* Moved to symbol precedence *)
      (* (cmp_top_symb symb_precendence); *) 
      p q
  in
  (if first <> Ord.eq then
    PartialOrd.of_ord first
  else
    kbo_terms p q)
  |> tap (fun x -> dbg D_trace @@ lazy (sprintf "-- result atoms: %s %s %s" (Term.to_string p) (PartialOrd.to_string x) (Term.to_string q)))

(* Compare predicate literals, given a comparison on atoms *)
let[@inline] kbo_predlits kbo_atoms = fun sign1 atom1 sign2 atom2 -> 
  dbg D_trace @@ lazy (sprintf "-- kbo_predlits %c %s , %c %s" 
    (if sign1 then '+' else '-') (Term.to_string atom1) 
    (if sign2 then '+' else '-') (Term.to_string atom2)
  );
  (* First compare atoms, if equal compare sign *)
  (
  if atom1 != atom2 then 
    kbo_atoms atom1 atom2
  else 
    Bool.compare sign2 sign1 |> PartialOrd.of_ord 
  ) |> tap (fun x -> dbg D_trace @@ lazy (sprintf "-- result predlits: %s" (PartialOrd.to_string x)))

(* Compare literals, given a comparison on terms *)
let[@inline] kbo_lits kbo_terms kbo_oriented = fun l k -> 
  dbg D_trace @@ lazy (sprintf "-- kbo_lits %s , %s" (Term.to_string l) (Term.to_string k));
  let kbo_atoms = kbo_atoms kbo_terms in
  let kbo_predlits = kbo_predlits kbo_atoms in

  (* Small optimisation *)
  let is_pred rhs = 
    match rhs with 
    | Term.Fun (sym,_,_) -> sym == Symbol.symb_top
    | Term.Var _ -> false
  in

  (

  let sign1, atom1 = Term.split_sign_lit l in
  let sign2, atom2 = Term.split_sign_lit k in
  match Term.Eq.decompose_atom atom1, Term.Eq.decompose_atom atom2 with
  (* Case: both eqs *)
  | Some (l1,r1), Some (l2,r2) -> (
    (* Need to further check if they are predicates or not (syntactic equalities that are semantically non-equality) *)
    dbg D_trace @@ lazy "Eq case";
    match is_pred r1, is_pred r2 with
    | true, true -> 
      dbg D_trace @@ lazy "pred vs pred";
      kbo_predlits sign1 l1 sign2 l2

    | true, false -> 
      dbg D_trace @@ lazy "pred > equality";
      GT
    | false, true -> 
      dbg D_trace @@ lazy "equality < pred";
      LT

    | false, false ->
    (* if Term.Eq.is_predicate_eq l && not (Term.Eq.is_predicate_eq k) then (
      dbg D_trace @@ lazy "immediate";
      GT
    ) else if not (Term.Eq.is_predicate_eq l) && Term.Eq.is_predicate_eq k then (
      dbg D_trace @@ lazy "immediate";
      LT
    ) else ( *)
      dbg D_trace @@ lazy "eq vs eq";
      (* let sign1, (s,t) = Term.Eq.decompose_lit l in
      let sign2, (u,v) = Term.Eq.decompose_lit k in *)
      let s = l1 in
      let t = r1 in
      let u = l2 in
      let v = r2 in

      let mode1 = 
        (* Special cases that need only 1 comparison *)
        let fast_result = 
          if Bool.O.(sign1 = sign2) then
            if s == u then (
              dbg D_trace @@ lazy (sprintf "fast: %s == %s : res = %s v %s" 
                (Term.to_string s) (Term.to_string u)
                (Term.to_string t) (Term.to_string v)
              );
              Some (kbo_terms t v)
            ) else if s == v then (
              dbg D_trace @@ lazy (sprintf "fast: %s == %s : res = %s v %s" 
                (Term.to_string s) (Term.to_string v)
                (Term.to_string t) (Term.to_string u)
              );
              Some (kbo_terms t u)
            ) else if t == u then (
              dbg D_trace @@ lazy (sprintf "fast: %s == %s : res = %s v %s" 
                (Term.to_string t) (Term.to_string u)
                (Term.to_string s) (Term.to_string v)
              );
              Some (kbo_terms s v)
            ) else if t == v then (
              dbg D_trace @@ lazy (sprintf "fast: %s == %s : res = %s v %s" 
                (Term.to_string t) (Term.to_string v)
                (Term.to_string s) (Term.to_string u)
              );
              Some (kbo_terms s u)
            ) else (
              None
            )
          else
            None
        in

        match fast_result with 
        | Some x -> x
        | None ->
          (* let id = get_ordering_id() in *)
          let st = kbo_oriented (* kbo_terms id *) l in
          let uv = kbo_oriented (* kbo_terms id *) k in
          dbg D_trace @@ lazy (sprintf "%s %s"
            (PartialOrd.to_string st)
            (PartialOrd.to_string uv)
          );
          let su = (fun () -> kbo_atoms s u) in
          let tv = (fun () -> kbo_atoms t v) in
          let sv = (fun () -> kbo_atoms s v) in
          let tu = (fun () -> kbo_atoms t u) in

          (* Here we use the tree *)
          let open Orderings_eq_table in
          odd
          |> query_bin sign1
          |> query_bin sign2
          |> query_ord st
          |> query_ord uv
          |> query_ord_lazy su
          |> query_ord_lazy tv
          |> query_ord_lazy sv
          |> query_ord_lazy tu
          |> query_term
          (* Orderings_eq_table.get ~sign1 ~sign2 ~st ~uv ~su ~tv ~sv ~tu *)
      in

      (* If dbg_flag = true, also try with multiset ordering and assert that they are equal. *)
      let check_with_multiset = true in
      dbg_env D_trace @@ (fun () -> if check_with_multiset then (
        dbg D_trace @@ lazy (sprintf "mode1: %s" (PartialOrd.to_string mode1));

        let mode2 = 
          let multiset_of_lit sign l r =
            match sign with
            | false -> [l;l;r;r]
            | true  -> [l;r]
          in
          let l_multiset = multiset_of_lit sign1 s t in
          let k_multiset = multiset_of_lit sign2 u v in
          Multiset_ordering.ord kbo_atoms l_multiset k_multiset
        in
        dbg D_trace @@ lazy (sprintf "mode2: %s" (PartialOrd.to_string mode2));

        assert PartialOrd.O.(mode1 = mode2);
      ));

      mode1
    (* ) *)
  ) 

  (* Case: one is eq and other is predicate *)
  | Some (l1,r1), None -> 
    (* Still need to check if the (syntactic) eq is actually a (semantic) predicate *)
    if is_pred r1 then
      kbo_predlits sign1 l1 sign2 atom2
    else (
      dbg D_trace @@ lazy "equality < pred";
      LT
    )
  | None, Some (l2,r2) -> 
    (* Still need to check if the (syntactic) eq is actually a (semantic) predicate *)
    if is_pred r2 then
      kbo_predlits sign1 atom1 sign2 l2
    else (
      dbg D_trace @@ lazy "pred > equality";
      GT
    )

  (* Case: both predicates *)
  | None, None -> 
    dbg D_trace @@ lazy "pred vs pred";
    kbo_predlits sign1 atom1 sign2 atom2

  (* | _ -> assert false *)

  ) |> tap (fun x -> dbg D_trace @@ lazy (sprintf "-- result lits: %s %s %s" (Term.to_string l) (PartialOrd.to_string x) (Term.to_string k)))


(* Infix operators *)
(* Helper functor *)
(* module MakeInfix (S:sig type t val cmp : t -> t -> PartialOrd.t end) *)

(* module Lits  = PartialOrdMakeInfix(struct type t = Term.literal let partial_compare = kbo_lits  end)
module Atoms = PartialOrdMakeInfix(struct type t = Term.term    let partial_compare = kbo_atoms end)
module Terms = PartialOrdMakeInfix(struct type t = Term.term    let partial_compare = kbo_terms end) *)

(* TODO unit testing for this module *)

let make ~weight ~symb_ordering =
  let uid = Orderings.get_next_uid () in
  let terms = kbo_terms ~weight ~symb_ordering in
  let oriented = mk_kbo_oriented uid terms in
  let atoms = kbo_atoms terms in
  let lits = kbo_lits terms oriented in
  ({ uid; terms; atoms; lits; oriented } : Orderings.t)

