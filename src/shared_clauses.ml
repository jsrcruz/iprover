open Lib
open Clause

let shared_clauses = ref BCSet.empty 
let get_all_shared_clauses () = !shared_clauses

let add_shared_clause clause = 
  shared_clauses := BCSet.add clause !shared_clauses

let add_shared_clause_list clause_list =
  List.iter add_shared_clause clause_list

let add_shared_clause_set clause_set =
  shared_clauses := BCSet.union clause_set !shared_clauses

let rm_shared_clause clause = 
  shared_clauses := BCSet.remove clause !shared_clauses
