# Configuration file for the general flow of managing the preprocessors
# and the prover-heuristic configuration.
import config_schedule # Import for specifying schedules
import tempfile

# The prover executable
PROVER = "./res/iproveropt"

# Whether to make the prover compute the costly proof reconstruction
# WARNING: This variable is globally accessible and modified in the main function
PROOF_MODEL_OUT = True # Default should always be to output proofs

# Creates a tmp directory in the system tmp dir
TMP_DIR = tempfile.mkdtemp(prefix="iprover_out_")

PROC_SLEEP_TIME = 0.5

# Timeout used for testing whether a problem is quick to parse
PARSE_TEST_TIMEOUT = 2

# Initial grace
GRACE = 5

# Heuristic context modifier for various competition requirements.
# for some competitions/divisions the heuristics have to be modified
# in order to run correctly. In this dictionary, we the new/alternative
# option-value pairs as a list.
CONTEXT_MODIFIERS = {'default': [], # Empty as no modification
                     'smt': ['--sup_smt_interval 1000', '--smt_ac_axioms fast'],
                     'fnt': ['--suppress_unsat_res true']
                     }

# Global variable to specify the context (this gets replaced by the list in runtime)
HEURISTIC_CONTEXT = "default"


# Global dict used to store the processes currently running
# the global scope enables process elimination upon interruption
PROVER_PROCESSES = {}

# LTB params
LTB_PHASE_LOW_MAX_RUNTIME = 12
LTB_PHASE_HIGH_MIN_RUNTIME = 180


# When running internal schedules we need to specify the the prep heuristic as separate.
# To run on qbf preprocessing needs to contain at least "--qbf_mode true" to succesfully
# run. In this setup we run the best heuristic as the preprocessor. The best heuristics
# are in the single schedules, hence we create a mapping between them as it will be less to update.
INTERNAL_PREP_HEURISTICS = {"default": "heur/default",
                            "fof_schedule": "heur/default",
                            "default_test": "heur/default",
                            "default_claus_test": "heur/default_claus_test",
                            "ueq_sched_no_abstr": "heur/245752",
                            "test_single_core_300": "heur/default_fof",
                            "test_single_core_300_admissible": "heur/default_fof",
                            "test_single_core_300_super": "heur/default_fof",
                            "test_single_core_300_admissible_super": "heur/default_fof"}


# Scheudle mapping for string and specification
#SCHEDULES = {"default": config_schedule.default,
#             "default_test": config_schedule.default_test,
#             "default_claus_test": config_schedule.default_claus_test,
#             "ueq_sched_no_abstr": config_schedule.ueq_sched_no_abstr}

SCHEDULES = {"default": config_schedule.default,
             "test_parallel": config_schedule.test_parallel,
             "fof_schedule": config_schedule.fof_schedule,
             "ueq_schedule": config_schedule.ueq_schedule,
             #"ltb_high_phase_schedule": config_schedule.ltb_high_phase_schedule,
             "fnt_schedule": config_schedule.fnt_schedule,
             #"ltb_low_phase_v4": config_schedule.ltb_low_phase_v4,
             #"ltb_low_phase_v5": config_schedule.ltb_low_phase_v5,
             #"ltb_high_common": config_schedule.ltb_high_common,
             "sup_cicm_20_train_default": config_schedule.sup_cicm_20_train_default,
             "sup_cicm_20_train_admissible": config_schedule.sup_cicm_20_train_admissible,
             "sup_cicm_20_train_parallel": config_schedule.sup_cicm_20_train_parallel,
             "test_single_core_300": config_schedule.test_single_core_300,
             "test_single_core_300_mixed": config_schedule.test_single_core_300_mixed,
             "test_single_core_300_admissible": config_schedule.test_single_core_300_admissible,
             "test_single_core_300_super": config_schedule.test_single_core_300_super,
             "test_single_core_300_super_x2": config_schedule.test_single_core_300_super_x2,
             "test_single_core_300_super_x3": config_schedule.test_single_core_300_super_x3,
             "test_single_core_300_super_x4": config_schedule.test_single_core_300_super_x4,
             "test_single_core_300_admissible_super": config_schedule.test_single_core_300_admissible_super,
             "smt_4000_super_300_4": config_schedule.smt_4000_super_300_4,
             "smt_4000_super_single_600": config_schedule.smt_4000_super_single_600,
             "smt_4000_super_single_900": config_schedule.smt_4000_super_single_900,
             "smt_4000_super_single_1200": config_schedule.smt_4000_super_single_1200,
             "smt_3scp_default_1200": config_schedule.smt_3scp_default_1200,
             "smt_3scp_default_300_x4": config_schedule.smt_3scp_default_300_x4,
             "smt_2scp_default_with_sat_1200": config_schedule.smt_2scp_default_with_sat_1200,
             "smt_2scp_default_with_sat_300_x4": config_schedule.smt_2scp_default_with_sat_300_x4,
             "admissible_schedule_test_recomputed": config_schedule.admissible_schedule_test_recomputed
             }

