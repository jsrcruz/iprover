import os
import subprocess
import re
import sys
import config
import prover_runner as pr
from helper import eprint
import helper as hp

# Get root logger
import logging
log = logging.getLogger()


def handle_pre_processing(problem_path, pre_processor_heuristic, pre_processor_timeout, time_limit, ltb):

    if ltb:
        log.error("Preprocessing not supported for batchfiles")
        sys.exit(1)

    # Run the preprocessor
    prep_problem = pre_process_problem(
        problem_path,
        pre_processor_heuristic,
        pre_processor_timeout,
        time_limit)

    # Check if the preprocessor solved the problem
    szs_status = pr.get_prover_output_status(prep_problem)
    if pr.check_prover_success(szs_status, ltb):
        hp.output_proof(prep_problem)
        log.info("Problem solved by pre-processing")

        # Clean up and terminate
        sys.exit(0)

    # Prep did not solve the problem, return the prep problem
    return prep_problem


def build_preprocessing_cmd(problem_path, prep_heuristic, prep_timeout):

    # Get the heuristic string
    opt_filters = ["schedule", "preprocessed_out", "tptp_safe_out", "time_out_prep_mult", "time_out_real"]
    prep_heuristic_string = pr.get_heuristic_string(prep_heuristic, prep_timeout, opt_filter=opt_filters)
    log.debug("prep_heuristic_string: {0}".format(prep_heuristic_string))

    # Build cmd string with heuristic and prep options
    cmd = "{0} {1} --schedule none --preprocessed_out true --tptp_safe_out true --time_out_prep_mult 1.0".format(
        config.PROVER, prep_heuristic_string)

    # Add the timeout and problem
    cmd += " --time_out_real {0:.2f} {1} ".format(prep_timeout, problem_path)

    # Get the out file and redirect output
    out_file = hp.get_tmp_out_file
    cmd += ' 1>> {0} 2>> {1}_error'.format(out_file, out_file)
    log.debug("Preprocessing cmd: {0}".format(cmd))

    return cmd, out_file


def run_preprocessor(prep_cmd, prep_timeout, out_file):

    # Run processing with the given timeout (or none)
    prep_proc = subprocess.Popen(prep_cmd, shell=True, preexec_fn=os.setsid)

    # Wait for the process to finish
    try:
        _ = prep_proc.communicate(timeout=prep_timeout)
        exc = prep_proc.returncode
    except subprocess.TimeoutExpired:
        prep_proc.kill()
        _ = prep_proc.communicate()
        exc = -1  # There has been a problem with the execution

    #  Check if any errors
    with open(out_file + "_error", 'r') as f:
        err = f.read()
        if err != '\n\n' and err != '':
            err = err.strip()
            log.error("Preprocessing ran with error: {0}".format(err))
        else:
            err = ""

    # TODO open errs!
    return err, exc


def check_preprocessing_run(out_file, problem_path, errs, exc):

    # Check errors and return original file as the output is corrupted or incorrect
    if errs != b'' or exc != 0:
        # Report instance
        eprint("ERROR: Preprocessing error with code: {0} and msg: {1}".format(exc, errs))
        # Return the original file
        return problem_path

    # Open file to check if some output was written to file
    with open(out_file, 'r') as f:
        outs = f.read()

    # The file needs to have content and contain 'cnf' (clauses) to at least be correct
    if outs == "" or "cnf" not in outs:
        log.warning("Preprocessing: error - returning original file")
        return problem_path

    # Preprocessing must have been successfull.
    # To make sure the file can be parsed by the prover, we grep the cnf clauses
    # and put it into a new file. (as some options might not be commented in the output)
    log.info("Preprocessing: finished prep file")

    # Filter the output to only contain cnf clauses (remove any comments)
    grep_out_path = grep_output_clauses(out_file)

    return grep_out_path


def grep_output_clauses(prep_outfile):
    # "grep" the output clauses and save to file

    # Get the file content
    with open(prep_outfile, 'r') as f:
        outs = f.read()

    # Grep the cnf clauses
    grep_outs = ""
    out_lines = outs.split("\n")
    for line in out_lines:
        if re.search(r'^cnf', line) is not None:
            grep_outs += line + "\n"

    # Write over the previous file content
    with open(prep_outfile, 'w') as f:
        f.write(grep_outs)

    return prep_outfile


def pre_process_problem(problem_path, prep_heuristic, prep_timeout, wc_timeout):

    # Check that the pre-processor heuristic exists
    hp.check_file_existence(prep_heuristic)

    # Compute the timeout bound
    prep_timeout = hp.compute_timeout_bound(prep_timeout, wc_timeout)

    # Get prep cmd
    prep_cmd, out_file = build_preprocessing_cmd(problem_path, prep_heuristic, prep_timeout)
    log.info("Running prep with timeout: ", prep_timeout)
    log.debug("Preprocessing cmd: {0}".format(prep_cmd))

    # Run the preprocessor process
    errs, exc = run_preprocessor(prep_cmd, prep_timeout, out_file)

    # Check if preprocessing was successfull. (Returns original file if not)
    prep_problem_path = check_preprocessing_run(out_file, problem_path, errs, exc)

    return prep_problem_path


