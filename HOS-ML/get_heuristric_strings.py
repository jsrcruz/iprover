import mysql.connector as db
import db_cred
import os
import errno
import sys
import config

CLAUSIFIER_PATH = "res/vclausify_rel"

def get_heuristic_params(experiment_id):

    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        # Get the parameters for the experiment

        # ParameterValueInt
        curs.execute("""
                     SELECT ParameterName, ParameterValueInt
                     FROM ExperimentProverParameters
                     WHERE ParameterValueInt IS NOT NULL
                     AND Experiment=%s;
                     """, (experiment_id,))
        value_int = curs.fetchall()

        # ParameterValueBool
        curs.execute("""
                     SELECT ParameterName, ParameterValueBool
                     FROM ExperimentProverParameters
                     WHERE ParameterValueBool IS NOT NULL
                     AND Experiment=%s;
                     """, (experiment_id,))
        value_bool = curs.fetchall()

        # Convert boolean '0','1' to 'false','true' / This could have been done in the db query
        for n in range(0, len(value_bool)):
            if value_bool[n][1]:
                value_bool[n] = (value_bool[n][0], 'true')
            else:
                value_bool[n] = (value_bool[n][0], 'false')

        # ParameterValueString
        curs.execute("""
                     SELECT ParameterName, ParameterValueString
                     FROM ExperimentProverParameters
                     WHERE ParameterValueString IS NOT NULL
                     AND Experiment=%s;
                     """, (experiment_id,))
        value_string = curs.fetchall()

        # ParameterValueFloat
        curs.execute("""

                     SELECT ParameterName, ParameterValueFloat
                     FROM ExperimentProverParameters
                     WHERE ParameterValueFloat IS NOT NULL
                     AND Experiment=%s;
                     """, (experiment_id,))

        value_float = curs.fetchall()

        # Combine all options into a list of tuples
        tuples = value_int + value_bool + value_string + value_float

        param_dict = dict(tuples)

        return param_dict

    except (db.Error) as err:
        print(err)
    except (Exception) as err:
        print(err)
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def quote_bracket_options(heur_params):

    # Check every option
    for k, v in heur_params.items():
        # Check if we have a [*] option
        if "[" == v[0] and "]" == v[-1]:
            heur_params[k] = "\"" + v + "\""

    return heur_params


def parse_heuristic(heur_params):

    # Remove include path
    heur_params.pop('--include_path', None)

    # Remove proof out blockers
    #heur_params.pop('--res_out_proof', None)
    #heur_params.pop('--inst_out_proof', None)
    #heur_params.pop('--sat_out_model', None)

    # Enforce proof out
    heur_params["--res_out_proof"] = "true"
    heur_params["--inst_out_proof"] = "true"
    heur_params["--sat_out_model"] = "small"

    # Remove timeout real
    heur_params.pop('--time_out_real', None)

    # Enforce clausifier to be present!
    # Use the vclausifieir as it is what is used in SMAC
    #if "--clausifier" in heur_params:
    heur_params["--clausifier"] = CLAUSIFIER_PATH

    # Check if it exists, as we need to make sure we do not override any sine options
    if '--clausifier_options' not in heur_params:
        heur_params['--clausifier_options'] = '--mode clausify -t 300'


    ### Clausifier ##
    """
    if "--clausifier" in heur_params and "--clausifier_options" in heur_params:
        # Change clausifier path

        # Quote clausifier options
        heur_params["--clausifier_options"] = "\"" + heur_params["--clausifier_options"] + "\""

        # Change clausifier time to the max time set above
        claus_options = re.sub(r"-t \d+\.\d+",
                               "-t {0}".format(float(MAX_CLAUSIFIER_TIME)),
                               heur_params["--clausifier_options"])
        heur_params["--clausifier_options"] = claus_options

    # Quote bracket options
    heur_params = quote_bracket_options(heur_params)
    #"""

    # Return parameters
    return heur_params


def param_dict_to_string(heur_params):

    # Get the parameters in sorted order
    heur_params = sorted(heur_params.items(), key=lambda kv: (kv[0]))

    # Construct heuristic string with a new option on each line
    heur_str = "\n".join([p + " " + str(v) for p, v in heur_params])

    # Return result
    return heur_str


def save_heuristic(heur_id, heur_str, heuristic_path):

    with open(heuristic_path + str(heur_id), 'w') as f:
        f.write(heur_str)


def main():

    # Destination path for the heuristic files
    dest_path = "heur/"
    try:
        os.makedirs(dest_path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    # If no experiments are specified from the user input, load them from the dictionary
    if len(sys.argv) > 1:
        exp_id = sys.argv[1:]
    else:
        all_heur = []
        # Get the heuristic paths from the dictionary
        sched = config.SCHEDULES.values()
        for sch in sched:
            all_heur += sch

        #print(len(all_heur))
        #print(all_heur)
        #print(len(sched))

        #print(sched)
        # Obtain the experiment ids
        exp_id = [e.split("/")[-1] for e, _ in all_heur]

    heuristic_list = sorted(set(exp_id))

    for heur in heuristic_list:
        print(heur)

        # Report if the string is emtpy
        heur_param_dict = get_heuristic_params(heur)
        # Check if empty
        if len(heur_param_dict) == 0:
            print("WARNING: No heuristic string returned for {0}".format(heur))
            break

        # Process the heuristic (remove timeout and other params)
        heur_param_dict = parse_heuristic(heur_param_dict)

        # Convert to string
        heur_string = param_dict_to_string(heur_param_dict)

        # Save the heuristic string
        save_heuristic(heur, heur_string, dest_path)


if __name__ == "__main__":
    main()
