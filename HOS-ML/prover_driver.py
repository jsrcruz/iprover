from helper import eprint
import atexit
import subprocess
import sys
import os
import time
import prover_runner as pr
import config
import argparse
from preprocessing import handle_pre_processing
import ltb_module
import helper as hp
import multiprocessing as mp
import signal


# Setup root logger
import logging
logging.basicConfig(stream=sys.stdout, level=logging.WARNING,
                    format='%(levelname)s - %(message)s')
log = logging.getLogger()

def main(problem_path, wc_timeout, schedule='default', schedule_mode='external',
         pre_processor_heuristic=None, pre_processor_timeout=200, no_cores=8, ltb=False,
         ltb_schedule_low_v4=None, ltb_schedule_low_v5=None, ltb_schedule_high=None, ltb_output_dir=".",
         suppress_proof_model_out=False, heuristic_context="default", loglevel=logging.WARNING):

    # Set the loglevel
    log.setLevel(loglevel)
    # Set the global proof reconstruction flag
    config.PROOF_MODEL_OUT = not suppress_proof_model_out
    # Set the context
    config.HEURISTIC_CONTEXT = heuristic_context

    # Check that problem/batchfile exists exists and exit if it doesn´t
    hp.check_file_existence(problem_path)

    # Get the current time as start time
    start_time = time.time()

    # Add  grace
    time_left = wc_timeout + config.GRACE
    log.info("Added {0}s grace, new timelimit is: {1}".format(config.GRACE, time_left))

    # Pre process problem if pre_processor is set (not avaliable for LTB)
    if pre_processor_heuristic and not ltb:
        log.debug("Preprocessing problem")
        problem_path = handle_pre_processing(
            problem_path,
            pre_processor_heuristic,
            pre_processor_timeout,
            time_left,
            ltb)

        # Prep did not solve the problem, update the time left and return the prep problem
        time_left = time_left - (time.time() - start_time)
        log.info("Time spent on prep: {0:.2f}".format(time.time() - start_time))
        log.info("Time left for prover: {0:.2f}".format(time_left))

    # Either run the LTB or standard setup
    if ltb:
        log.debug("Running LTB setup")
        handle_ltb_run(
            problem_path,
            ltb_output_dir,
            schedule_mode,
            no_cores,
            ltb_schedule_low_v4,
            ltb_schedule_low_v5,
            ltb_schedule_high)
    else:
        log.debug("Running standard setup")
        handle_prover_run(problem_path, start_time, time_left, schedule, schedule_mode, no_cores)



def get_input_args():

    parser = argparse.ArgumentParser()

    # Positional arguments
    parser.add_argument("problem_path", help="Path to problem")
    parser.add_argument("wc_timeout", type=float, help="WC time limit for problem")

    # Optional Arguments
    parser.add_argument("--no_cores", type=int, help="No cores available for proving (and embedding)", default=8)
    parser.add_argument("--schedule", help="The schedule to run",
                        default="default", choices=list(config.SCHEDULES.keys()))
    parser.add_argument("--schedule_mode", help="How to run the heuristics on the prover",
                        default="external", choices=["external", "internal"])
    parser.add_argument("--heuristic_context", help="The context used to alter the heuristics",
                        default="default", type=str, choices=list(config.CONTEXT_MODIFIERS.keys()))

    parser.add_argument("--pre_processor_heuristic", help="The heuristic to run as a pre-processor")
    parser.add_argument("--pre_processor_timeout", type=int,
                        help="The timeout for the pre-processor")

    parser.add_argument("--suppress_proof_model_out", help="Turn off proof reconstruction",
                        action="store_true")
    # LTB arguments
    parser.add_argument(
        "--ltb",
        help="Set to run in LTB mode on BathcFile",
        action="store_true")
    parser.add_argument('--ltb_output_dir', help="Output directory for LTB proofs")
    parser.add_argument('--ltb_schedule_low_v4', help="The low time limit schedule for LTB (v4)",
                        choices=list(config.SCHEDULES.keys()))
    parser.add_argument('--ltb_schedule_low_v5', help="The low time limit schedule for LTB (v5)",
                        choices=list(config.SCHEDULES.keys()))
    parser.add_argument('--ltb_schedule_high', help="The high time limit schedule for LTB",
                        choices=list(config.SCHEDULES.keys()))

    # Set loglevels
    parser.add_argument(
        '-d', '--debug',
        help="Print debug logs",
        action="store_const", dest="loglevel", const=logging.DEBUG,
        default=logging.WARNING,
    )
    parser.add_argument(
        '-v', '--verbose',
        help="Print info logs",
        action="store_const", dest="loglevel", const=logging.INFO,
    )

    # Parse the arguments
    args = parser.parse_args()

    # Report id debug
    if args.loglevel == logging.DEBUG:
        print(args)

    # Return the arguments
    return args


def check_illegal_args(args):

    try:
        if args.wc_timeout < 0:
            eprint("ERROR: wc_timeout cannot be negative")
            raise ValueError

        if args.pre_processor_timeout is not None and args.pre_processor_timeout < 0:
            eprint("ERROR: pre_processor_timeout cannot be negative")
            raise ValueError
        if args.pre_processor_timeout is not None and args.pre_processor_timeout > args.wc_timeout:
            eprint("ERROR: pre_processor_timeout cannot be larger than wc_timeout")
            raise ValueError
        if args.pre_processor_timeout is not None and args.pre_processor_heuristic is None:
            eprint("ERROR: Need to specify preprocessor heuristic (no default)")
            raise ValueError

        # LTB options
        if args.pre_processor_heuristic and args.ltb:
            eprint("ERROR: preprocessing currently not supported for LTB")
            raise ValueError
        if args.ltb_schedule_low_v4 is not None and not args.ltb:
            eprint("ERROR: ltb_schedule_low_v4 only operational with ltb mode")
            raise ValueError
        if args.ltb_schedule_low_v5 is not None and not args.ltb:
            eprint("ERROR: ltb_schedule_low_v5 only operational with ltb mode")
            raise ValueError
        if args.ltb_schedule_high is not None and not args.ltb:
            eprint("ERROR: ltb_schedule_high only operational with ltb mode")
            raise ValueError

        # Check if the schedules are provided for LTB
        if args.ltb_schedule_low_v4 is None and args.ltb:
            eprint("ERROR: ltb_schedule_low_v4 is not provided!")
            raise ValueError()
        if args.ltb_schedule_low_v5 is None and args.ltb:
            eprint("ERROR: ltb_schedule_low_v5 is not provided!")
            raise ValueError()
        if args.ltb_schedule_high is None and args.ltb:
            eprint("ERROR: ltb_schedule_high is not provided!")
            raise ValueError()

    except ValueError:
        eprint("EXIT ON ERROR")
        sys.exit(1)


def parsing_test_succesfull(problem, timeout):

    # Check if TPTP variable is correctly set for the includes
    try:
        os.environ["TPTP"]
    except KeyError:
        log.error("TPTP variable not set correctly for clausification test")
        return False

    try:
        # Start the clausification test
        cmd = "./res/vclausify_rel --mode clausify -t {0} --include $(echo $TPTP) {1}".format(timeout, problem)
        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        log.debug("Clausify testing: {0}".format(cmd))

        # Wait for embedding to finish
        outs, errs = proc.communicate(timeout=max(8, timeout + 2))

    except subprocess.TimeoutExpired:
        # If communicate timed out the test clairly failed
        log.warning("Clausify test subprocess.TimeoutExpired")
        proc.kill()
        outs, errs = proc.communicate()
        return False

    test_passed = proc.returncode == 0 and errs == b''
    log.info("Clausification test passed: {0}".format(test_passed))
    return test_passed


# Handler for standard prover runs (non-LTB)
def handle_prover_run(problem, start_time, time_left, schedule, schedule_mode, no_cores):

    # Start proving
    #KK commented for cluster-scripts TODO
    print("% SZS status Started for {0}".format(problem))

    # Check if we can parse the problem in one second, otherwise, change the schedule mode to internal
    # Check if internal scheduling is more beneficial if problem takes time to parse
    if schedule_mode != "internal":
        if not parsing_test_succesfull(problem, config.PARSE_TEST_TIMEOUT):
            schedule_mode = "internal"
            log.info("Changed schedule mode to internal")
        # Update time left
        time_left = time_left - (time.time() - start_time)
        log.info("Time left: {0:g}".format(time_left))

    # Run the prover setup
    success, output_file = run_prover_setup(problem, time_left, schedule, schedule_mode, no_cores)

    if success:
        # Print the proof
        hp.output_proof(output_file)
        # Proof has been outputted by abiove. Unregister any proof other proof outputstart
        atexit.unregister(hp.output_proof)
    else:
        print("% SZS status Unknown for {0}".format(problem))


def handle_ltb_run(batchfile_path, proof_output_dir, schedule_mode, no_cores,
                   ltb_schedule_low_v4, ltb_schedule_low_v5, ltb_schedule_high):

    # Make the output directory
    if not os.path.exists(proof_output_dir):
        os.makedirs(proof_output_dir)
        log.info("Created proof output dir: {0}".format(proof_output_dir))

    # Get the start time
    start_time = time.time()

    # Get attributes of the batchfile
    batch_dir, time_limit, problems = ltb_module.process_batchfile(batchfile_path)

    # Create batch manager
    ltb_batch = ltb_module.create_ltb_batch(batch_dir, time_limit, problems)
    log.info("Loaded and created LTB batch: {0}".format(ltb_batch))

    # LTB LOW timelimit phase
    log.info("Running LTB LOW timelimit phase")
    ltb_batch = run_ltb_low_timelimit_phase(ltb_batch, proof_output_dir, schedule_mode, ltb_schedule_low_v4, ltb_schedule_low_v5, config.LTB_PHASE_LOW_MAX_RUNTIME, no_cores)

    # LTB HIGH timelimit phase
    time_left = time_limit - (time.time() - start_time)
    log.info("Running LTB HIGH timelimit phase with {0}s left".format(time_left))
    ltb_batch = run_ltb_high_timelimit_phase(
        ltb_batch,
        proof_output_dir,
        schedule_mode,
        ltb_schedule_high,
        time_left,
        no_cores,
        version="+4")

    # If there for some reason is time left after running +4
    # We have a little attempt at +5
    # LTB HIGH timelimit phase
    time_left = time_limit - (time.time() - start_time)
    log.info("Running SECOND LTB HIGH timelimit phase with {0}s left".format(time_left))
    ltb_batch = run_ltb_high_timelimit_phase(
        ltb_batch,
        proof_output_dir,
        schedule_mode,
        ltb_schedule_high,
        time_left,
        no_cores,
        version="+5")

def run_ltb_high_timelimit_phase(ltb_batch, proof_output_dir, schedule_mode,
                                 ltb_schedule_high, timelimit, no_cores, version=None):
    log.debug("LTB problem version: {0}".format(version))

    # Get the start time
    start_time = time.time()

    # Get the list of unsolved problems
    unsolved_problems = ltb_batch.get_unsolved_problems()
    log.info("Number of unsolved problems: {0}".format(version))

    # Make a process pool
    pool = mp.Pool(processes=no_cores)

    # Create manager for accessing the number of unsolved problems while running proccesses
    # in parallell. The number of unsolved processes is used to calculate the time available.
    manager = mp.Manager()
    val = manager.Value('i', len(unsolved_problems))
    log.debug("Created manager with shared value: {0}".format(val.value))
    #val.value = len(problems)
    lock = manager.Lock()

    # Create function arguments for the problem attempt
    func_args = [(val, lock, schedule_mode, ltb_schedule_high, proof_output_dir, prob,
                  start_time, timelimit, no_cores, version) for prob in unsolved_problems]

    # Compute the processes
    res = pool.starmap(run_ltb_multi_schedule, func_args)

    # No more processes to the queue
    log.info("Started process pool")
    pool.close()
    # Wait for pool to finish processing before carrying on
    pool.join()
    log.info("Process pool finished")
    log.info("Final unsolved val: {0}".format(val.value))

    log.debug("Process output results: {0}".format(res))

    # Update the ltb_batch object
    log.info("Updating the batch with solved problems")
    for problem_name, success in res:
        if success:
            ltb_batch.set_problem_solved(problem_name)

    # Log the result batch
    log.debug("ltb_batch: {0}".format(ltb_batch))

    return ltb_batch


def run_ltb_multi_schedule(no_unsolved, lock, schedule_mode, schedule, proof_output_dir, problem,
                           start_time, wc_timelimit, no_cores, version):

    # Get the problem version with the least number of clauses
    if version is None:
        log.debug("Version is not set: computing optimal version")
        problem_path, version_name = ltb_module.compute_optimal_problem_version_ltb(problem)
    else:
        log.debug("Getting problem with version: {0}".format(version))
        problem_path, version_name = problem.get_path(version=version)

    print("% SZS status Started for {0}".format(version_name))

    # Compute the runtime for the attempt based on the time left
    # and the time available when sharing between the processes
    # and the other unsolved problems.
    time_left = wc_timelimit - (time.time() - start_time)
    time_available = min(time_left, (time_left * no_cores) / no_unsolved.value)
    runtime = max(time_available, config.LTB_PHASE_HIGH_MIN_RUNTIME)

    log.debug("time_left: {0:.1f}".format(time_left))
    log.debug("time_available: {0:.1f}".format(time_available))
    log.debug("no_unsolved: {0}".format(no_unsolved.value))
    log.info("runtime: {0:.1f}".format(runtime))

    # Run the schedule on the problem, but only limit to 1 core!
    success, proof_file = run_prover_setup(problem_path, runtime, schedule, schedule_mode, 1, ltb_mode=True)

    if success:
        print("% SZS status Theorem for {0}".format(version_name))
        # Print to file
        ltb_module.output_proof_to_file(proof_output_dir, problem.name, proof_file)
    else:
        print("% SZS status GaveUp for {0}".format(version_name))
    print("% SZS status Ended for {0}".format(version_name))

    # If solved, update the shared variable
    if success:
        with lock:
            no_unsolved.value -= 1
            log.debug("Decreased number of unsolved variables")

    # Return the problem name and the success status
    return problem.name, success


def run_ltb_low_timelimit_phase(ltb_batch, proof_output_dir, schedule_mode,
                                ltb_schedule_low_v4, ltb_schedule_low_v5, low_timelimit, no_cores):

    # Run low timelimit on +4 problems
    log.info("Starting LTB round: low-timelimit v:+4: timeout: {0}".format(low_timelimit))
    ltb_batch = run_ltb_problem_attempt(
        ltb_batch,
        proof_output_dir,
        schedule_mode,
        ltb_schedule_low_v4,
        low_timelimit,
        no_cores,
        version="+4")

    # Run low timelimit on +5 problems
    log.info("Starting LTB round: low-timelimit v:+5 timeout: {0}".format(low_timelimit))
    ltb_batch = run_ltb_problem_attempt(
        ltb_batch,
        proof_output_dir,
        schedule_mode,
        ltb_schedule_low_v5,
        low_timelimit,
        no_cores,
        version="+5")

    log.info("Low Time-limit phase ended: {0} problems solved".format(ltb_batch.get_no_solved_problems()))

    return ltb_batch


def run_ltb_problem_attempt(ltb_batch, proof_output_dir, schedule_mode,
                            schedule, attempt_timelimit, no_cores, version=None):

    for problem in list(ltb_batch.get_unsolved_problems()):

        # Get the problem_path from the problem
        problem_path, version_name = problem.get_path(version=version)
        print("% SZS status Started for {0}".format(version_name))

        success, output_file = run_prover_setup(
            problem_path, attempt_timelimit, schedule, schedule_mode, no_cores, ltb_mode=True)

        if success:
            print("% SZS status Theorem for {0}".format(version_name))
            ltb_batch = ltb_module.handle_solved_problem(ltb_batch, proof_output_dir, problem, output_file)
        else:
            print("% SZS status GaveUp for {0}".format(version_name))

        print("% SZS status Ended for {0}".format(version_name))

    return ltb_batch


def get_schedule_conf(schedule, schedule_mode, problem_path, no_cores):

    try:
        schedule_conf = config.SCHEDULES[schedule]
        log.info("Obtained schedule: {0}".format(schedule))
    except KeyError:
        log.error("The schedule \"{0}\" is not implemented.".format(schedule))
        sys.exit(1)

    if isinstance(schedule_conf, dict):
        # The schedule is admissible, so find the local schedule
        log.info("Admissible schedules, predicting local schedule")
        local_schedule = predict_local_schedule(schedule_conf, problem_path, no_cores)
        log.info("Obtained local schedule: {0}".format(local_schedule))
        # Extract the appropriate schedule
        schedule_conf = schedule_conf[local_schedule]

    # If we use multiple cores (nested lists) in external mode, we need to flatten the schedule
    if isinstance(schedule_conf, list) and isinstance(schedule_conf[0], list) and schedule_mode == "external":
        schedule_conf = get_core_order(schedule_conf)

    # Handle internal prep scheduling if set
    if schedule_mode == "internal":
        # Get the prep heuristic
        prep_heuristic = get_scheule_internal_prep_heuristic(schedule)
        log.info("Using prep heuristic: {0}".format(prep_heuristic))

        # Transform schedule into single core if not in core format
        if isinstance(schedule_conf, list) and isinstance(schedule_conf[0], tuple):
            log.info("Placing internal schedule on a single core")
            schedule_conf = [schedule_conf]
    else:
        prep_heuristic = None

    log.info("Final schedule: {0}".format(schedule_conf))

    return schedule_conf, prep_heuristic


def get_core_order(schedule):

    start_times = {}
    run_times = {}
    for core in schedule:
        # The start time on a core is always zero
        current_time = 0
        for heur, run_time in core:
            # Extract the runtime
            run_times[heur] = run_time
            # Set the start time of theu heuristic
            start_times[heur] = current_time
            # Increment the time
            current_time += run_time

    # Sort heuristics according to their start times
    heur_order = [k for k, v in sorted(start_times.items(), key=lambda item: item[1])]


    # Rebuild flat schedule
    flattened = []
    for heur in heur_order:
        flattened += [(heur, run_times[heur])]

    return flattened


def embed_problem(schedule, problem, no_cores, print_proof=True):

    # Run the problem mebedding
    cmd = "./dist/run_embedding/run_embedding {0} {1} {2} --clausifier_path res/vclausify_rel --no_cores {3}".format(config.PROVER, schedule["model"], problem, no_cores)
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid)
    log.debug("Getting embedding: {0}".format(cmd))
    try:
        # Wait for embedding to finish
        outs, errs = proc.communicate(timeout=15)

        # If there was an error while embedding the problem, return None
        if not (proc.returncode == 0 or proc.returncode == 10) or errs != b'':
            log.warning("Embedding code: {0} msg: {1}".format(proc.returncode, errs))
            return None

    except subprocess.TimeoutExpired:
        log.warning("run_embedding subprocess.TimeoutExpired")
        os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
        outs, errs = proc.communicate()
        return None

    # If the problem was solved, report and quit!
    if proc.returncode == 10:
        log.info("Problem was solved while embedding")
        if print_proof:
            log.info("Printing proof and exiting")
            print(outs.decode("utf-8"))
            sys.exit(0)
        else:
            # For use with the simulation code
            return "solved"

    # convert string output to list of floats and return the embedding vector
    embedding = [float(s) for s in outs.decode("utf-8")[:-1].strip("][").split()]
    log.debug("Obtained embedding: {0}".format(embedding))
    return embedding


def map_embedding_to_cluster(embedding, cluster_centers):

    # Get the embedding and map to the local schedule
    try:
        cluster = hp.get_closest_cluster_center(embedding, cluster_centers)
        return cluster
    except Exception as err:
        log.warning("Unexpected exception during schedule mapping: {0}".format(err))
        # Return default scheudle if something goes wrong
        return "default"


def predict_local_schedule(schedule, problem, no_cores, print_proof=True):
    # Obtain the problem embedding
    embedding = embed_problem(schedule, problem, no_cores, print_proof=print_proof)

    # If embedding was unsuccessfull, return fallback
    if embedding is None:
        return "default"
    elif embedding == "solved": # Used with the simulation code
        return "solved"

    cluster = map_embedding_to_cluster(embedding, schedule["cluster_centers"])
    if cluster is None:
        return "default"
    else:
        return cluster


def get_scheule_internal_prep_heuristic(schedule):

    # Get the schedule preprocessor
    try:
        internal_prep_heuristic = config.INTERNAL_PREP_HEURISTICS[schedule]
    except KeyError:
        log.warning("The prep heuristic for \"{0}\" is not implemented. Using default prep. ".format(schedule))
        internal_prep_heuristic = "heur/schedule_none"

    return internal_prep_heuristic


def run_prover_setup(problem_path, time_left, schedule, schedule_mode, no_cores, ltb_mode=False):

    # Keep track of time
    now = time.time()

    # Compute the schedule setup and run
    log.debug("Computing schedule configuration")
    schedule_conf, prep_heuristic = get_schedule_conf(schedule, schedule_mode, problem_path, no_cores)
    # Get time left after finding the schedule
    time_left = time_left - (time.time() - now)
    log.info("Time left after finding schedule: {0:g}".format(time_left))

    log.debug("Running schedule in mode: {0}".format(schedule_mode))
    success, proof_file = pr.run_schedule(
        problem_path, schedule_mode, schedule_conf, time_left, no_cores, ltb_mode, prep_heuristic)

    return success, proof_file


if __name__ == "__main__":

    # Get the arguments
    args = get_input_args()

    # Check for illegall arg combinations: This can probably be done smarter
    # in the argparses (later)
    check_illegal_args(args)

    # Run the program
    main(**vars(args))
    log.info("Finished")
