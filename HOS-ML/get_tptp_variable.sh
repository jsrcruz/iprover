#!/bin/bash

# We set the TPTP variable differently depending on the file structure
# of the given problem. If it is a TPTP style file structure, we need
# to find the root TPTP directory. Otherwise, we set the variable as the
# directory of the problem. We assume that it is a TPTP file structure
# if the problem path contains "TPTP".


# Chech whether the full path contains TPTP
dirname $1 | grep -q "TPTP"
if [ $? -eq 0 ];
then
    # TPTP style file structure, get root TPTP directory
    TPTP=$(dirname $1 | grep -oP "(?s).*TPTP(.*?)/")
else
    # No TPTP file structure, set TPTP variable
    # as the parent directory of the problem
    TPTP=$(dirname $1)
fi

# Repor tthe result
echo $TPTP

