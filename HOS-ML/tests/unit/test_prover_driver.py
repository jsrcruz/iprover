from unittest import mock
from prover_driver import run_ltb_multi_schedule


@mock.patch('ltb_module.output_proof_to_file', auto_spec=True)
@mock.patch('prover_driver.run_prover_setup', auto_spec=True)
@mock.patch('ltb_module.compute_optimal_problem_version_ltb',
            return_value=("problem_path", "problem_version.p"), auto_spec=True)
def test_run_ltb_multi_schedule_success(compute_version, run_prover, report_result, capsys):

    # Mock the multipeorcessing related variables
    value_mock = mock.MagicMock()
    value_mock.value = 1
    lock_mock = mock.MagicMock()

    # Mock the ltb problem
    prob_mock = mock.MagicMock()
    prob_mock.name = "test_problem"

    # Mock the proof attempt (should check the contents of the call, especially the cores)
    run_prover.return_value = (True, "proof_output_file")

    # Run the function to test
    res_name, res_success = run_ltb_multi_schedule(value_mock, lock_mock, "external", "schedule",
                                                   "proof_output_dir", prob_mock, 0, 10, 4, None)

    # Check the returned values
    assert res_name == "test_problem"
    assert res_success

    # Assert that the number ofsolved problems has decreased by 1
    assert value_mock.value == 0

    # Check runtime output (Theorem)
    out, err = capsys.readouterr()
    assert err == ""
    assert "% SZS status Started for problem_version" in out
    assert "% SZS status Theorem for problem_version" in out
    assert "% SZS status Ended for problem_version" in out

    # Check that the prover was ran correctly
    run_prover.assert_called_once_with("problem_path", mock.ANY, "schedule", "external", 1, ltb_mode=True)
    # Check that the success result handler was called appropriatly
    report_result.assert_called_once_with("proof_output_dir", "test_problem", "proof_output_file")



@mock.patch('ltb_module.output_proof_to_file', auto_spec=True)
@mock.patch('prover_driver.run_prover_setup', auto_spec=True, return_value=(False, "output_file"))
@mock.patch('ltb_module.compute_optimal_problem_version_ltb',
            return_value=("problem_path", "problem_version.p"), auto_spec=True)
def test_run_ltb_multi_schedule_failure(compute_version, run_prover, report_result, capsys):

    # Mock the multipeorcessing related variables
    value_mock = mock.MagicMock()
    value_mock.value = 3
    lock_mock = mock.MagicMock()

    # Mock the ltb problem
    prob_mock = mock.MagicMock()
    prob_mock.name = "test_problem"

    # Mock the proof attempt (should check the contents of the call, especially the cores)
    #run_prover.return_value = (False, "output_file")
    #assert run_prover.return_value == 0
    #assert run_prover == 0

    # Run the function to test
    res_name, res_success = run_ltb_multi_schedule(value_mock, lock_mock, "external", "schedule",
                                                   "proof_output_dir", prob_mock, 0, 10, 4, None)

    # Check the returned values
    assert res_name == "test_problem"
    assert not res_success

    # Assert that the number ofsolved problems has decreased by 1
    assert value_mock.value == 3

    # Check runtime output (Theorem)
    out, err = capsys.readouterr()
    assert err == ""
    assert "% SZS status Started for problem_version" in out
    assert "% SZS status GaveUp for problem_version" in out
    assert "% SZS status Ended for problem_version" in out

    # Check that the success result handler was called appropriatly
    assert report_result.call_count == 0

    # Check that the prover runner was called properly, especially important is the number of cores
    run_prover.assert_called_once_with("problem_path", mock.ANY, "schedule", "external",
                                       1, ltb_mode=True)
